/******************************************************************************
                        FUNCIONES - VALIDACION DE CAMPOS
 "^[0-9.]+$");
******************************************************************************/
/* Nombre y Apellido Cliente */
$('input[name="name"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Email */
$('input[name="email"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9üÜ_@.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* DNI | CUIL */
$('input[name="identification"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Teléfono */
$('input[name="phone"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9+]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Dirección */
$('input[name="address"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9áéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Número del domicilio */
$('input[name="number"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Piso de Departamento */
$('input[name="floor"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñ0-9-° ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Localidad */
$('input[name="locality"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ0-9 ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Código Postal */
$('input[name="postal_code"]').bind('keypress', function(event) {

    let regex = new RegExp("[A-Z0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Tipo de Estado */
$('input[name="state_type"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});
/******************************************************************************
                        TIENDANUBE
******************************************************************************/
/* Número de Orden */
$('input[name="order_number"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Nombre y Apellido Cliente */
$('input[name="customer_name"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Email */
$('input[name="customer_email"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9üÜ_@.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* DNI */
$('input[name="customer_identification"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Teléfono */
$('input[name="customer_phone"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9+]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Dirección */
$('input[name="customer_address"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9áéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Número del domicilio */
$('input[name="customer_number"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Piso de Departamento */
$('input[name="customer_floor"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñ0-9-° ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Localidad */
$('input[name="customer_locality"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñ0-9 ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Ciudad */
$('input[name="customer_city"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñ0-9 ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Código Postal */
$('input[name="customer_zipcode"]').bind('keypress', function(event) {

    let regex = new RegExp("[A-Z0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Provincia / Estado */
$('input[name="customer_province"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Paises */
$('input[name="customer_country"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Codigo de Producto */
$('input[name="product_id"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Nombre del Producto */
$('input[name="product_name"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9-áéíóúÁÉÍÓÚÑñüÜ(), ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Precio de Producto */
$('input[name="product_price"]').bind('keypress', function(event) {
// "^[0-9]{1,10}[\.][0-9]{1,2}$");
    let regex = new RegExp("[0-9.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Codigo de Producto */
$('input[name="product_quantity"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Medios de Pago */
$('input[name="gateway"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Estado de la Orden */
$('input[name="oder_status"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Estado de Pago */
$('input[name="payment_status"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Moneda */
$('input[name="currency"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Subtotal */
$('input[name="subtotal"]').bind('keypress', function(event) {
// "^[0-9]{1,10}[\.][0-9]{1,2}$");
    let regex = new RegExp("[0-9.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Cupon de Descuento */
$('input[name="discount_coupon"]').bind('keypress', function(event) {
// "^[0-9]{1,10}[\.][0-9]{1,2}$");
    let regex = new RegExp("[0-9.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Descuento */
$('input[name="discount"]').bind('keypress', function(event) {
// "^[0-9]{1,10}[\.][0-9]{1,2}$");
    let regex = new RegExp("[0-9.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Costo del Envio */
$('input[name="shipping_cost_customer"]').bind('keypress', function(event) {
// "^[0-9]{1,10}[\.][0-9]{1,2}$");
    let regex = new RegExp("[0-9.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Total */
$('input[name="total"]').bind('keypress', function(event) {
// "^[0-9]{1,10}[\.][0-9]{1,2}$");
    let regex = new RegExp("[0-9.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Medio de Envio */
$('input[name="shipping_option"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9áéíóúÁÉÍÓÚÑñüÜ(), ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* Identificador transacción medio pago */
$('input[name="gateway_id"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});

/* ID de la Orden */
$('input[name="order_id"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }

});