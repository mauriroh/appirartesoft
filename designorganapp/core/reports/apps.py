from django.apps import AppConfig


class ReportsConfig(AppConfig):
    # name = 'reports'
    name = 'core.reports'
