from django.urls import path

from core.reports.views.reportsale.views import ReportSaleView

app_name = 'reports_app'

urlpatterns = [
    # reports
    path('report-sale/', ReportSaleView.as_view(), name='report-sale'),
]