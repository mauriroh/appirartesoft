from django.contrib.auth.mixins import LoginRequiredMixin
from core.erp.mixins import ValidatePermissionRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from core.erp.models import Sale
from core.reports.forms import ReportForm

from django.db.models.functions import Coalesce
from django.db.models import Sum


 # Video 87
class ReportSaleView(LoginRequiredMixin, ValidatePermissionRequiredMixin, TemplateView):
    template_name = 'report-sale/report-sale.html'
    success_url = reverse_lazy('reports_app:report-sale')
    success_url_return = reverse_lazy('productos_app:graphic-detail-sales')
    permission_required = 'view_sale'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_report':
                data = []
                start_date = request.POST.get('start_date', '')
                end_date = request.POST.get('end_date', '')
                search = Sale.objects.all()
                if len(start_date) and len(end_date):
                    search = search.filter(date_joined__range=[start_date, end_date])
                for s in search:
                    data.append([
                        s.order_number,
                        s.cli.name,
                        s.date_joined.strftime('%Y-%m-%d'), # self.date_joined.strftime('%Y-%m-%d')
                        format(s.subtotal, '.2f'),
                        format(s.iva, '.2f'),
                        format(s.total, '.2f'),
                    ])

                subtotal = search.aggregate(r=Coalesce(Sum('subtotal'), 0)).get('r')
                iva = search.aggregate(r=Coalesce(Sum('iva'), 0)).get('r')
                total = search.aggregate(r=Coalesce(Sum('total'), 0)).get('r')

                data.append([
                    '',
                    '',
                    '',
                    format(subtotal, '.2f'),
                    format(iva, '.2f'),
                    format(total, '.2f'),
                ])
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Reporte de Ventas'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['list_url'] = self.success_url
        context['list_url_return'] = self.success_url_return
        context['form'] = ReportForm()
        return context
