from django.apps import AppConfig


class ErpConfig(AppConfig):
    # name = 'erp'
    name = 'core.erp'
