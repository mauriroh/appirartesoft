import requests
import json
API_KEY = 'bearer 64ec344e950b2447f719901dc1b4d92575adee02'
BASE_URL = 'https://api.tiendanube.com/v1/440495/products'
USER_AGENT = 'irartecontacto@yahoo.com.ar'
# USER_AGENT = 'soft.organapp@gmail.com'


try:
    params = {
        'Content-Type': 'application/json',
        'Authentication': f'{API_KEY}',
        'User-Agent': f'{USER_AGENT}'
    }

    res = requests.get(f'{BASE_URL}', headers=params)
    print("Respuesta de res")
    print(res)

    if res.status_code == 200:
        print(res.text)
        response_json = res.json()
        for r in response_json:
            print(r['id'])
            print(r['name']['es'])
            # print(r['images'])

    else:
        print(res.text)
except Exception as e:
    print(e)









class Estados(models.Model):
    state_type = models.CharField(max_length=20, null=False, blank=False, unique=True, verbose_name='Estado')

    def __str__(self):
        return self.state_type

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'
        db_table = 'states'
        ordering = ['id']



class Department(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False, unique=True, verbose_name='Departamento / Partido')

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'
        db_table = 'department'
        ordering = ['name']



class Province(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False, unique=True, verbose_name='Provincia')

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Provincia'
        verbose_name_plural = 'Provincias'
        db_table = 'province'
        ordering = ['name']



class Client(models.Model):
    name = models.CharField(max_length=250, null=False, blank=True, verbose_name='Nombre')
    email = models.EmailField(blank=True, default='', verbose_name='Email')
    identification = models.CharField(max_length=250, blank=True, default='', verbose_name='DNI / CUIT')
    phone = models.CharField(max_length=250, blank=True, default='', verbose_name='Teléfono / Móvil')
    address = models.CharField(max_length=250, blank=True, default='', verbose_name='Dirección')
    number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número')
    floor = models.CharField(max_length=250, blank=True, default='', verbose_name='Piso')
    locality = models.CharField(max_length=250, blank=True, default='', verbose_name='Localidad')
    department = models.ForeignKey(Department, on_delete=models.CASCADE, verbose_name='Departamento / Ciudad')
    postal_code = models.CharField(max_length=250, blank=True, default='', verbose_name='Código Postal')
    province = models.ForeignKey(Province, on_delete=models.CASCADE, verbose_name='Provincia / Estado')
    others = models.TextField(blank=True, default='', verbose_name='Otros')

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        item['department'] = self.department.toJSON()
        item['province'] = self.province.toJSON()
        return item

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        db_table = 'client'
        ordering = ['name']
        unique_together = ('name','email')



class Product(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False, verbose_name='Nombre del Producto')
    stock = models.IntegerField(default=0, verbose_name='Stock')
    pvp = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Precio de venta')

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        item['pvp'] = format(self.pvp, '.2f')
        return item

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        db_table = 'product'
        ordering = ['name']



class Sale(models.Model):
    cli = models.ForeignKey(Client, on_delete=models.CASCADE)
    date_joined = models.DateField(default=datetime.now)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    iva = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.cli.name

    def toJSON(self):
        item = model_to_dict(self)
        item['cli'] = self.cli.toJSON()
        item['subtotal'] = format(self.subtotal, '.2f')
        item['iva'] = format(self.iva, '.2f')
        item['total'] = format(self.total, '.2f')
        item['date_joined'] = self.date_joined.strftime('%Y-%m-%d')
        item['det'] = [i.toJSON() for i in self.detsale_set.all()]  #vídeo 64 - 12:20
        return item

    def delete(self, using=None, keep_parents=False):
        for det in self.detsale_set.all():
            det.prod.stock += det.cant
            det.prod.save()
        super(Sale, self).delete()

    class Meta:
        verbose_name = 'Venta'
        verbose_name_plural = 'Ventas'
        db_table = 'sale'
        ordering = ['id']



class DetSale(models.Model):
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
    prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    cant = models.IntegerField(default=0)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.prod.name

    def toJSON(self):
        item = model_to_dict(self, exclude=['sale'])
        item['prod'] = self.prod.toJSON()
        item['price'] = format(self.price, '.2f')
        item['subtotal'] = format(self.subtotal, '.2f')
        return item

    class Meta:
        verbose_name = 'Detalle de Venta'
        verbose_name_plural = 'Detalle de Ventas'
        db_table = 'sale_detail'
        ordering = ['id']



class Production(models.Model):
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número de Orden')
    product_name = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Nombre del Producto')
    quantity = models.PositiveIntegerField(default=0, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    shop = models.CharField(max_length=15, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')

    def __str__(self):
        return self.product_name.name

    def toJSON(self):
        item = model_to_dict(self)
        item['product_name'] = self.product_name.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        return item

    class Meta:
        verbose_name = 'Producción'
        verbose_name_plural = 'Producciones'
        db_table = 'production'
        ordering = ['id']



class Tiendanube(models.Model):
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    created_at = models.DateField(null=True, blank=True, verbose_name='Fecha venta')
    customer_name = models.CharField(max_length=250, blank=True, default='', verbose_name='Nombre comprador')
    customer_email = models.EmailField(blank=True, default='', verbose_name='Email')
    customer_identification = models.CharField(max_length=250, blank=True, default='', verbose_name='DNI / CUIT')
    customer_phone = models.CharField(max_length=250, blank=True, default='', verbose_name='Teléfono')
    customer_address = models.CharField(max_length=250, blank=True, default='', verbose_name='Dirección')
    customer_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número')
    customer_floor = models.CharField(max_length=250, blank=True, default='', verbose_name='Piso')
    customer_locality = models.CharField(max_length=250, blank=True, default='', verbose_name='Localidad')
    customer_city = models.CharField(max_length=250, blank=True, default='', verbose_name='Ciudad')
    customer_zipcode = models.CharField(max_length=250, blank=True, default='', verbose_name='Código Postal')
    customer_province = models.CharField(max_length=250, blank=True, default='', verbose_name='Provincia o estado')
    customer_country = models.CharField(max_length=250, blank=True, default='', verbose_name='País')
    product_id = models.CharField(max_length=250,  blank=True, default='', verbose_name='Id producto')
    product_name = models.CharField(max_length=250,  blank=True, default='', verbose_name='Nombre producto')
    product_price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Precio producto')
    product_quantity = models.IntegerField(default=0, verbose_name='Cantidad producto')
    paid_at = models.DateField(null=True, blank=True, verbose_name='Fecha pago')
    gateway = models.CharField(max_length=250, blank=True, default='', verbose_name='Medio pago')
    oder_status = models.CharField(max_length=250, blank=True, default='', verbose_name='Estado orden')
    payment_status = models.CharField(max_length=250, blank=True, default='', verbose_name='Estado pago')
    currency = models.CharField(max_length=250, blank=True, default='', verbose_name='Moneda')
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Subtotal Venta')
    discount_coupon = models.CharField(max_length=250, blank=True, default='', verbose_name='Cupón descuento')
    discount = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Descuento')
    shipping_cost_customer = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Costo envío')
    total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Total venta')
    shipping_option = models.CharField(max_length=250, blank=True, default='', verbose_name='Medio envío')
    customer_note = models.TextField(blank=True, default='', null=True, verbose_name='Nota comprador')
    owner_note = models.TextField(blank=True, default='', null=True, verbose_name='Nota vendedor')
    gateway_id = models.CharField(max_length=250, blank=True, default='', null=True, verbose_name='Identificador transacción medio pago')
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Identificador orden')

    def toJSON(self):
        item = model_to_dict(self)
        item['created_at'] = self.created_at.strftime('%Y-%m-%d')
        item['product_price'] = format(self.product_price, '.2f')
        item['paid_at'] = self.paid_at.strftime('%Y-%m-%d')
        item['subtotal'] = format(self.subtotal, '.2f')
        item['discount'] = format(self.discount, '.2f')
        item['shipping_cost_customer'] = format(self.shipping_cost_customer, '.2f')
        item['total'] = format(self.total, '.2f')
        return item

    class Meta:
        verbose_name = 'Tiendanube'
        verbose_name_plural = 'Tiendanube'
        db_table = 'tiendanube'
        ordering = ['-order_number']







#
#
# from datetime import datetime
#
# from django.db import models
# from django.forms import model_to_dict
#
# from config.settings import MEDIA_URL, STATIC_URL
# from core.erp.choices import gender_choices
# from core.erp.managers import ClientsManager
#
#
#
# class Estados(models.Model):
#     state_type = models.CharField(max_length=20, null=False, blank=False, unique=True, verbose_name='Estado')
#
#     def __str__(self):
#         return self.state_type
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         return item
#
#     class Meta:
#         verbose_name = 'Estado'
#         verbose_name_plural = 'Estados'
#         db_table = 'states'
#         ordering = ['id']
#
#
#
# class Department(models.Model):
#     name = models.CharField(max_length=250, null=False, blank=False, unique=True, verbose_name='Departamento / Partido')
#     # objects = DepartmentManager()
#     def __str__(self):
#         return self.name
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         return item
#
#     class Meta:
#         verbose_name = 'Departamento'
#         verbose_name_plural = 'Departamentos'
#         db_table = 'department'
#         ordering = ['name']
#
#
#
# class Province(models.Model):
#     name = models.CharField(max_length=250, null=False, blank=False, unique=True, verbose_name='Provincia')
#
#     # objects = ProvinceManager()
#
#     def __str__(self):
#         return self.name
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         return item
#
#     class Meta:
#         verbose_name = 'Provincia'
#         verbose_name_plural = 'Provincias'
#         db_table = 'province'
#         ordering = ['name']
#
#
#
# class Client(models.Model):
#     name = models.CharField(max_length=30, null=False, blank=True, verbose_name='Nombre')
#     surname = models.CharField(max_length=250, null=False, blank=False, verbose_name='Apellido')
#     gender = models.CharField(max_length=1, choices=gender_choices, default='O', verbose_name='Sexo')
#     mobile = models.CharField(max_length=250, blank=True, default='', verbose_name='Móbil')
#     phone = models.CharField(max_length=250, blank=True, default='', verbose_name='Teléfono')
#     email = models.EmailField(blank=True, default='', verbose_name='Email')
#     address = models.CharField(max_length=250, blank=True, default='', verbose_name='Dirección')
#     department = models.ForeignKey(Department, on_delete=models.CASCADE, verbose_name='Departamento')
#     neighborhood = models.CharField(max_length=20, blank=True, default='', verbose_name='Barrio')
#     locality = models.CharField(max_length=250, blank=True, default='', verbose_name='Localidad')
#     province = models.ForeignKey(Province, on_delete=models.CASCADE, verbose_name='Provincia')
#     postal_code = models.CharField(max_length=250, blank=True, default='', verbose_name='Código Postal')
#     others = models.TextField(blank=True, default='', verbose_name='Otros')
#     identification = models.CharField(max_length=250, blank=True, default='', verbose_name='DNI / CUIT')
#     number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número')
#
#     objects = ClientsManager()
#
#     def __str__(self):
#         return self.get_full_name()
#
#     def get_full_name(self):
#         return '{}, {}'.format(self.surname, self.name)
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         item['gender'] = {'id': self.gender, 'name': self.get_gender_display()}
#         item['department'] = self.department.toJSON()
#         item['province'] = self.province.toJSON()
#         item['full_name'] = self.get_full_name()
#         return item
#
#     class Meta:
#         verbose_name = 'Cliente'
#         verbose_name_plural = 'Clientes'
#         db_table = 'client'
#         ordering = ['surname']
#         unique_together = ('surname', 'name', 'email')
#
#
#
#
#
#
#
# class Product(models.Model):
#     name = models.CharField(max_length=250, null=False, blank=False, verbose_name='Nombre del Producto')
#     stock = models.IntegerField(default=0, verbose_name='Stock')
#     pvp = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Precio de venta')
#
#     def __str__(self):
#         return self.name
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         item['pvp'] = format(self.pvp, '.2f')
#         return item
#
#     class Meta:
#         verbose_name = 'Producto'
#         verbose_name_plural = 'Productos'
#         db_table = 'product'
#         ordering = ['id']
#
#
#
# class Sale(models.Model):
#     cli = models.ForeignKey(Client, on_delete=models.CASCADE)
#     date_joined = models.DateField(default=datetime.now)
#     subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
#     iva = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
#     total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
#
#     def __str__(self):
#         return self.cli.name
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         item['cli'] = self.cli.toJSON()
#         item['subtotal'] = format(self.subtotal, '.2f')
#         item['iva'] = format(self.iva, '.2f')
#         item['total'] = format(self.total, '.2f')
#         item['date_joined'] = self.date_joined.strftime('%Y-%m-%d')
#         item['det'] = [i.toJSON() for i in self.detsale_set.all()]  #vídeo 64 - 12:20
#         return item
#
#     def delete(self, using=None, keep_parents=False):
#         for det in self.detsale_set.all():
#             det.prod.stock += det.cant
#             det.prod.save()
#         super(Sale, self).delete()
#
#     class Meta:
#         verbose_name = 'Venta'
#         verbose_name_plural = 'Ventas'
#         db_table = 'sale'
#         ordering = ['id']
#
#
#
# class DetSale(models.Model):
#     sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
#     prod = models.ForeignKey(Product, on_delete=models.CASCADE)
#     price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
#     cant = models.IntegerField(default=0)
#     subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
#
#     def __str__(self):
#         return self.prod.name
#
#     def toJSON(self):
#         item = model_to_dict(self, exclude=['sale'])
#         item['prod'] = self.prod.toJSON()
#         item['price'] = format(self.price, '.2f')
#         item['subtotal'] = format(self.subtotal, '.2f')
#         return item
#
#     class Meta:
#         verbose_name = 'Detalle de Venta'
#         verbose_name_plural = 'Detalle de Ventas'
#         db_table = 'sale_detail'
#         ordering = ['id']
#
#
#
# class Processes(models.Model):
#     process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
#     observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
#     quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
#     state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
#     destiny = models.CharField(max_length=15, null=False, blank=False, verbose_name='Tiendas')
#     process_date_joined = models.DateField(default=datetime.now)
#     process_date_finish = models.DateField(default=datetime.now)
#
#     def __str__(self):
#         return self.process_prod.name
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         item['process_prod'] = self.process_prod.toJSON()
#         item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
#         item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
#         return item
#
#     class Meta:
#         verbose_name = 'Proceso'
#         verbose_name_plural = 'Procesos'
#         db_table = 'processes'
#         ordering = ['id']
#
#
#
# class TiendanubeApi(models.Model):
#     order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
#     created_at = models.DateField(null=True, blank=True, verbose_name='Fecha venta')
#     customer_name = models.CharField(max_length=250, blank=True, default='', verbose_name='Nombre comprador')
#     customer_email = models.EmailField(blank=True, default='', verbose_name='Email')
#     customer_identification = models.CharField(max_length=250, blank=True, default='', verbose_name='DNI / CUIT')
#     customer_phone = models.CharField(max_length=250, blank=True, default='', verbose_name='Teléfono')
#     customer_address = models.CharField(max_length=250, blank=True, default='', verbose_name='Dirección')
#     customer_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número')
#     customer_floor = models.CharField(max_length=250, blank=True, default='', verbose_name='Piso')
#     customer_locality = models.CharField(max_length=250, blank=True, default='', verbose_name='Localidad')
#     customer_city = models.CharField(max_length=250, blank=True, default='', verbose_name='Ciudad')
#     customer_zipcode = models.CharField(max_length=250, blank=True, default='', verbose_name='Código Postal')
#     customer_province = models.CharField(max_length=250, blank=True, default='', verbose_name='Provincia o estado')
#     customer_country = models.CharField(max_length=250, blank=True, default='', verbose_name='País')
#     product_id = models.CharField(max_length=250,  blank=True, default='', verbose_name='Id producto')
#     product_name = models.CharField(max_length=250,  blank=True, default='', verbose_name='Nombre producto')
#     product_price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Precio producto')
#     product_quantity = models.IntegerField(default=0, verbose_name='Cantidad producto')
#     product_image = models.TextField(null=True, blank=True, verbose_name='Imagen producto')
#     paid_at = models.DateField(null=True, blank=True, verbose_name='Fecha pago')
#     gateway = models.CharField(max_length=250, blank=True, default='', verbose_name='Medio pago')
#     oder_status = models.CharField(max_length=250, blank=True, default='', verbose_name='Estado orden')
#     payment_status = models.CharField(max_length=250, blank=True, default='', verbose_name='Estado pago')
#     currency = models.CharField(max_length=250, blank=True, default='', verbose_name='Moneda')
#     subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Subtotal Venta')
#     discount_coupon = models.CharField(max_length=250, blank=True, default='', verbose_name='Cupón descuento')
#     discount = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Descuento')
#     shipping_cost_customer = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Costo envío')
#     total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Total venta')
#     shipping_option = models.CharField(max_length=250, blank=True, default='', verbose_name='Medio envío')
#     customer_note = models.TextField(blank=True, default='', verbose_name='Nota comprador', null=True)
#     owner_note = models.TextField(blank=True, default='', verbose_name='Nota vendedor', null=True)
#     gateway_id = models.CharField(max_length=250, blank=True, default='', null=True, verbose_name='Identificador transacción medio pago')
#     order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Identificador orden')
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         item['created_at'] = self.created_at.strftime('%Y-%m-%d')
#         item['subtotal'] = format(self.subtotal, '.2f')
#         item['discount'] = format(self.discount, '.2f')
#         item['shipping_cost_customer'] = format(self.shipping_cost_customer, '.2f')
#         item['total'] = format(self.total, '.2f')
#         item['paid_at'] = self.paid_at.strftime('%Y-%m-%d')
#         item['product_price'] = format(self.product_price, '.2f')
#         item['product_image'] = self.get_image()
#         return item
#
#     def get_image(self):
#         if self.get_image:
#             return '{}{}'.format(MEDIA_URL, self.get_image)
#         return '{}{}'.format(STATIC_URL, 'image/empty.png')
#
#     class Meta:
#         verbose_name = 'Tiendanube'
#         verbose_name_plural = 'Tiendanube'
#         db_table = 'tiendanubeapi'
#         ordering = ['-order_number']
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
# #  *******************ELIMINAR ************************
# class Category(models.Model):
#     name = models.CharField(max_length=50, unique=True, verbose_name='Tipo de Prenda')
#
#     def __str__(self):
#         return self.name
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         return item
#
#     class Meta:
#         verbose_name = 'Tipo de Prenda'
#         verbose_name_plural = 'Tipo de Prendas'
#         db_table = 'category'
#         ordering = ['name']
#
# # class Product(models.Model):
# #     name = models.CharField(max_length=250, null=False, blank=False, verbose_name='Nombre del Producto')
# #     cat = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Tipo de Prenda')
# #     size = models.CharField(max_length=4, null=False, blank=False, verbose_name='Telle')
# #     image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
# #     stock = models.IntegerField(default=0, verbose_name='Stock')
# #     pvp = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Precio de venta')
# #
# #     def __str__(self):
# #         return self.name
# #
# #     def __str__(self):
# #         return self.get_full_name()
# #
# #     def get_full_name(self):
# #         return '{}, {}'.format(self.cat.name, self.name)
# #
# #     def toJSON(self):
# #         item = model_to_dict(self)
# #         item['full_name'] = '{}, {}'.format(self.cat.name, self.name)
# #         item['cat'] = self.cat.toJSON()
# #         item['image'] = self.get_image()
# #         item['pvp'] = format(self.pvp, '.2f')
# #         return item
# #
# #     def get_image(self):
# #         if self.image:
# #             return '{}{}'.format(MEDIA_URL, self.image)
# #         return '{}{}'.format(STATIC_URL, 'image/empty.png')
# #
# #     class Meta:
# #         # verbose_name = 'Producto'
# #         # verbose_name_plural = 'Productos'
# #         verbose_name = 'Prod'
# #         verbose_name_plural = 'Prods'
# #         db_table = 'product'
# #         ordering = ['id']
#
#
# # ESTA TABLA NO LA USO PORQUE TIENE CAMPO QUE NO DEBERÍAN IR
# class Tiendanube(models.Model):
#     order_number = models.CharField(max_length=15, blank=True, default='', verbose_name='Número orden')
#     email = models.EmailField(blank=True, default='', verbose_name='Email')
#     date_today = models.DateField(null=True, blank=True, verbose_name='Fecha Venta')
#     oder_status = models.CharField(max_length=15, blank=True, default='', verbose_name='Estado orden')
#     pay_status = models.CharField(max_length=15, blank=True, default='', verbose_name='Estado pago')
#     delivery_status = models.CharField(max_length=20, blank=True, default='', verbose_name='Estado envío')
#     currency = models.CharField(max_length=3, blank=True, default='', verbose_name='Moneda')
#     product_subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Subtotal producto')
#     discount = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Descuento')
#     delivery_cost = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Costo envío')
#     total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Total')
#     name_buyer = models.CharField(max_length=50, blank=True, default='', verbose_name='Nombre comprador')
#     dni_cuit = models.CharField(max_length=11, blank=True, default='', verbose_name='DNI / CUIT')
#     phone = models.CharField(max_length=20, blank=True, default='', verbose_name='Teléfono')
#     delivery_name = models.CharField(max_length=50, blank=True, default='', verbose_name='Nombre para envío')
#     phone_delivery = models.CharField(max_length=20, blank=True, default='', verbose_name='Teléfono para envío')
#     address = models.CharField(max_length=30, blank=True, default='', verbose_name='Dirección')
#     number = models.CharField(max_length=5, blank=True, default='', verbose_name='Número')
#     floor = models.CharField(max_length=30, blank=True, default='', verbose_name='Piso')
#     locality = models.CharField(max_length=30, blank=True, default='', verbose_name='Localidad')
#     city = models.CharField(max_length=30, blank=True, default='', verbose_name='Ciudad')
#     postal_code = models.CharField(max_length=8, blank=True, default='', verbose_name='Código Postal')
#     province = models.CharField(max_length=30, blank=True, default='', verbose_name='Provincia o estado')
#     country = models.CharField(max_length=30, blank=True, default='', verbose_name='País')
#     average_costs = models.CharField(max_length=100, blank=True, default='', verbose_name='Medio envío')
#     retirement_branch = models.CharField(max_length=100, blank=True, default='', verbose_name='Sucursal retiro')
#     pay_method = models.CharField(max_length=30, blank=True, default='', verbose_name='Medio pago')
#     discount_coupon = models.CharField(max_length=50, blank=True, default='', verbose_name='Cupón descuento')
#     buyer_notes = models.TextField(blank=True, default='', verbose_name='Notas comprador')
#     seller_notes = models.TextField(blank=True, default='', verbose_name='Notas vendedor')
#     date_pay = models.DateField(null=True, blank=True, verbose_name='Fecha pago')
#     date_send = models.DateField(null=True, blank=True, verbose_name='Fecha envío')
#     product_name = models.CharField(max_length=150,  blank=True, default='', verbose_name='Nombre producto')
#     price_product = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Precio producto')
#     cant = models.IntegerField(default=0, verbose_name='Cantidad')
#     sku = models.CharField(max_length=20, blank=True, default='', verbose_name='SKU')
#     channel = models.CharField(max_length=30, blank=True, default='', verbose_name='Canal')
#     shipping_tracking_code = models.CharField(max_length=50, blank=True, default='', verbose_name='Código tracking envío')
#     half_payment_identifier = models.CharField(max_length=20, blank=True, default='', verbose_name='Identificador transacción medio pago')
#     order_identifier = models.CharField(max_length=20, blank=True, default='', verbose_name='Identificador orden')
#     physical_product = models.CharField(max_length=2, blank=True, default='', verbose_name='Producto físico')
#
#
#     def toJSON(self):
#         item = model_to_dict(self)
#         item['date_today'] = self.date_today.strftime('%Y-%m-%d')
#         item['product_subtotal'] = format(self.product_subtotal, '.2f')
#         item['discount'] = format(self.discount, '.2f')
#         item['delivery_cost'] = format(self.delivery_cost, '.2f')
#         item['total'] = format(self.total, '.2f')
#         item['date_pay'] = self.date_pay.strftime('%Y-%m-%d')
#         item['price_product'] = format(self.price_product, '.2f')
#         item['image'] = self.get_image()
#         return item
#
#     class Meta:
#         verbose_name = 'Tiendanube'
#         verbose_name_plural = 'Tiendanube'
#         ordering = ['order_number']
