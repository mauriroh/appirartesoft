from django.contrib import admin

# STATES
from core.erp.models import *


class ModelAdminEstados(admin.ModelAdmin):
    list_display = (
        'id',
        'state_type',
    )
    # Buscador
    search_fields = ('state_type',)
    ordering = ['id']
admin.site.register(Estados, ModelAdminEstados)


# DEPARTMENT
class ModelAdminDepartment(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )
    # Buscador
    search_fields = ('name',)
    ordering = ['name']
    paginate_by = 10
admin.site.register(Department, ModelAdminDepartment)


# LOCALITY
class ModelAdminProvince(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )
    # Buscador
    search_fields = ('name',)
    ordering = ['name']
    paginate_by = 10
admin.site.register(Province, ModelAdminProvince)


# CLIENTS
class ModeladoVistaCliente(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'email',
        'identification',
        'phone',
        'address',
        'number',
        'floor',
        'locality',
        'department',
        'province',
        'postal_code',
    )
    #Funciones
    # def full_name(self, obj):
    #     return obj.surname + ' ' + obj.name
    #Otros filtrados
    search_fields = ('name', 'email',)
    list_filter = ('department',)
    ordering = ['name']
    paginate_by=10
admin.site.register(Client, ModeladoVistaCliente)


# PRODUCTS
class ModelAdminProduct(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'stock',
        'pvp',
        'productimageid'
    )
    # Buscador
    search_fields = ('name',)
    ordering = ['name']
    paginate_by = 10
admin.site.register(Product, ModelAdminProduct)


# SALE
class ModelAdminSale(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'cli',
        'date_joined',
        'subtotal',
        'iva',
        'total',
    )

    search_fields = ('order_number', 'date_joined', 'cli',)
    ordering = ['-id']
    paginate_by = 10
admin.site.register(Sale, ModelAdminSale)


# SALE DETAIL
class ModelAdminSaleDetail(admin.ModelAdmin):
    list_display = (
        'id',
        'sale',
        'prod',
        'price',
        'cant',
        'subtotal',
    )

    search_fields = ('prod', 'sale',)
    ordering = ['-sale']
    paginate_by = 10
admin.site.register(DetSale, ModelAdminSaleDetail)


################## ALL PROCESS ##################
# PROCESS
class ModeladoVistaProcesses(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'product_id',
        'process_prod',
        'quantity',
        'state',
        'destiny',
        'process_date_joined',
        'process_date_finish',
        'order_id',
        'state_dressmaking',
    )
    search_fields = ('process_prod',)
    ordering = ['-order_id']
    paginate_by=10
admin.site.register(Processes, ModeladoVistaProcesses)


# 01 - DESING
class ModeladoVistaProcessesDesign(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'product_id',
        'process_prod',
        'quantity',
        'state',
        'destiny',
        'process_date_joined',
        'process_date_finish',
        'order_id',
    )
    search_fields = ('process_prod',)
    ordering = ['-order_id']
    paginate_by=10
admin.site.register(ProcessesDesign, ModeladoVistaProcessesDesign)


# 02 -LAYOUT
class ModelViewProcessesLayout(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'product_id',
        'process_prod',
        'quantity',
        'state',
        'destiny',
        'process_date_joined',
        'process_date_finish',
        'order_id',
    )
    search_fields = ('process_prod',)
    ordering = ['-order_id']
    paginate_by=10
admin.site.register(ProcessesLayout, ModelViewProcessesLayout)


# 03 - PRINT
class ModelViewProcessesPrint(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'product_id',
        'process_prod',
        'quantity',
        'state',
        'destiny',
        'process_date_joined',
        'process_date_finish',
        'order_id',
    )
    search_fields = ('process_prod',)
    ordering = ['-order_id']
    paginate_by=10
admin.site.register(ProcessesPrint, ModelViewProcessesPrint)


# 04 - IRONING
class ModelViewProcessesIroning(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'product_id',
        'process_prod',
        'quantity',
        'state',
        'destiny',
        'process_date_joined',
        'process_date_finish',
        'order_id',
    )
    search_fields = ('process_prod',)
    ordering = ['-order_id']
    paginate_by=10
admin.site.register(ProcessesIroning, ModelViewProcessesIroning)


# 05 - MAKING
class ModelViewProcessesMaking(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'product_id',
        'process_prod',
        'quantity',
        'state',
        'state_dressmaking',
        'destiny',
        'process_date_joined',
        'process_date_finish',
        'order_id',
    )
    search_fields = ('process_prod',)
    ordering = ['-order_id']
    paginate_by=10
admin.site.register(ProcessesMaking, ModelViewProcessesMaking)


# 06 - PACK
class ModelViewProcessesPack(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'product_id',
        'process_prod',
        'quantity',
        'state',
        'destiny',
        'process_date_joined',
        'process_date_finish',
        'order_id',
    )
    search_fields = ('process_prod',)
    ordering = ['-order_id']
    paginate_by=10
admin.site.register(ProcessesPack, ModelViewProcessesPack)


# 07 - DISPATCH
class ModelViewProcessesDispatch(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'product_id',
        'process_prod',
        'quantity',
        'state',
        'destiny',
        'process_date_joined',
        'process_date_finish',
        'order_id',
    )
    search_fields = ('process_prod',)
    ordering = ['-order_id']
    paginate_by=10
admin.site.register(ProcessesDispatch, ModelViewProcessesDispatch)


# 10 - PENDING
class ModelViewProcessesPending(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'product_id',
        'process_prod',
        'quantity',
        'state',
        'destiny',
        'process_date_joined',
        'process_date_finish',
        'order_id',
    )
    search_fields = ('process_prod',)
    ordering = ['-order_id']
    paginate_by = 10
admin.site.register(ProcessesPending, ModelViewProcessesPending)


################## TIENDANUBE ##################
class ModelAdminTiendanube(admin.ModelAdmin):
    list_display = (
        'id',
        'order_number',
        'created_at',
        'customer_name',
        'customer_email',
        'customer_identification',
        'customer_phone',
        'customer_address',
        'customer_number',
        'customer_floor',
        'customer_locality',
        'customer_city',
        'customer_zipcode',
        'customer_province',
        'customer_country',
        'product_id',
        'product_name',
        'product_price',
        'product_quantity',
        'product_image',
        'paid_at',
        'gateway',
        'oder_status',
        'payment_status',
        'currency',
        'subtotal',
        'discount_coupon',
        'discount',
        'shipping_cost_customer',
        'total',
        'shipping_option',
        'customer_note',
        'owner_note',
        'gateway_id',
        'order_id',
    )

    search_fields = ('order_number', 'product_name', 'customer_name',)
    # list_filter = ('order_number',)
    ordering = ['-order_id']
    paginate_by = 10
admin.site.register(TiendanubeApi, ModelAdminTiendanube)


################# SERVICES STATE ##################
class ModelViewServicesState(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )
    ordering = ['id']
admin.site.register(ServicesState, ModelViewServicesState)


#################  SERVICES PAY ##################
class ModelViewServices(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'client_number',
        'date_joined',
        'date_finish',
        'total',
        'services_state',
    )
    search_fields = ('name',)
    ordering = ['-id']
    paginate_by=10
admin.site.register(Services, ModelViewServices)


#################  DRESSMAKING ##################
class ModelAdminDressmaking(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )
    # Buscador
    search_fields = ('name',)
    ordering = ['name']
    paginate_by = 10
admin.site.register(Dressmaking, ModelAdminDressmaking)
