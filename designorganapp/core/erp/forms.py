from datetime import datetime

from django import forms
from django.forms import ModelForm

from core.erp.models import *
# Functions
from .functions import (
    alphabet_category_name,
    alphabet_name,
    alphabet_number_size,
    alphabet_surname,
)


####################################
#              ESTADOS             #
####################################
class StatesForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['state_type'].widget.attrs['autofocus'] = True

    class Meta:
        model = Estados
        fields = '__all__'
        widgets = {
            'state_type': forms.TextInput(
                attrs={
                    'placeholder': 'States',
                    'autocomplete': 'off',
                }
            ),

        }


########################################
# FORM LOCALITY / DEPARTMENT / PARTIDO #
########################################
class DepartmentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['autofocus'] = True

    class Meta:
        model = Department
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'placeholder': 'Department | City',
                    'autocomplete': 'off',
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#             PROVINCIAS           #
####################################
class ProvinceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['autofocus'] = True

    class Meta:
        model = Province
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'placeholder': 'Province | State',
                    'autocomplete': 'off',
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#             CLIENTES             #
####################################
class ClientForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['autofocus'] = True

    class Meta:
        model = Client
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'placeholder': 'Name & Surname',
                    'class': 'form-control',
                }
            ),
            'email': forms.EmailInput(
                attrs={
                    'placeholder': 'Email',
                    'class': 'form-control',
                }
            ),
            'identification': forms.TextInput(
                attrs={
                    'placeholder': 'DNI | CUIT',
                    'class': 'form-control',
                }
            ),
            'phone': forms.TextInput(
                attrs={
                    'placeholder': 'Phone',
                    'class': 'form-control',
                }
            ),
            'address': forms.TextInput(
                attrs={
                    'placeholder': 'Address',
                    'class': 'form-control',
                }
            ),
            'number': forms.TextInput(
                attrs={
                    'placeholder': 'Number',
                    'class': 'form-control',
                }
            ),
            'floor': forms.TextInput(
                attrs={
                    'placeholder': 'Floor',
                    'class': 'form-control',
                }
            ),
            'locality': forms.TextInput(
                attrs={
                    'placeholder': 'Locality',
                    'class': 'form-control',
                }
            ),
            'postal_code': forms.TextInput(
                attrs={
                    'placeholder': 'Postal Code',
                    'class': 'form-control',
                }
            ),
            'others': forms.Textarea(
                attrs={
                    'placeholder': 'Others data',
                    'rows': '3',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#             PRODUCTOS            #
####################################
class ProductForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['autofocus'] = True

    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'placeholder': 'Product Name',
                    'autocomplete': 'on',
                }
            ),
            'stock': forms.TextInput(
                attrs={
                    'placeholder': 'Stock',
                    'autocomplete': 'off',
                }
            ),
            'pvp': forms.TextInput(
                attrs={
                    'placeholder': 'Sale Price',
                    'autocomplete': 'off',
                }
            )
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#               VENTAS             #
####################################
class SaleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['cli'].queryset = Client.objects.none()# Este devuelve en el campo selección de cliente lo espacios
                                                            # vacios para poder hacer la búsqueda con mayor datos al
                                                            # momento de tipear en el select2 en la vista de ventas
    class Meta:
        model = Sale
        fields = '__all__'
        widgets = {
            'cli': forms.Select(attrs={
                'class': 'custom-select select2',
            }),
            'date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'date_joined',
                    'data-target': '#date_joined',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'iva': forms.TextInput(
                attrs={
                    'class': 'form-control text-center',
                }
            ),
            'subtotal': forms.TextInput(
                attrs={
                    'readonly': True,
                    'class': 'form-control',
                }
            ),
            'total': forms.TextInput(
                attrs={
                    'readonly': True,
                    'class': 'form-control',
                }
            )
        }



####################################
#          ALL PRODUCCION          #
#             PROCESOS             #
####################################
class ProcessesForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Processes
        fields = '__all__'
        widgets = {
            'process_prod': forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'observation': forms.Textarea(
                attrs={
                    'rows': '2',
                    'placeholder': 'Observations',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
            'quantity': forms.TextInput(
                attrs={
                    'placeholder': 'Quantity',
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'type': 'number',
                }
            ),
            'state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'destiny': forms.TextInput(
                attrs={
                    'placeholder': 'Shop',
                    'class': 'form-control',
                    'style': 'width: 100%',
                }
            ),
            'process_date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_joined',
                    'data-target': '#process_date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'process_date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_finish',
                    'data-target': '#process_date_finish',
                    'data-toggle': 'datetimepicker'
                }
            ),
        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#           01 - DISENIO            #
####################################
class ProcessesDesignForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.fields['process_prod'].queryset = Product.objects.none()

    class Meta:
        model = ProcessesDesign
        fields = '__all__'
        widgets = {
            'process_prod': forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'observation': forms.Textarea(
                attrs={
                    'rows': '2',
                    'placeholder': 'Observations',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
            'quantity': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'destiny': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'process_date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_joined',
                    'data-target': '#process_date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'process_date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_finish',
                    'data-target': '#process_date_finish',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'order_number': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 35%',
                    'readonly': True,
                }
            ),
        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#       02 - MAQUETERIA            #
####################################
class ProcessesLayoutForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.fields['process_prod'].queryset = Product.objects.none()

    class Meta:
        model = ProcessesLayout
        fields = '__all__'
        widgets = {
            'process_prod': forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'observation': forms.Textarea(
                attrs={
                    'rows': '2',
                    'placeholder': 'Observations',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
            'quantity': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'destiny': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'process_date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_joined',
                    'data-target': '#process_date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'process_date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_finish',
                    'data-target': '#process_date_finish',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'order_number': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 35%',
                    'readonly': True,
                }
            ),
        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#       03 - IMPRESION             #
####################################
class ProcessesPrintForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.fields['process_prod'].queryset = Product.objects.none()

    class Meta:
        model = ProcessesPrint
        fields = '__all__'
        widgets = {
            'process_prod': forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'observation': forms.Textarea(
                attrs={
                    'rows': '2',
                    'placeholder': 'Observations',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
            'quantity': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'destiny': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'process_date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_joined',
                    'data-target': '#process_date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'process_date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_finish',
                    'data-target': '#process_date_finish',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'order_number': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 35%',
                    'readonly': True,
                }
            ),
        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


####################################
#        04 - PLANCHADO            #
####################################
class ProcessesIroningForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.fields['process_prod'].queryset = Product.objects.none()

    class Meta:
        model = ProcessesIroning
        model2 = Dressmaking
        fields = '__all__'
        widgets = {
            'process_prod': forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'observation': forms.Textarea(
                attrs={
                    'rows': '2',
                    'placeholder': 'Observations',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
            'quantity': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'name': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),

            'destiny': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'process_date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_joined',
                    'data-target': '#process_date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'process_date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_finish',
                    'data-target': '#process_date_finish',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'order_number': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 35%',
                    'readonly': True,
                }
            ),
        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#     05 - CONFECCIONISTAS         #
####################################
class ProcessesMakingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.fields['process_prod'].queryset = Product.objects.none()

    class Meta:
        model = ProcessesMaking
        fields = '__all__'
        widgets = {
            'process_prod': forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'observation': forms.Textarea(
                attrs={
                    'rows': '2',
                    'placeholder': 'Observations',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
            'quantity': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'destiny': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'process_date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_joined',
                    'data-target': '#process_date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'process_date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_finish',
                    'data-target': '#process_date_finish',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'order_number': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 35%',
                    'readonly': True,
                }
            ),
        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#       06 - PAQUETERIA            #
####################################
class ProcessesPackForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.fields['process_prod'].queryset = Product.objects.none()

    class Meta:
        model = ProcessesPack
        fields = '__all__'
        widgets = {
            'process_prod': forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'observation': forms.Textarea(
                attrs={
                    'rows': '2',
                    'placeholder': 'Observations',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
            'quantity': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'destiny': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'process_date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_joined',
                    'data-target': '#process_date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'process_date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_finish',
                    'data-target': '#process_date_finish',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'order_number': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 35%',
                    'readonly': True,
                }
            ),
        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#        07 - DESPACHO             #
####################################
class ProcessesDispatchForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.fields['process_prod'].queryset = Product.objects.none()

    class Meta:
        model = ProcessesDispatch
        fields = '__all__'
        widgets = {
            'process_prod': forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'observation': forms.Textarea(
                attrs={
                    'rows': '2',
                    'placeholder': 'Observations',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
            'quantity': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'destiny': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'process_date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_joined',
                    'data-target': '#process_date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'process_date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_finish',
                    'data-target': '#process_date_finish',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'order_number': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 35%',
                    'readonly': True,
                }
            ),
        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#         10 - PENDIENTES          #
####################################
class ProcessesPendingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.fields['process_prod'].queryset = Product.objects.none()

    class Meta:
        model = ProcessesPending
        fields = '__all__'
        widgets = {
            'process_prod': forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'observation': forms.Textarea(
                attrs={
                    'rows': '2',
                    'placeholder': 'Observations',
                    'class': 'form-control',
                    'style': 'width: 100%'
                }
            ),
            'quantity': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'state_dressmaking': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),
            'destiny': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 100%',
                    'readonly': True,
                }
            ),
            'process_date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_joined',
                    'data-target': '#process_date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'process_date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'process_date_finish',
                    'data-target': '#process_date_finish',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'order_number': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'style': 'width: 35%',
                    'readonly': True,
                }
            ),
        }

        # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#            TIENDANUBE          #
####################################
class TiendanubeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['order_number'].widget.attrs['autofocus'] = True

    class Meta:
        model = TiendanubeApi
        fields = '__all__'
        widgets = {
            'order_number': forms.TextInput(
                attrs={
                    'readonly': True,
                    'class': 'form-control',
                }
            ),
            'created_at': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'id': 'date_today',
                    'data-target': '#date_today',
                }
            ),
            'customer_name': forms.TextInput(
                attrs={
                    'placeholder': 'Customer Name & Surname',
                    'class': 'form-control',
                }
            ),
            'customer_email': forms.TextInput(
                attrs={
                    'placeholder': 'Email',
                    'class': 'form-control',
                }
            ),
            'customer_identification': forms.TextInput(
                attrs={
                    'placeholder': 'DNI | CUIT',
                    'class': 'form-control',
                }
            ),
            'customer_phone': forms.TextInput(
                attrs={
                    'placeholder': 'Phone',
                    'class': 'form-control',
                }
            ),
            'customer_address': forms.TextInput(
                attrs={
                    'placeholder': 'Address',
                    'class': 'form-control',
                }
            ),
            'customer_number': forms.TextInput(
                attrs={
                    'placeholder': 'Number',
                    'class': 'form-control',
                }
            ),
            'customer_floor': forms.TextInput(
                attrs={
                    'placeholder': 'Floor',
                    'class': 'form-control',
                }
            ),
            'customer_locality': forms.TextInput(
                attrs={
                    'placeholder': 'Locality',
                    'class': 'form-control',
                }
            ),
            'customer_city': forms.TextInput(
                attrs={
                    'placeholder': 'City | Department',
                    'class': 'form-control',
                }
            ),
            'customer_zipcode': forms.TextInput(
                attrs={
                    'placeholder': 'Postal Code',
                    'class': 'form-control',
                }
            ),
            'customer_province': forms.TextInput(
                attrs={
                    'placeholder': 'Province',
                    'class': 'form-control',
                }
            ),
            'customer_country': forms.TextInput(
                attrs={
                    'placeholder': 'Country',
                    'class': 'form-control',
                }
            ),
            'product_name': forms.TextInput(
                attrs={
                    'placeholder': 'Product Name',
                    'class': 'form-control',
                }
            ),
            'product_price': forms.TextInput(
                attrs={
                    'placeholder': 'Product Price',
                    'class': 'form-control',
                }
            ),
            'product_quantity': forms.TextInput(
                attrs={
                    'placeholder': 'Product Quantity',
                    'class': 'form-control',
                }
            ),
            'paid_at': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'id': 'date_pay',
                    'data-target': '#date_pay',
                }
            ),
            'gateway': forms.TextInput(
                attrs={
                    'placeholder': 'Payment Method',
                    'class': 'form-control',
                }
            ),
            'oder_status': forms.TextInput(
                attrs={
                    'placeholder': 'Oder Status',
                    'class': 'form-control',
                }
            ),
            'payment_status': forms.TextInput(
                attrs={
                    'placeholder': 'Payment Status',
                    'class': 'form-control',
                }
            ),
            'currency': forms.TextInput(
                attrs={
                    'placeholder': 'Currency',
                    'class': 'form-control',
                }
            ),
            'subtotal': forms.TextInput(
                attrs={
                    'placeholder': '0.0',
                    'readonly': True,
                    'class': 'form-control',
                }
            ),
            'discount_coupon': forms.TextInput(
                attrs={
                    'placeholder': '0.0',
                    'class': 'form-control',
                }
            ),
            'discount': forms.TextInput(
                attrs={
                    'placeholder': '0.0',
                    'class': 'form-control',
                }
            ),
            'shipping_cost_customer': forms.TextInput(
                attrs={
                    'placeholder': '0.0',
                    'class': 'form-control',
                }
            ),
            'total': forms.TextInput(
                attrs={
                    'readonly': True,
                    'class': 'form-control',
                }
            ),
            'shipping_option': forms.TextInput(
                attrs={
                    'placeholder': 'Shipping Option',
                    'class': 'form-control',
                }
            ),
            'customer_note': forms.Textarea(
                attrs={
                    'rows': '2',
                    'class': 'form-control',
                }
            ),
            'owner_note': forms.Textarea(
                attrs={
                    'rows': '2',
                    'class': 'form-control',
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



####################################
#        PAGO DE SERVICIOS         #
####################################
class ServicesStateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['autofocus'] = True

    class Meta:
        model = ServicesState
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'placeholder': 'Services State',
                }
            ),

        }



####################################
#       PAGO DE SERVICIOS          #
####################################
class ServicesForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['autofocus'] = True

    class Meta:
        model = Services
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'placeholder': 'Services Name',
                    'class': 'form-control',
                }
            ),
            'client_number': forms.TextInput(
                attrs={
                    'placeholder': 'Client Number | Account number',
                    'class': 'form-control',
                }
            ),
            'date_joined': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'date_joined',
                    'data-target': '#date_joined',
                    'data-toggle': 'datetimepicker',
                    'readonly': True,
                }
            ),
            'date_finish': forms.DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'date_finish',
                    'data-target': '#date_finish',
                    'data-toggle': 'datetimepicker',
                }
            ),
            'total': forms.TextInput(
                attrs={
                    'placeholder': '0.0',
                    'class': 'form-control',
                }
            ),
            'services_state': forms.Select(
                attrs={
                    'class': 'custom-select select2',
                    'style': 'width: 100%'
                }
            ),

        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data



########################################
#        TIPO DE CONFECCIONISTAS       #
########################################
class DressmakingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['autofocus'] = True

    class Meta:
        model = Dressmaking
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'placeholder': 'Dressmaking Name',
                    'autocomplete': 'off',
                }
            ),

        }

    # GUARDADO DE DATOS
    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                instance = form.save()
                data = instance.toJSON()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data




# VALIDACIONES
    # def clean_surname(self):
    #     surname = self.cleaned_data["surname"]
    #     alfabeto = surname.upper()
    #     flag = alphabet_surname(alfabeto)
    #     if flag == True:
    #         return alfabeto
    #     else:
    #         raise forms.ValidationError('Apellido: Solo se permiten letras o espacios.')
    #
    # def clean_name(self):
    #     name = self.cleaned_data["name"]
    #     alfabeto = name.upper()
    #     flag = alphabet_name(alfabeto)
    #     if flag == True:
    #         return alfabeto
    #     else:
    #         raise forms.ValidationError('Nombre: Solo se permiten letras o espacios.')
    #
    #         # def clean_mobile(self):
    #
    # #     mobile = self.cleaned_data["mobile"]
    # #     if len(mobile) <=20:
    # #         return mobile
    # #     else:
    # #         raise forms.ValidationError( 'Móvil: Ha superdo los 20 caractéres. ')
    #
    # # def clean_phone(self):
    # #     phone=self.cleaned_data[ "phone"]
    # #     if len(phone) <=20:
    # #         return phone
    # #     else:
    # #         raise forms.ValidationError( 'Teléfono: Ha superdo los 20 caractéres. ')
    #
    # # def clean_email(self):
    # #     email= self.cleaned_data["email"]
    # #     return email
    #
    # # def clean_postal_code(self):
    # #     codigo_postal = self.cleaned_data["postal_code"]
    # #     flag = cod_postal(codigo_postal)
    # #     if flag == True:
    # #         return codigo_postal
    # #     else:
    # #         raise forms.ValidationError('Código Postal: Solo se permiten números')
    #
    # def clean_address(self):
    #     direccion = self.cleaned_data["address"]
    #     direccion_upper = direccion.upper()
    #     return direccion_upper
    #
    # def clean_neighborhood(self):
    #     barrio = self.cleaned_data["neighborhood"]
    #     barrio_upper = barrio.upper()
    #     return barrio_upper
    #
    # def clean_locality(self):
    #     locality = self.cleaned_data["locality"]
    #     locality_upper = locality.upper()
    #     return locality_upper