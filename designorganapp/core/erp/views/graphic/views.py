from datetime import datetime
from django.contrib.auth.mixins import LoginRequiredMixin
from core.erp.mixins import ValidatePermissionRequiredMixin
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from core.erp.models import Sale, TiendanubeApi
# REPORTS
from django.urls import reverse_lazy
#from core.reports.forms import ReportForm
# REPORTS



class GraphicDetailSalesAllView(LoginRequiredMixin, ValidatePermissionRequiredMixin, TemplateView):
    template_name = 'graphic/detailsalesall.html'
    success_url_reports = reverse_lazy('reports_app:report-sale')
    permission_required = 'view_sale'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'get_graph_sales_year_month':
                data = {
                    'name': 'Porcentaje de ventas',
                    'showInLegend': False,
                    'colorByPoint': True,
                    'data': self.get_graph_sales_year_month()
                }
            # elif action == 'get_graph_sales_products_year_month':
            #     data = {
            #         'name': 'Porcentaje',
            #         'colorByPoint': True,
            #         'data': self.get_graph_sales_products_year_month(),
            #     }
            # elif action == 'get_graph_online':
            #     data = {'y': randint(1, 100)}
            #     print(data)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_graph_sales_year_month(self):
        data = []
        try:
            year = datetime.now().year
            for m in range(1, 13):
                total = Sale.objects.filter(date_joined__year=year, date_joined__month=m).aggregate(r=Coalesce(Sum('total'), 0)).get('r')
                # if total > 0:
                data.append(float(total))
        except:
            pass
        return data

    # def get_graph_sales_products_year_month(self):
    #     data = []
    #     year = datetime.now().year
    #     month = datetime.now().month
    #     try:
    #         for p in Product.objects.all():
    #             total = DetSale.objects.filter(sale__date_joined__year=year, sale__date_joined__month=month, prod_id=p.id).aggregate(r=Coalesce(Sum('subtotal'), 0)).get('r')
    #             if total > 0:
    #                 data.append({
    #                     'name': p.name,
    #                     'y': float(total)
    #                 })
    #     except:
    #         pass
    #     return data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Gráfico de Ventas Anual'
        context['panel'] = 'Panel de administrador'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['list_url_reports'] = self.success_url_reports
        context['graph_sales_year_month'] = self.get_graph_sales_year_month()
        return context


class GraphicTiendanubeOneSalesVie(LoginRequiredMixin, ValidatePermissionRequiredMixin, TemplateView):
    template_name = 'graphic/tiendanubeonesales.html'
    permission_required = 'view_tiendanube'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'get_graph_tiendanube_sales_year_month':
                data = {
                    'name': 'Porcentaje de ventas',
                    'showInLegend': False,
                    'colorByPoint': True,
                    'data': self.get_graph_tiendanube_sales_year_month()
                }
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_graph_tiendanube_sales_year_month(self):
        data = []
        try:
            year = datetime.now().year
            for m in range(1, 13):
                total = TiendanubeApi.objects.filter(created_at__year=year, created_at__month=m).aggregate(r=Coalesce(Sum('total'), 0)).get('r')
                # if total > 0:
                data.append(float(total))
        except:
            pass
        return data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Gráfico de Ventas Anual de Tiendanube 1'
        context['panel'] = 'Panel de administrador'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['get_graph_tiendanube_sales_year_month'] = self.get_graph_tiendanube_sales_year_month()
        return context

