# from django.http import HttpResponseRedirect
# from django.template.loader import get_template
# from xhtml2pdf import pisa
# from django.views.generic import DeleteView
# from django.db.models import Q
# from django.http import HttpResponse
# from django.conf import settings

import datetime
import json
import os

from django.contrib.auth.mixins import LoginRequiredMixin
from core.erp.mixins import ValidatePermissionRequiredMixin
from datetime import datetime, timedelta
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, ListView, UpdateView, DetailView

from core.erp.forms import ProcessesForm
from core.erp.models import Processes, ProcessesDesign, ProcessesLayout, ProcessesPrint, ProcessesIroning, ProcessesMaking, ProcessesPack, ProcessesDispatch, Product, ProcessesPending, Dressmaking



class ProcessesListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Processes
    template_name = 'processes/list.html'
    permission_required = 'view_processes'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        var = request.user
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Processes.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cont_design = 0
        cont_pending = 0
        cont_layout = 0
        cont_print = 0
        cont_ironing = 0
        cont_making = 0
        cont_pack = 0
        cont_dispatch = 0
        for i in Processes.objects.all():
            if i.state.__str__() == 'Diseño':
                cont_design += 1
            elif i.state.__str__() == 'Pendiente':
                cont_pending += 1
            elif i.state.__str__() == 'Maquetería':
                cont_layout += 1
            elif i.state.__str__() == 'Impresión':
                cont_print += 1
            elif i.state.__str__() == 'Planchado':
                cont_ironing += 1
            elif i.state.__str__() == 'Confección':
                cont_making += 1
            elif i.state.__str__() == 'Paquetería':
                cont_pack += 1
            elif i.state.__str__() == 'Despacho':
                cont_dispatch += 1
        context['Diseño'] = cont_design
        context['Pendiente'] = cont_pending
        context['Maquetería'] = cont_layout
        context['Impresión'] = cont_print
        context['Planchado'] = cont_ironing
        context['Confección'] = cont_making
        context['Paquetería'] = cont_pack
        context['Despacho'] = cont_dispatch
        context['title'] = 'Listado de Producción'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['create_url'] = reverse_lazy('productos_app:processes-create')
        context['list_url'] = reverse_lazy('productos_app:processes-list')
        context['entity'] = 'Procesos'
        return context


class ProcessesCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Processes
    model2 = ProcessesDesign
    model3 = Product
    form_class = ProcessesForm
    template_name = 'processes/create.html'
    success_url = reverse_lazy('productos_app:processes-list')
    permission_required = 'add_processes'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # Convierte el texto en fecha y extrae solo el año/mes/día
    def convert_date(self, process_date_joined):
        if process_date_joined is not None:
            index = process_date_joined.find('T')
            return datetime.strptime(process_date_joined[0:index], '%Y-%m-%d')
        return datetime.now().date()

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            # # SEARCH ORDER NUMBER
            if action == 'search_products':
                data = []
                prods = Product.objects.filter(name__icontains=request.POST['term'])    #[0:10]
                for i in prods:
                    item = i.toJSON()
                    item['value'] = i.name
                    # item['text'] = i.name
                    data.append(item)
            elif action == 'add':
                with transaction.atomic():
                    proces = json.loads(request.POST['proces'])
                    processes = Processes()
                    processes.process_prod_id = int(proces['product_id'])
                    processes.observation = proces['observation']
                    processes.quantity = int(proces['quantity'])
                    processes.state_id = proces['state']
                    processes.destiny = proces['destiny']
                    processes.process_date_joined = proces['process_date_joined']
                    if proces['process_date_joined'] == proces['process_date_finish']:
                        processes.process_date_finish = datetime.now() + timedelta(7)
                    else:
                        processes.process_date_finish = proces['process_date_finish']
                    processes.product_id = proces['product_id']
                    processes.order_number = ''
                    for p in Product.objects.filter(processes__product_id=int(proces['product_id'])):
                        processes.image = p.image
                    processes.state_dressmaking_id = proces['state_dressmaking']
                    # pdesing.state_dressmaking_id = dress_making[0].id
                    processes.save()
                    processes.order_number = "LC01-" + str(processes.id)
                    processes.save()

                    # 01 PROCESO DISEÑOS
                    if proces['state'] == '1':
                        pdesing = ProcessesDesign()
                        pdesing.process_prod_id = int(proces['product_id'])
                        pdesing.observation = proces['observation']
                        pdesing.quantity = int(proces['quantity'])
                        pdesing.state_id = proces['state']
                        pdesing.destiny = proces['destiny']
                        pdesing.process_date_joined = proces['process_date_joined']
                        if proces['process_date_joined'] == proces['process_date_finish']:
                            pdesing.process_date_finish = datetime.now() + timedelta(7)
                        else:
                            pdesing.process_date_finish = proces['process_date_finish']
                        pdesing.product_id = proces['product_id']
                        pdesing.order_number = "LC01-" + str(processes.id)
                        for p in Product.objects.filter(processes__product_id=int(proces['product_id'])):
                            pdesing.image = p.image
                        pdesing.state_dressmaking_id = proces['state_dressmaking']
                        pdesing.save()

                    # 02 PROCESO MAQUETERIA
                    elif proces['state'] == '2':
                        playout = ProcessesLayout()
                        playout.process_prod_id = int(proces['product_id'])
                        playout.observation = proces['observation']
                        playout.quantity = int(proces['quantity'])
                        playout.state_id = proces['state']
                        playout.destiny = proces['destiny']
                        playout.process_date_joined = proces['process_date_joined']
                        if proces['process_date_joined'] == proces['process_date_finish']:
                            playout.process_date_finish = datetime.now() + timedelta(7)
                        else:
                            playout.process_date_finish = proces['process_date_finish']
                        playout.product_id = proces['product_id']
                        playout.order_number = "LC01-" + str(processes.id)
                        for p in Product.objects.filter(processes__product_id=int(proces['product_id'])):
                            playout.image = p.image
                        playout.state_dressmaking_id = proces['state_dressmaking']
                        playout.save()

                    # 03 PROCESO IMPRESION
                    elif proces['state'] == '3':
                        pprint = ProcessesPrint()
                        pprint.process_prod_id = int(proces['product_id'])
                        pprint.observation = proces['observation']
                        pprint.quantity = int(proces['quantity'])
                        pprint.state_id = proces['state']
                        pprint.destiny = proces['destiny']
                        pprint.process_date_joined = proces['process_date_joined']
                        if proces['process_date_joined'] == proces['process_date_finish']:
                            pprint.process_date_finish = datetime.now() + timedelta(7)
                        else:
                            pprint.process_date_finish = proces['process_date_finish']
                        pprint.product_id = proces['product_id']
                        pprint.order_number = "LC01-" + str(processes.id)
                        for p in Product.objects.filter(processes__product_id=int(proces['product_id'])):
                            pprint.image = p.image
                        pprint.state_dressmaking_id = proces['state_dressmaking']
                        pprint.save()

                    # 04 PROCESO PLANCHADO
                    elif proces['state'] == '4':
                        pironing = ProcessesIroning()
                        pironing.process_prod_id = int(proces['product_id'])
                        pironing.observation = proces['observation']
                        pironing.quantity = int(proces['quantity'])
                        pironing.state_id = proces['state']
                        pironing.destiny = proces['destiny']
                        pironing.process_date_joined = proces['process_date_joined']
                        if proces['process_date_joined'] == proces['process_date_finish']:
                            pironing.process_date_finish = datetime.now() + timedelta(7)
                        else:
                            pironing.process_date_finish = proces['process_date_finish']
                        pironing.product_id = proces['product_id']
                        pironing.order_number = "LC01-" + str(processes.id)
                        for p in Product.objects.filter(processes__product_id=int(proces['product_id'])):
                            pironing.image = p.image
                        pironing.state_dressmaking_id = proces['state_dressmaking']
                        pironing.save()

                    # 05 PROCESO CONFECCION
                    elif proces['state'] == '5':
                        pmaking = ProcessesMaking()
                        pmaking.process_prod_id = int(proces['product_id'])
                        pmaking.observation = proces['observation']
                        pmaking.quantity = int(proces['quantity'])
                        pmaking.state_id = proces['state']
                        pmaking.destiny = proces['destiny']
                        pmaking.process_date_joined = proces['process_date_joined']
                        if proces['process_date_joined'] == proces['process_date_finish']:
                            pmaking.process_date_finish = datetime.now() + timedelta(7)
                        else:
                            pmaking.process_date_finish = proces['process_date_finish']
                        pmaking.product_id = proces['product_id']
                        pmaking.order_number = "LC01-"+ str(processes.id)
                        for p in Product.objects.filter(processes__product_id=int(proces['product_id'])):
                            pmaking.image = p.image
                        pmaking.state_dressmaking_id = proces['state_dressmaking']
                        pmaking.save()

                    # 06 PROCESO PAQUETERIA
                    elif proces['state'] == '6':
                        ppack = ProcessesPack()
                        ppack.process_prod_id = int(proces['product_id'])
                        ppack.observation = proces['observation']
                        ppack.quantity = int(proces['quantity'])
                        ppack.state_id = proces['state']
                        ppack.destiny = proces['destiny']
                        ppack.process_date_joined = proces['process_date_joined']
                        if proces['process_date_joined'] == proces['process_date_finish']:
                            ppack.process_date_finish = datetime.now() + timedelta(7)
                        else:
                            ppack.process_date_finish = proces['process_date_finish']
                        ppack.product_id = proces['product_id']
                        ppack.order_number = "LC01-" + str(processes.id)
                        for p in Product.objects.filter(processes__product_id=int(proces['product_id'])):
                            ppack.image = p.image
                        ppack.state_dressmaking_id = proces['state_dressmaking']
                        ppack.save()

                    # 07 PROCESO DESPACHO
                    elif proces['state'] == '7':
                        pdispatch = ProcessesDispatch()
                        pdispatch.process_prod_id = int(proces['product_id'])
                        pdispatch.observation = proces['observation']
                        pdispatch.quantity = int(proces['quantity'])
                        pdispatch.state_id = proces['state']
                        pdispatch.destiny = proces['destiny']
                        pdispatch.process_date_joined = proces['process_date_joined']
                        if proces['process_date_joined'] == proces['process_date_finish']:
                            pdispatch.process_date_finish = datetime.now() + timedelta(7)
                        else:
                            pdispatch.process_date_finish = proces['process_date_finish']
                        pdispatch.product_id = proces['product_id']
                        pdispatch.order_number = "LC01-" + str(processes.id)
                        for p in Product.objects.filter(processes__product_id=int(proces['product_id'])):
                            pdispatch.image = p.image
                        pdispatch.state_dressmaking_id = proces['state_dressmaking']
                        pdispatch.save()

                    # 10 PROCESO PENDIENTES
                    elif proces['state'] == '10':
                        pPending = ProcessesPending()
                        pPending.process_prod_id = int(proces['product_id'])
                        pPending.observation = proces['observation']
                        pPending.quantity = int(proces['quantity'])
                        pPending.state_id = proces['state']
                        pPending.destiny = proces['destiny']
                        pPending.process_date_joined = proces['process_date_joined']
                        if proces['process_date_joined'] == proces['process_date_finish']:
                            pPending.process_date_finish = datetime.now() + timedelta(7)
                        else:
                            pPending.process_date_finish = proces['process_date_finish']
                        pPending.product_id = proces['product_id']
                        pPending.order_number = "LC01-" + str(processes.id)
                        for p in Product.objects.filter(processes__product_id=int(proces['product_id'])):
                            pPending.image = p.image
                        pPending.state_dressmaking_id = proces['state_dressmaking']
                        pPending.save()

            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Nueva Producción'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['action'] = 'add'
        context['list_url'] = self.success_url
        return context


class ProcessesUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Processes
    model2 = Product
    form_class = ProcessesForm
    template_name = 'processes/update.html'
    success_url = reverse_lazy('productos_app:processes-list')
    permission_required = 'change_processes'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                with transaction.atomic():
                    proces = json.loads(request.POST['proces'])
                    #CAMBIO MASIVO DE LA ORDEN
                    #FALSO SOLO CAMBIA EL PRODUCTO SELECCIONADO.
                    if proces['checkbox_order'] == False:
                        processes = self.get_object()
                        processes.process_prod_id = int(proces['product_id'])
                        processes.observation = proces['observation']
                        processes.quantity = int(proces['quantity'])
                        processes.state_id = proces['state']
                        processes.destiny = proces['destiny']
                        processes.process_date_joined = proces['process_date_joined']
                        processes.process_date_finish = proces['process_date_finish']
                        processes.order_id = proces['order_id']
                        processes.product_id = proces['product_id']
                        processes.order_number = proces['order_number']
                        for p in Product.objects.filter(id=int(proces['product_id'])):
                            processes.image = p.image
                        processes.state_dressmaking_id = proces['state_dressmaking']
                        processes.save()

                        # Estado FINALIZADO
                        if (proces['state'] == '8'):
                            for i in ProcessesDesign.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesLayout.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesPrint.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesIroning.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesMaking.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesPack.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesDispatch.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesPending.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                        # Estado CANCELADO
                        elif (proces['state'] == '9'):
                            for i in ProcessesDesign.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesLayout.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesPrint.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesIroning.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesMaking.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesPack.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesDispatch.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                            for i in ProcessesPending.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                        else:
                            # 01 PROCESO DISEÑOS
                            if proces['state'] == '1':
                                flag = False
                                for i in ProcessesDesign.objects.all().order_by('-id'):
                                    if((i.order_number==proces['order_number']) and (i.product_id==proces['product_id'])):
                                        i.process_prod_id = int(proces['product_id'])
                                        i.observation = proces['observation']
                                        i.quantity = int(proces['quantity'])
                                        i.state_id = proces['state']
                                        i.destiny = proces['destiny']
                                        i.process_date_joined = proces['process_date_joined']
                                        i.process_date_finish = proces['process_date_finish']
                                        i.order_id = proces['order_id']
                                        i.product_id = proces['product_id']
                                        i.order_number = proces['order_number']
                                        for p in Product.objects.filter(id=int(proces['product_id'])):
                                            i.image = p.image
                                        i.state_dressmaking_id = proces['state_dressmaking']
                                        i.save()
                                        flag = True
                                        break

                                if flag == False:
                                    pdesing = ProcessesDesign()
                                    pdesing.process_prod_id = int(proces['product_id'])
                                    pdesing.observation = proces['observation']
                                    pdesing.quantity = int(proces['quantity'])
                                    pdesing.state_id = proces['state']
                                    pdesing.destiny = proces['destiny']
                                    pdesing.process_date_joined = proces['process_date_joined']
                                    pdesing.process_date_finish = proces['process_date_finish']
                                    pdesing.order_id = proces['order_id']
                                    pdesing.product_id = proces['product_id']
                                    pdesing.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        pdesing.image = p.image
                                    pdesing.state_dressmaking_id = proces['state_dressmaking']
                                    pdesing.save()

                                for i in ProcessesLayout.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPrint.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesIroning.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesMaking.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPack.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesDispatch.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPending.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                            # 02 PROCESO MAQUETERIA
                            elif proces['state'] == '2':
                                flag = False
                                for i in ProcessesLayout.objects.all().order_by():
                                    if((i.order_number==proces['order_number']) and (i.product_id==proces['product_id'])):
                                        i.process_prod_id = int(proces['product_id'])
                                        i.observation = proces['observation']
                                        i.quantity = int(proces['quantity'])
                                        i.state_id = proces['state']
                                        i.destiny = proces['destiny']
                                        i.process_date_joined = proces['process_date_joined']
                                        i.process_date_finish = proces['process_date_finish']
                                        i.order_id = proces['order_id']
                                        i.product_id = proces['product_id']
                                        i.order_number = proces['order_number']
                                        for p in Product.objects.filter(id=int(proces['product_id'])):
                                            i.image = p.image
                                        i.state_dressmaking_id = proces['state_dressmaking']
                                        i.save()
                                        flag = True
                                        break

                                if flag == False:
                                    playout = ProcessesLayout()
                                    playout.process_prod_id = int(proces['product_id'])
                                    playout.observation = proces['observation']
                                    playout.quantity = int(proces['quantity'])
                                    playout.state_id = proces['state']
                                    playout.destiny = proces['destiny']
                                    playout.process_date_joined = proces['process_date_joined']
                                    playout.process_date_finish = proces['process_date_finish']
                                    playout.order_id = proces['order_id']
                                    playout.product_id = proces['product_id']
                                    playout.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        playout.image = p.image
                                    playout.state_dressmaking_id = proces['state_dressmaking']
                                    playout.save()

                                for i in ProcessesDesign.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPrint.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesIroning.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesMaking.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPack.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesDispatch.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPending.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                            # 03 PROCESO IMPRESION
                            elif proces['state'] == '3':
                                flag = False
                                for i in ProcessesPrint.objects.all().order_by():
                                    if((i.order_number==proces['order_number']) and (i.product_id==proces['product_id'])):
                                        i.process_prod_id = int(proces['product_id'])
                                        i.observation = proces['observation']
                                        i.quantity = int(proces['quantity'])
                                        i.state_id = proces['state']
                                        i.destiny = proces['destiny']
                                        i.process_date_joined = proces['process_date_joined']
                                        i.process_date_finish = proces['process_date_finish']
                                        i.order_id = proces['order_id']
                                        i.product_id = proces['product_id']
                                        i.order_number = proces['order_number']
                                        for p in Product.objects.filter(id=int(proces['product_id'])):
                                            i.image = p.image
                                        i.state_dressmaking_id = proces['state_dressmaking']
                                        i.save()
                                        flag = True
                                        break

                                if flag == False:
                                    pprint = ProcessesPrint()
                                    pprint.process_prod_id = int(proces['product_id'])
                                    pprint.observation = proces['observation']
                                    pprint.quantity = int(proces['quantity'])
                                    pprint.state_id = proces['state']
                                    pprint.destiny = proces['destiny']
                                    pprint.process_date_joined = proces['process_date_joined']
                                    pprint.process_date_finish = proces['process_date_finish']
                                    pprint.order_id = proces['order_id']
                                    pprint.product_id = proces['product_id']
                                    pprint.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        pprint.image = p.image
                                    pprint.state_dressmaking_id = proces['state_dressmaking']
                                    pprint.save()

                                for i in ProcessesDesign.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesLayout.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesIroning.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesMaking.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPack.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesDispatch.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPending.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                            # 04 PROCESO PLANCHADO
                            elif proces['state'] == '4':
                                flag = False
                                for i in ProcessesIroning.objects.all().order_by():
                                    if((i.order_number==proces['order_number']) and (i.product_id==proces['product_id'])):
                                        i.process_prod_id = int(proces['product_id'])
                                        i.observation = proces['observation']
                                        i.quantity = int(proces['quantity'])
                                        i.state_id = proces['state']
                                        i.destiny = proces['destiny']
                                        i.process_date_joined = proces['process_date_joined']
                                        i.process_date_finish = proces['process_date_finish']
                                        i.order_id = proces['order_id']
                                        i.product_id = proces['product_id']
                                        i.order_number = proces['order_number']
                                        for p in Product.objects.filter(id=int(proces['product_id'])):
                                            i.image = p.image
                                        i.state_dressmaking_id = proces['state_dressmaking']
                                        i.save()
                                        flag = True
                                        break

                                if flag == False:
                                    pironing = ProcessesIroning()
                                    pironing.process_prod_id = int(proces['product_id'])
                                    pironing.observation = proces['observation']
                                    pironing.quantity = int(proces['quantity'])
                                    pironing.state_id = proces['state']
                                    pironing.destiny = proces['destiny']
                                    pironing.process_date_joined = proces['process_date_joined']
                                    pironing.process_date_finish = proces['process_date_finish']
                                    pironing.order_id = proces['order_id']
                                    pironing.product_id = proces['product_id']
                                    pironing.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        pironing.image = p.image
                                    pironing.state_dressmaking_id = proces['state_dressmaking']
                                    pironing.save()

                                for i in ProcessesDesign.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesLayout.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPrint.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesMaking.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPack.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesDispatch.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPending.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                            # 05 PROCESO CONFECCION
                            elif proces['state'] == '5':
                                flag = False
                                for i in ProcessesMaking.objects.all().order_by():
                                    if((i.order_number==proces['order_number']) and (i.product_id==proces['product_id'])):
                                        i.process_prod_id = int(proces['product_id'])
                                        i.observation = proces['observation']
                                        i.quantity = int(proces['quantity'])
                                        i.state_id = proces['state']
                                        i.destiny = proces['destiny']
                                        i.process_date_joined = proces['process_date_joined']
                                        i.process_date_finish = proces['process_date_finish']
                                        i.order_id = proces['order_id']
                                        i.product_id = proces['product_id']
                                        i.order_number = proces['order_number']
                                        for p in Product.objects.filter(id=int(proces['product_id'])):
                                            i.image = p.image
                                        i.state_dressmaking_id = proces['state_dressmaking']
                                        i.save()
                                        flag = True
                                        break

                                if flag == False:
                                    pmaking = ProcessesMaking()
                                    pmaking.process_prod_id = int(proces['product_id'])
                                    pmaking.observation = proces['observation']
                                    pmaking.quantity = int(proces['quantity'])
                                    pmaking.state_id = proces['state']
                                    pmaking.destiny = proces['destiny']
                                    pmaking.process_date_joined = proces['process_date_joined']
                                    pmaking.process_date_finish = proces['process_date_finish']
                                    pmaking.order_id = proces['order_id']
                                    pmaking.product_id = proces['product_id']
                                    pmaking.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        pmaking.image = p.image
                                    pmaking.state_dressmaking_id = proces['state_dressmaking']
                                    pmaking.save()

                                for i in ProcessesDesign.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesLayout.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPrint.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesIroning.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPack.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesDispatch.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPending.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                            # 06 PROCESO PAQUETERIA
                            elif proces['state'] == '6':
                                flag = False
                                for i in ProcessesPack.objects.all().order_by():
                                    if((i.order_number==proces['order_number']) and (i.product_id==proces['product_id'])):
                                        i.process_prod_id = int(proces['product_id'])
                                        i.observation = proces['observation']
                                        i.quantity = int(proces['quantity'])
                                        i.state_id = proces['state']
                                        i.destiny = proces['destiny']
                                        i.process_date_joined = proces['process_date_joined']
                                        i.process_date_finish = proces['process_date_finish']
                                        i.order_id = proces['order_id']
                                        i.product_id = proces['product_id']
                                        i.order_number = proces['order_number']
                                        for p in Product.objects.filter(id=int(proces['product_id'])):
                                            i.image = p.image
                                        i.state_dressmaking_id = proces['state_dressmaking']
                                        i.save()
                                        flag = True
                                        break

                                if flag == False:
                                    ppack = ProcessesPack()
                                    ppack.process_prod_id = int(proces['product_id'])
                                    ppack.observation = proces['observation']
                                    ppack.quantity = int(proces['quantity'])
                                    ppack.state_id = proces['state']
                                    ppack.destiny = proces['destiny']
                                    ppack.process_date_joined = proces['process_date_joined']
                                    ppack.process_date_finish = proces['process_date_finish']
                                    ppack.order_id = proces['order_id']
                                    ppack.product_id = proces['product_id']
                                    ppack.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        ppack.image = p.image
                                    ppack.state_dressmaking_id = proces['state_dressmaking']
                                    ppack.save()

                                for i in ProcessesDesign.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesLayout.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPrint.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesIroning.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesMaking.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesDispatch.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPending.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                            # 07 PROCESO DESPACHO
                            elif proces['state'] == '7':
                                flag = False
                                for i in ProcessesDispatch.objects.all().order_by():
                                    if((i.order_number==proces['order_number']) and (i.product_id==proces['product_id'])):
                                        i.process_prod_id = int(proces['product_id'])
                                        i.observation = proces['observation']
                                        i.quantity = int(proces['quantity'])
                                        i.state_id = proces['state']
                                        i.destiny = proces['destiny']
                                        i.process_date_joined = proces['process_date_joined']
                                        i.process_date_finish = proces['process_date_finish']
                                        i.order_id = proces['order_id']
                                        i.product_id = proces['product_id']
                                        i.order_number = proces['order_number']
                                        for p in Product.objects.filter(id=int(proces['product_id'])):
                                            i.image = p.image
                                        i.state_dressmaking_id = proces['state_dressmaking']
                                        i.save()
                                        flag = True
                                        break

                                if flag == False:
                                    pdispatch = ProcessesDispatch()
                                    pdispatch.process_prod_id = int(proces['product_id'])
                                    pdispatch.observation = proces['observation']
                                    pdispatch.quantity = int(proces['quantity'])
                                    pdispatch.state_id = proces['state']
                                    pdispatch.destiny = proces['destiny']
                                    pdispatch.process_date_joined = proces['process_date_joined']
                                    pdispatch.process_date_finish = proces['process_date_finish']
                                    pdispatch.order_id = proces['order_id']
                                    pdispatch.product_id = proces['product_id']
                                    pdispatch.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        pdispatch.image = p.image
                                    pdispatch.state_dressmaking_id = proces['state_dressmaking']
                                    pdispatch.save()

                                for i in ProcessesDesign.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesLayout.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPrint.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesIroning.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesMaking.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPack.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPending.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                            # 10 PROCESO PENDIENTES
                            elif proces['state'] == '10':
                                flag = False
                                for i in ProcessesPending.objects.all().order_by():
                                    if((i.order_number==proces['order_number']) and (i.product_id==proces['product_id'])):
                                        i.process_prod_id = int(proces['product_id'])
                                        i.observation = proces['observation']
                                        i.quantity = int(proces['quantity'])
                                        i.state_id = proces['state']
                                        i.destiny = proces['destiny']
                                        i.process_date_joined = proces['process_date_joined']
                                        i.process_date_finish = proces['process_date_finish']
                                        i.order_id = proces['order_id']
                                        i.product_id = proces['product_id']
                                        i.order_number = proces['order_number']
                                        for p in Product.objects.filter(id=int(proces['product_id'])):
                                            i.image = p.image
                                        i.state_dressmaking_id = proces['state_dressmaking']
                                        i.save()
                                        flag = True
                                        break

                                if flag == False:
                                    pPending = ProcessesPending()
                                    pPending.process_prod_id = int(proces['product_id'])
                                    pPending.observation = proces['observation']
                                    pPending.quantity = int(proces['quantity'])
                                    pPending.state_id = proces['state']
                                    pPending.destiny = proces['destiny']
                                    pPending.process_date_joined = proces['process_date_joined']
                                    pPending.process_date_finish = proces['process_date_finish']
                                    pPending.order_id = proces['order_id']
                                    pPending.product_id = proces['product_id']
                                    pPending.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        pPending.image = p.image
                                    pPending.state_dressmaking_id = proces['state_dressmaking']
                                    pPending.save()

                                for i in ProcessesDesign.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesLayout.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPrint.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesIroning.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesMaking.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesPack.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                                for i in ProcessesDispatch.objects.all().order_by():
                                    if ((i.order_number == proces['order_number']) and (
                                            i.product_id == proces['product_id'])):
                                        i.delete()
                                        break

                    #CAMBIO MASIVO DE LA ORDEN
                    #CAMBIO DE ESTADO DE LA ORDEN DE COMPRA COMPLETA.
                    else:
                        for i in (Processes.objects.filter(order_id__exact=proces['order_id'])):
                            i.observation = proces['observation']
                            i.state_id = proces['state']
                            i.destiny = proces['destiny']
                            i.process_date_joined = proces['process_date_joined']
                            i.process_date_finish = proces['process_date_finish']
                            for p in Product.objects.filter(id=int(i.process_prod_id)):
                                i.image = p.image
                            i.state_dressmaking_id = proces['state_dressmaking']
                            i.save()
                        # 01 PROCESO DISEÑOS.
                        if proces['state'] == '1':
                            for j in ProcessesDesign.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                            for i in (Processes.objects.filter(order_id__exact=proces['order_id'])):
                                pdesign = ProcessesDesign()
                                pdesign.process_prod_id = int(i.process_prod_id)
                                pdesign.observation = i.observation
                                pdesign.quantity = int(i.quantity)
                                pdesign.state_id = int(i.state_id)
                                pdesign.destiny = i.destiny
                                pdesign.process_date_joined = proces['process_date_joined']
                                pdesign.process_date_finish = proces['process_date_finish']
                                pdesign.order_id = i.order_id
                                pdesign.product_id = int(i.product_id)
                                pdesign.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pdesign.image = p.image
                                pdesign.state_dressmaking_id = proces['state_dressmaking']
                                pdesign.save()


                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPrint.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesIroning.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesMaking.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPack.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesDispatch.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPending.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # 02 PROCESO MAQUETERIA
                        elif proces['state'] == '2':
                            for j in ProcessesLayout.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                            for i in (Processes.objects.filter(order_id__exact=proces['order_id'])):
                                playout = ProcessesLayout()
                                playout.process_prod_id = int(i.process_prod_id)
                                playout.observation = i.observation
                                playout.quantity = int(i.quantity)
                                playout.state_id = int(i.state_id)
                                playout.destiny = i.destiny
                                playout.process_date_joined = proces['process_date_joined']
                                playout.process_date_finish = proces['process_date_finish']
                                playout.order_id = i.order_id
                                playout.product_id = int(i.product_id)
                                playout.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    playout.image = p.image
                                playout.state_dressmaking_id = proces['state_dressmaking']
                                playout.save()

                            for i in ProcessesDesign.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPrint.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesIroning.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesMaking.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPack.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesDispatch.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPending.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # 03 PROCESO IMPRESION
                        elif proces['state'] == '3':
                            for j in ProcessesPrint.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                            for i in (Processes.objects.filter(order_id__exact=proces['order_id'])):
                                pprint = ProcessesPrint()
                                pprint.process_prod_id = int(i.process_prod_id)
                                pprint.observation = i.observation
                                pprint.quantity = int(i.quantity)
                                pprint.state_id = int(i.state_id)
                                pprint.destiny = i.destiny
                                pprint.process_date_joined = proces['process_date_joined']
                                pprint.process_date_finish = proces['process_date_finish']
                                pprint.order_id = i.order_id
                                pprint.product_id = int(i.product_id)
                                pprint.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pprint.image = p.image
                                pprint.state_dressmaking_id = proces['state_dressmaking']
                                pprint.save()

                            for i in ProcessesDesign.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesIroning.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesMaking.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPack.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesDispatch.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPending.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # 04 PROCESO PLANCHADO
                        elif proces['state'] == '4':
                            for j in ProcessesIroning.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                            for i in (Processes.objects.filter(order_id__exact=proces['order_id'])):
                                pironing = ProcessesIroning()
                                pironing.process_prod_id = int(i.process_prod_id)
                                pironing.observation = i.observation
                                pironing.quantity = int(i.quantity)
                                pironing.state_id = int(i.state_id)
                                pironing.destiny = i.destiny
                                pironing.process_date_joined = proces['process_date_joined']
                                pironing.process_date_finish = proces['process_date_finish']
                                pironing.order_id = i.order_id
                                pironing.product_id = int(i.product_id)
                                pironing.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pironing.image = p.image
                                pironing.state_dressmaking_id = proces['state_dressmaking']
                                pironing.save()

                            for i in ProcessesDesign.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPrint.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesMaking.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPack.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesDispatch.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPending.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # 05 PROCESO CONFECCION
                        elif proces['state'] == '5':
                            for j in ProcessesMaking.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                            for i in (Processes.objects.filter(order_id__exact=proces['order_id'])):
                                pmaking = ProcessesMaking()
                                pmaking.process_prod_id = int(i.process_prod_id)
                                pmaking.observation = i.observation
                                pmaking.quantity = int(i.quantity)
                                pmaking.state_id = int(i.state_id)
                                pmaking.destiny = i.destiny
                                pmaking.process_date_joined = proces['process_date_joined']
                                pmaking.process_date_finish = proces['process_date_finish']
                                pmaking.order_id = i.order_id
                                pmaking.product_id = int(i.product_id)
                                pmaking.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pmaking.image = p.image
                                pmaking.state_dressmaking_id = proces['state_dressmaking']
                                pmaking.save()

                            for i in ProcessesDesign.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPrint.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesIroning.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPack.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesDispatch.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPending.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # 06 PROCESO PAQUETERIA
                        elif proces['state'] == '6':
                            for j in ProcessesPack.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                            for i in (Processes.objects.filter(order_id__exact=proces['order_id'])):
                                ppack = ProcessesPack()
                                ppack.process_prod_id = int(i.process_prod_id)
                                ppack.observation = i.observation
                                ppack.quantity = int(i.quantity)
                                ppack.state_id = int(i.state_id)
                                ppack.destiny = i.destiny
                                ppack.process_date_joined = proces['process_date_joined']
                                ppack.process_date_finish = proces['process_date_finish']
                                ppack.order_id = i.order_id
                                ppack.product_id = int(i.product_id)
                                ppack.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    ppack.image = p.image
                                ppack.state_dressmaking_id = proces['state_dressmaking']
                                ppack.save()

                            for i in ProcessesDesign.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPrint.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesIroning.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesMaking.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesDispatch.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPending.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # 07 PROCESO DESPACHO
                        elif proces['state'] == '7':
                            for j in ProcessesDispatch.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                            for i in (Processes.objects.filter(order_id__exact=proces['order_id'])):
                                pdispatch = ProcessesDispatch()
                                pdispatch.process_prod_id = int(i.process_prod_id)
                                pdispatch.observation = i.observation
                                pdispatch.quantity = int(i.quantity)
                                pdispatch.state_id = int(i.state_id)
                                pdispatch.destiny = i.destiny
                                pdispatch.process_date_joined = proces['process_date_joined']
                                pdispatch.process_date_finish = proces['process_date_finish']
                                pdispatch.order_id = i.order_id
                                pdispatch.product_id = int(i.product_id)
                                pdispatch.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pdispatch.image = p.image
                                pdispatch.state_dressmaking_id = proces['state_dressmaking']
                                pdispatch.save()

                            for i in ProcessesDesign.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPrint.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesIroning.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesMaking.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPack.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPending.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # 10 PROCESO PENDIENTES
                        elif proces['state'] == '10':
                            for j in ProcessesPending.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                            for i in (Processes.objects.filter(order_id__exact=proces['order_id'])):
                                ppending = ProcessesPending()
                                ppending.process_prod_id = int(i.process_prod_id)
                                ppending.observation = i.observation
                                ppending.quantity = int(i.quantity)
                                ppending.state_id = int(i.state_id)
                                ppending.destiny = i.destiny
                                ppending.process_date_joined = proces['process_date_joined']
                                ppending.process_date_finish = proces['process_date_finish']
                                ppending.order_id = i.order_id
                                ppending.product_id = int(i.product_id)
                                ppending.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    ppending.image = p.image
                                ppending.state_dressmaking_id = proces['state_dressmaking']
                                ppending.save()

                            for i in ProcessesDesign.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPrint.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesIroning.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesMaking.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPack.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesDispatch.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # ESTADO FINALIZADO
                        elif (proces['state'] == '8'):
                            for i in ProcessesDesign.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPrint.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesIroning.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesMaking.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPack.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesDispatch.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPending.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # ESTADO CANCELADO
                        elif (proces['state'] == '9'):
                            for i in ProcessesDesign.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPrint.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesIroning.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesMaking.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPack.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesDispatch.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                            for i in ProcessesPending.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()


            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['process_prod_id'] = self.object.process_prod.id
        context['start_date'] = self.object.process_date_joined
        context['finish_date'] = self.object.process_date_finish
        context['title'] = 'Editar Producción'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['action'] = 'edit'
        context['list_url'] = self.success_url
        return context


class ProcessesDetailView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DetailView):
    model = Processes
    template_name = 'processes/detail.html'
    success_url = reverse_lazy('productos_app:processes-list')
    permission_required = 'view_processes'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Detalle de Producción'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['list_url'] = self.success_url
        return context
