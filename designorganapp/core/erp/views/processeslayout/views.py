import json

from django.contrib.auth.mixins import LoginRequiredMixin
from core.erp.mixins import ValidatePermissionRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, UpdateView, DetailView

from core.erp.forms import ProcessesLayoutForm
from core.erp.models import Processes, ProcessesDesign, ProcessesLayout, ProcessesPrint, ProcessesIroning, ProcessesMaking, ProcessesPack, ProcessesDispatch, Product, ProcessesPending



# 02 - LAYOUT
class LayoutListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = ProcessesLayout
    template_name = 'processes-layout/list.html'
    permission_required = 'view_processeslayout'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in ProcessesLayout.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cont = 0
        for i in ProcessesLayout.objects.all():
            cont += 1
        context['context_cont'] = cont
        context['proces'] = 'Maquetería'
        context['title'] = 'Listado de Maquetería'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['list_url'] = reverse_lazy('productos_app:processes-layout-list')
        context['entity'] = 'Clientes'
        return context


class LayoutUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = ProcessesLayout
    model2 = Product
    form_class = ProcessesLayoutForm
    template_name = 'processes-layout/update.html'
    success_url = reverse_lazy('productos_app:processes-layout-list')
    permission_required = 'change_processeslayout'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                with transaction.atomic():
                    proces = json.loads(request.POST['proces'])
                    # CAMBIO MASIVO DE LA ORDEN - FALSO SOLO CAMBIA EL PRODUCTO SELECCIONADO
                    if proces['checkbox_order'] == False:
                        playout = self.get_object()
                        # 02 PROCESO MAQUETERIA
                        if proces['state'] == '2':
                            playout.process_prod_id = int(proces['product_id'])
                            playout.observation = proces['observation']
                            playout.quantity = int(proces['quantity'])
                            playout.state_id = proces['state']
                            playout.destiny = proces['destiny']
                            playout.process_date_joined = proces['process_date_joined']
                            playout.process_date_finish = proces['process_date_finish']
                            playout.order_id = proces['order_id']
                            playout.product_id = proces['product_id']
                            playout.order_number = proces['order_number']
                            for p in Product.objects.filter(id=int(proces['product_id'])):
                                playout.image = p.image
                            playout.state_dressmaking_id = proces['state_dressmaking']
                            playout.save()

                            for i in Processes.objects.filter(state_id='2'):
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.process_prod_id = int(proces['product_id'])
                                    i.observation = proces['observation']
                                    i.quantity = int(proces['quantity'])
                                    i.state_id = proces['state']
                                    i.destiny = proces['destiny']
                                    i.process_date_joined = proces['process_date_joined']
                                    i.process_date_finish = proces['process_date_finish']
                                    i.order_id = proces['order_id']
                                    i.product_id = proces['product_id']
                                    i.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        i.image = p.image
                                    i.state_dressmaking_id = proces['state_dressmaking']
                                    i.save()
                                    break
                        else:
                            # 02 PROCESO MAQUETERIA
                            for i in Processes.objects.filter(state_id='2'):
                                if ((i.order_number == proces['order_number']) and (i.product_id == proces['product_id'])):
                                    i.process_prod_id = int(proces['product_id'])
                                    i.observation = proces['observation']
                                    i.quantity = int(proces['quantity'])
                                    i.state_id = proces['state']
                                    i.destiny = proces['destiny']
                                    i.process_date_joined = proces['process_date_joined']
                                    i.process_date_finish = proces['process_date_finish']
                                    i.order_id = proces['order_id']
                                    i.product_id = proces['product_id']
                                    i.order_number = proces['order_number']
                                    for p in Product.objects.filter(id=int(proces['product_id'])):
                                        i.image = p.image
                                    i.state_dressmaking_id = proces['state_dressmaking']
                                    i.save()
                                    break

                            # 01 PROCESO DISEÑOS
                            if proces['state'] == '1':
                                pdesing = ProcessesDesign()
                                pdesing.process_prod_id = int(proces['product_id'])
                                pdesing.observation = proces['observation']
                                pdesing.quantity = int(proces['quantity'])
                                pdesing.state_id = proces['state']
                                pdesing.destiny = proces['destiny']
                                pdesing.process_date_joined = proces['process_date_joined']
                                pdesing.process_date_finish = proces['process_date_finish']
                                pdesing.order_id = proces['order_id']
                                pdesing.product_id = proces['product_id']
                                pdesing.order_number = proces['order_number']
                                for p in Product.objects.filter(id=int(proces['product_id'])):
                                    pdesing.image = p.image
                                pdesing.state_dressmaking_id = proces['state_dressmaking']
                                pdesing.save()

                            # 03 PROCESO IMPRESION
                            elif proces['state'] == '3':
                                pprint = ProcessesPrint()
                                pprint.process_prod_id = int(proces['product_id'])
                                pprint.observation = proces['observation']
                                pprint.quantity = int(proces['quantity'])
                                pprint.state_id = proces['state']
                                pprint.destiny = proces['destiny']
                                pprint.process_date_joined = proces['process_date_joined']
                                pprint.process_date_finish = proces['process_date_finish']
                                pprint.order_id = proces['order_id']
                                pprint.product_id = proces['product_id']
                                pprint.order_number = proces['order_number']
                                for p in Product.objects.filter(id=int(proces['product_id'])):
                                    pprint.image = p.image
                                pprint.state_dressmaking_id = proces['state_dressmaking']
                                pprint.save()

                            # 04 PROCESO PLANCHADO
                            elif proces['state'] == '4':
                                pironing = ProcessesIroning()
                                pironing.process_prod_id = int(proces['product_id'])
                                pironing.observation = proces['observation']
                                pironing.quantity = int(proces['quantity'])
                                pironing.state_id = proces['state']
                                pironing.destiny = proces['destiny']
                                pironing.process_date_joined = proces['process_date_joined']
                                pironing.process_date_finish = proces['process_date_finish']
                                pironing.order_id = proces['order_id']
                                pironing.product_id = proces['product_id']
                                pironing.order_number = proces['order_number']
                                for p in Product.objects.filter(id=int(proces['product_id'])):
                                    pironing.image = p.image
                                pironing.state_dressmaking_id = proces['state_dressmaking']
                                pironing.save()

                            # 05 PROCESO CONFECCION
                            elif proces['state'] == '5':
                                pmaking = ProcessesMaking()
                                pmaking.process_prod_id = int(proces['product_id'])
                                pmaking.observation = proces['observation']
                                pmaking.quantity = int(proces['quantity'])
                                pmaking.state_id = proces['state']
                                pmaking.destiny = proces['destiny']
                                pmaking.process_date_joined = proces['process_date_joined']
                                pmaking.process_date_finish = proces['process_date_finish']
                                pmaking.order_id = proces['order_id']
                                pmaking.product_id = proces['product_id']
                                pmaking.order_number = proces['order_number']
                                for p in Product.objects.filter(id=int(proces['product_id'])):
                                    pmaking.image = p.image
                                pmaking.state_dressmaking_id = proces['state_dressmaking']
                                pmaking.save()

                            # 06 PROCESO PAQUETERIA
                            elif proces['state'] == '6':
                                ppack = ProcessesPack()
                                ppack.process_prod_id = int(proces['product_id'])
                                ppack.observation = proces['observation']
                                ppack.quantity = int(proces['quantity'])
                                ppack.state_id = proces['state']
                                ppack.destiny = proces['destiny']
                                ppack.process_date_joined = proces['process_date_joined']
                                ppack.process_date_finish = proces['process_date_finish']
                                ppack.order_id = proces['order_id']
                                ppack.product_id = proces['product_id']
                                ppack.order_number = proces['order_number']
                                for p in Product.objects.filter(id=int(proces['product_id'])):
                                    ppack.image = p.image
                                ppack.state_dressmaking_id = proces['state_dressmaking']
                                ppack.save()

                            # 07 PROCESO DESPACHO
                            elif proces['state'] == '7':
                                pdispatch = ProcessesDispatch()
                                pdispatch.process_prod_id = int(proces['product_id'])
                                pdispatch.observation = proces['observation']
                                pdispatch.quantity = int(proces['quantity'])
                                pdispatch.state_id = proces['state']
                                pdispatch.destiny = proces['destiny']
                                pdispatch.process_date_joined = proces['process_date_joined']
                                pdispatch.process_date_finish = proces['process_date_finish']
                                pdispatch.order_id = proces['order_id']
                                pdispatch.product_id = proces['product_id']
                                pdispatch.order_number = proces['order_number']
                                for p in Product.objects.filter(id=int(proces['product_id'])):
                                    pdispatch.image = p.image
                                pdispatch.state_dressmaking_id = proces['state_dressmaking']
                                pdispatch.save()

                            # 10 PROCESO PENDIENTES
                            elif proces['state'] == '10':
                                pPending = ProcessesPending()
                                pPending.process_prod_id = int(proces['product_id'])
                                pPending.observation = proces['observation']
                                pPending.quantity = int(proces['quantity'])
                                pPending.state_id = proces['state']
                                pPending.destiny = proces['destiny']
                                pPending.process_date_joined = proces['process_date_joined']
                                pPending.process_date_finish = proces['process_date_finish']
                                pPending.order_id = proces['order_id']
                                pPending.product_id = proces['product_id']
                                pPending.order_number = proces['order_number']
                                for p in Product.objects.filter(id=int(proces['product_id'])):
                                    pPending.image = p.image
                                pPending.state_dressmaking_id = proces['state_dressmaking']
                                pPending.save()

                            for i in ProcessesLayout.objects.all().order_by():
                                if ((i.order_number == proces['order_number']) and (
                                        i.product_id == proces['product_id'])):
                                    i.delete()
                                    break

                    # CAMBIO DE ESTADO DE LA ORDEN DE COMPRA COMPLETA
                    else:
                        # 02 PROCESO MAQUETERIA
                        if proces['state'] == '2':
                            for i in ProcessesLayout.objects.filter(order_number__exact=proces['order_number']):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                i.observation = proces['observation']
                                i.state_id = proces['state']
                                i.destiny = proces['destiny']
                                i.process_date_joined = proces['process_date_joined']
                                i.process_date_finish = proces['process_date_finish']
                                i.image = found_product[0].image
                                i.state_dressmaking_id = proces['state_dressmaking']
                                i.save()

                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                        # 01 PROCESO DISEÑOS
                        elif proces['state'] == '1':
                            # AGREGA TODOS LOS PRODUCTOS EN LA NUEVA TABLA
                            for i in (ProcessesLayout.objects.filter(order_id__exact=proces['order_id'])):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                pdesign = ProcessesDesign()
                                pdesign.process_prod_id = int(i.process_prod_id)
                                pdesign.observation = proces['observation']
                                pdesign.quantity = int(i.quantity)
                                pdesign.state_id = proces['state']
                                pdesign.destiny = proces['destiny']
                                pdesign.process_date_joined = proces['process_date_joined']
                                pdesign.process_date_finish = proces['process_date_finish']
                                pdesign.order_id = i.order_id
                                pdesign.product_id = int(i.product_id)
                                pdesign.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pdesign.image = p.image
                                pdesign.state_dressmaking_id = proces['state_dressmaking']
                                pdesign.save()
                                # ACTUALIZA ESTADOS E IMÁGENES EN PRODUCCION
                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                            # ELIMINA TODOS LOS PRODUCOS DE LA TABLA ACTUAL
                            for j in ProcessesLayout.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                        # 03 PROCESO IMPRESION
                        elif proces['state'] == '3':
                            # AGREGA TODOS LOS PRODUCTOS EN LA NUEVA TABLA
                            for i in (ProcessesLayout.objects.filter(order_id__exact=proces['order_id'])):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                pprint = ProcessesPrint()
                                pprint.process_prod_id = int(i.process_prod_id)
                                pprint.observation = proces['observation']
                                pprint.quantity = int(i.quantity)
                                pprint.state_id = proces['state']
                                pprint.destiny = proces['destiny']
                                pprint.process_date_joined = proces['process_date_joined']
                                pprint.process_date_finish = proces['process_date_finish']
                                pprint.order_id = i.order_id
                                pprint.product_id = int(i.product_id)
                                pprint.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pprint.image = p.image
                                pprint.state_dressmaking_id = proces['state_dressmaking']
                                pprint.save()
                                # ACTUALIZA ESTADOS E IMÁGENES EN PRODUCCION
                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                            # ELIMINA TODOS LOS PRODUCOS DE LA TABLA ACTUAL
                            for j in ProcessesLayout.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                        # 04 PROCESO PLANCHADO
                        elif proces['state'] == '4':
                            # AGREGA TODOS LOS PRODUCTOS EN LA NUEVA TABLA
                            for i in (ProcessesLayout.objects.filter(order_id__exact=proces['order_id'])):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                pironing = ProcessesIroning()
                                pironing.process_prod_id = int(i.process_prod_id)
                                pironing.observation = proces['observation']
                                pironing.quantity = int(i.quantity)
                                pironing.state_id = proces['state']
                                pironing.destiny = proces['destiny']
                                pironing.process_date_joined = proces['process_date_joined']
                                pironing.process_date_finish = proces['process_date_finish']
                                pironing.order_id = i.order_id
                                pironing.product_id = int(i.product_id)
                                pironing.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pironing.image = p.image
                                pironing.state_dressmaking_id = proces['state_dressmaking']
                                pironing.save()
                                # ACTUALIZA ESTADOS E IMÁGENES EN PRODUCCION
                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                            # ELIMINA TODOS LOS PRODUCOS DE LA TABLA ACTUAL
                            for j in ProcessesLayout.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                        # 05 PROCESO CONFECCION
                        elif proces['state'] == '5':
                            # AGREGA TODOS LOS PRODUCTOS EN LA NUEVA TABLA
                            for i in (ProcessesLayout.objects.filter(order_id__exact=proces['order_id'])):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                pmaking = ProcessesMaking()
                                pmaking.process_prod_id = int(i.process_prod_id)
                                pmaking.observation = proces['observation']
                                pmaking.quantity = int(i.quantity)
                                pmaking.state_id = proces['state']
                                pmaking.destiny = proces['destiny']
                                pmaking.process_date_joined = proces['process_date_joined']
                                pmaking.process_date_finish = proces['process_date_finish']
                                pmaking.order_id = i.order_id
                                pmaking.product_id = int(i.product_id)
                                pmaking.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pmaking.image = p.image
                                pmaking.state_dressmaking_id = proces['state_dressmaking']
                                pmaking.save()
                                # ACTUALIZA ESTADOS E IMÁGENES EN PRODUCCION
                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                            # ELIMINA TODOS LOS PRODUCOS DE LA TABLA ACTUAL
                            for j in ProcessesLayout.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                        # 06 PROCESO PAQUETERIA
                        elif proces['state'] == '6':
                            # AGREGA TODOS LOS PRODUCTOS EN LA NUEVA TABLA
                            for i in (ProcessesLayout.objects.filter(order_id__exact=proces['order_id'])):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                ppack = ProcessesPack()
                                ppack.process_prod_id = int(i.process_prod_id)
                                ppack.observation = proces['observation']
                                ppack.quantity = int(i.quantity)
                                ppack.state_id = proces['state']
                                ppack.destiny = proces['destiny']
                                ppack.process_date_joined = proces['process_date_joined']
                                ppack.process_date_finish = proces['process_date_finish']
                                ppack.order_id = i.order_id
                                ppack.product_id = int(i.product_id)
                                ppack.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    ppack.image = p.image
                                ppack.state_dressmaking_id = proces['state_dressmaking']
                                ppack.save()
                                # ACTUALIZA ESTADOS E IMÁGENES EN PRODUCCION
                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                            # ELIMINA TODOS LOS PRODUCOS DE LA TABLA ACTUAL
                            for j in ProcessesLayout.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                        # 07 PROCESO DESPACHO
                        elif proces['state'] == '7':
                            # AGREGA TODOS LOS PRODUCTOS EN LA NUEVA TABLA
                            for i in (ProcessesLayout.objects.filter(order_id__exact=proces['order_id'])):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                pdispatch = ProcessesDispatch()
                                pdispatch.process_prod_id = int(i.process_prod_id)
                                pdispatch.observation = proces['observation']
                                pdispatch.quantity = int(i.quantity)
                                pdispatch.state_id = proces['state']
                                pdispatch.destiny = proces['destiny']
                                pdispatch.process_date_joined = proces['process_date_joined']
                                pdispatch.process_date_finish = proces['process_date_finish']
                                pdispatch.order_id = i.order_id
                                pdispatch.product_id = int(i.product_id)
                                pdispatch.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    pdispatch.image = p.image
                                pdispatch.state_dressmaking_id = proces['state_dressmaking']
                                pdispatch.save()
                                # ACTUALIZA ESTADOS E IMÁGENES EN PRODUCCION
                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                            # ELIMINA TODOS LOS PRODUCOS DE LA TABLA ACTUAL
                            for j in ProcessesLayout.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                        # 10 PROCESO PENDIENTES
                        elif proces['state'] == '10':
                            # AGREGA TODOS LOS PRODUCTOS EN LA NUEVA TABLA
                            for i in (ProcessesLayout.objects.filter(order_id__exact=proces['order_id'])):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                ppending = ProcessesPending()
                                ppending.process_prod_id = int(i.process_prod_id)
                                ppending.observation = proces['observation']
                                ppending.quantity = int(i.quantity)
                                ppending.state_id = proces['state']
                                ppending.destiny = proces['destiny']
                                ppending.process_date_joined = proces['process_date_joined']
                                ppending.process_date_finish = proces['process_date_finish']
                                ppending.order_id = i.order_id
                                ppending.product_id = int(i.product_id)
                                ppending.order_number = i.order_number
                                for p in Product.objects.filter(id=int(i.product_id)):
                                    ppending.image = p.image
                                ppending.state_dressmaking_id = proces['state_dressmaking']
                                ppending.save()
                                # ACTUALIZA ESTADOS E IMÁGENES EN PRODUCCION
                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                            # ELIMINA TODOS LOS PRODUCOS DE LA TABLA ACTUAL
                            for j in ProcessesLayout.objects.all().order_by():
                                if j.order_number == proces['order_number']:
                                    j.delete()

                        # ESTADO FINALIZADO
                        elif (proces['state'] == '8'):
                            for i in (ProcessesLayout.objects.filter(order_id__exact=proces['order_id'])):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                # ACTUALIZA ESTADOS E IMÁGENES EN PRODUCCION
                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

                        # ESTADO CANCELADO
                        elif (proces['state'] == '9'):
                            for i in (ProcessesLayout.objects.filter(order_id__exact=proces['order_id'])):
                                found_product = Product.objects.filter(id=int(i.process_prod_id))
                                # ACTUALIZA ESTADOS E IMÁGENES EN PRODUCCION
                                for j in Processes.objects.filter(order_number__exact=proces['order_number']):
                                    if found_product[0].id == j.process_prod_id:
                                        j.observation = proces['observation']
                                        j.state_id = proces['state']
                                        j.destiny = proces['destiny']
                                        j.process_date_joined = proces['process_date_joined']
                                        j.process_date_finish = proces['process_date_finish']
                                        j.image = found_product[0].image
                                        j.state_dressmaking_id = proces['state_dressmaking']
                                        j.save()

                            for i in ProcessesLayout.objects.all().order_by():
                                if i.order_number == proces['order_number']:
                                    i.delete()

            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['process_prod_id'] = self.object.process_prod.id
        context['start_date'] = self.object.process_date_joined
        context['finish_date'] = self.object.process_date_finish
        context['title'] = 'Editar Maquetería'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['action'] = 'edit'
        context['list_url'] = self.success_url
        return context


class LayoutDetailView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DetailView):
    model = ProcessesLayout
    template_name = 'processes-layout/detail.html'
    success_url = reverse_lazy('productos_app:processes-layout-list')
    permission_required = 'view_processeslayout'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Detalle de Maquetería'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['list_url'] = self.success_url
        return context
