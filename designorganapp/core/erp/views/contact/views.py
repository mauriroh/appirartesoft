import json
import os

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import Q
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, View
from xhtml2pdf import pisa
from datetime import datetime, timedelta

from core.erp.forms import SaleForm, ClientForm
from core.erp.mixins import ValidatePermissionRequiredMixin
from core.erp.models import Sale, Product, DetSale, Client, Processes, Estados, ProcessesDesign, Dressmaking



class ContactCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Sale
    form_class = SaleForm
    template_name = 'contact/create.html'
    success_url = reverse_lazy('productos_app:sale-list')
    permission_required = 'add_sale'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            # # SEARCH PRODUCTS AUTOCOMPLETE
            if action == 'search_products':
                data = []
                ids_exclude = json.loads(request.POST['ids'])
                term = request.POST['term']
                products = Product.objects.filter(stock__gt=0)
                if len(term):
                    products = products.filter(name__icontains=term)
                for i in products.exclude(id__in=ids_exclude):  #[0:10]:
                    item = i.toJSON()
                    item['value'] = i.name
                    data.append(item)
            # SEARCH PRODUCTS SELECT2
            # elif action == 'search_autocomplete':
            #     data = []
            #     ids_exclude = json.loads(request.POST['ids'])
            #     term = request.POST['term'].strip()
            #     data.append({'id': term, 'text': term})
            #     products = Product.objects.filter(name__icontains=term, stock__gt=0)
            #     for i in products.exclude(id__in=ids_exclude):    #[0:10]:
            #         item = i.toJSON()
            #         item['text'] = i.name
            #         data.append(item)

            elif action == 'search_autocomplete':
                data = []
                ids_exclude = json.loads(request.POST['ids'])
                term = request.POST['term'].strip()
                data.append({'id': term, 'text': term})
                products = Product.objects.filter(name__icontains=term)
                for i in products.exclude(id__in=ids_exclude):  #[0:10]:
                    item = i.toJSON()
                    item['text'] = i.name
                    data.append(item)

            elif action == 'add':
                with transaction.atomic():
                    vents = json.loads(request.POST['vents'])
                    sale = Sale()
                    sale.date_joined = vents['date_joined']
                    sale.cli_id = vents['cli']
                    sale.subtotal = float(vents['subtotal'])
                    sale.iva = float(vents['iva'])
                    sale.total = float(vents['total'])
                    sale.save()
                    sale.order_number = "CT01-" + str(sale.id)
                    saleContact = "CT01-" + str(sale.id)
                    sale.save()
                    for i in vents['products']:
                        det = DetSale()
                        det.sale_id = sale.id
                        det.prod_id = i['id']
                        det.cant = int(i['cant'])
                        det.price = float(i['pvp'])
                        det.subtotal = float(i['subtotal'])
                        det.save()
                    # En este le enviamos el ID de la factura para crear el PDF
                    data = {'id': sale.id}

                    # print('*********** MODELO Procesos ***********')
                    for i in vents['products']:
                        dress_making = Dressmaking.objects.filter(id=1)
                        found_products = Product.objects.filter(name__icontains=i['name'])
                        process = Processes()
                        process.process_prod_id = int(i['id'])
                        process.observation = ''
                        process.quantity = int(i['cant'])
                        process.state_id = Estados.objects.get(state_type='Diseño').id
                        process.destiny = 'Contacto'
                        process.process_date_joined = vents['date_joined']
                        process.process_date_finish = datetime.now() + timedelta(7)
                        process.order_id = ''
                        process.product_id = found_products[0].id
                        process.order_number = saleContact
                        process.image = found_products[0].image
                        process.state_dressmaking_id = dress_making[0].id
                        process.save()
                        #
                        pdesing = ProcessesDesign()
                        pdesing.process_prod_id = int(i['id'])
                        pdesing.observation = ''
                        pdesing.quantity = int(i['cant'])
                        pdesing.state_id = Estados.objects.get(state_type='Diseño').id
                        pdesing.destiny = 'Contacto'
                        pdesing.process_date_joined = vents['date_joined']
                        pdesing.process_date_finish = datetime.now() + timedelta(7)
                        pdesing.order_id = ''
                        pdesing.product_id = found_products[0].id
                        pdesing.order_number = saleContact
                        pdesing.image = found_products[0].image
                        pdesing.state_dressmaking_id = dress_making[0].id
                        pdesing.save()


            elif action == 'search_clients':
                data = []
                term = request.POST['term']
                clients = Client.objects.filter(Q(name__icontains=term) | Q(identification__icontains=term))    #[0:10]
                for i in clients:
                    item = i.toJSON()
                    item['text'] = i.get_full_name()
                    data.append(item)
            elif action == 'create_client':
                with transaction.atomic():
                    frmClient = ClientForm(request.POST)
                    data = frmClient.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Contacto: Creación de Ventas'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['det'] = []  # Al crear esta variable en el contexto del UpdateView, esta debe ser inicializada en el CreateView como vacia
        context['frmClient'] = ClientForm()
        return context


class SaleInvoicePdfView(LoginRequiredMixin, View):

    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path

    def get(self, request, *args, **kwargs):
        try:
            template = get_template('sale/invoice.html')
            context = {
                'sale': Sale.objects.get(pk=self.kwargs['pk']),
                'comp': {'name': 'Irarte', 'ruc': 'Mendoza - Argentina',
                         'address': ' Las Heras 661, Ciudad de Mendoza'},
                'icon': '{}{}'.format(settings.MEDIA_URL, 'logo.png')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename="report.pdf"'     # Este paso es p/descargar el PDF directamente
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
        except:
            pass
        return HttpResponseRedirect(reverse_lazy('productos_app:sale-list'))

