import datetime
import json
import os

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.db import transaction

from core.erp.forms import ServicesForm
from core.erp.mixins import ValidatePermissionRequiredMixin
from core.erp.models import Services



class ServicesListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Services
    template_name = 'services/list.html'
    permission_required = 'view_services'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Services.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Pago de Servicios'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['create_url'] = reverse_lazy('productos_app:services-create')
        context['list_url'] = reverse_lazy('productos_app:services-list')
        context['entity'] = 'Procesos'
        return context


class ServicesCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Services
    form_class = ServicesForm
    template_name = 'services/create.html'
    success_url = reverse_lazy('productos_app:services-list')
    permission_required = 'add_services'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                with transaction.atomic():
                    serv = json.loads(request.POST['serv'])
                    services = Services()
                    services.name = serv['name']
                    services.client_number = serv['client_number']
                    services.date_joined = serv['date_joined']
                    services.date_finish = serv['date_finish']
                    services.total = float(serv['total'])
                    services.services_state_id = serv['services_state']
                    services.save()

            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Nuevo Pago de Servicios'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['action'] = 'add'
        context['list_url'] = self.success_url
        return context


class ServicesUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Services
    form_class = ServicesForm
    template_name = 'services/create.html'
    success_url = reverse_lazy('productos_app:services-list')
    permission_required = 'change_services'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                with transaction.atomic():
                    serv = json.loads(request.POST['serv'])
                    services = self.get_object()
                    services.name = serv['name']
                    services.client_number = serv['client_number']
                    services.date_joined = serv['date_joined']
                    services.date_finish = serv['date_finish']
                    services.total = float(serv['total'])
                    services.services_state_id = serv['services_state']
                    services.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Editar Pago de Servicios'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['action'] = 'edit'
        context['list_url'] = self.success_url
        return context


class ServicesDeleteView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    model = Services
    template_name = 'services/delete.html'
    success_url = reverse_lazy('productos_app:services-list')
    permission_required = 'delete_services'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminar Pago de Servicios'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['list_url'] = self.success_url
        return context
