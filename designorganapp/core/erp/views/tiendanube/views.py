import json

import requests
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta
from django.views.generic import CreateView, ListView, DeleteView, UpdateView

from core.erp.forms import TiendanubeForm, ProcessesForm
from core.erp.mixins import ValidatePermissionRequiredMixin
from core.erp.models import TiendanubeApi, Client, Department, Province, Processes, Estados, Product, Sale, DetSale, ProcessesDesign, ProcessesPending, Dressmaking

API_KEY = ''
USER_AGENT = ''
SITIO_WEB =''



# Lista las Órdenes
class TiendanubeListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = TiendanubeApi
    form_class = TiendanubeForm
    template_name = 'tiendanube/list.html'
    permission_required = 'view_tiendanubeapi'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in TiendanubeApi.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Productos Tiendanube'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['create_url'] = reverse_lazy('productos_app:tiendanube-createapiadd')
        context['downloadApi_url'] = reverse_lazy('productos_app:tiendanube-createapi')
        context['list_url'] = reverse_lazy('productos_app:tiendanube-list')
        context['entity'] = 'Tiendanube'
        return context


# DESCARGA ORDEN DE Tiendanube
class TiendanubeApiCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = TiendanubeApi
    model1 = Department
    model2 = Province
    model3 = Client
    model4 = Processes
    model5 = Product
    model6 = Sale
    model7 = DetSale
    model8 = Dressmaking
    form_class = TiendanubeForm
    template_name = 'tiendanube/createapi.html'
    success_url = reverse_lazy('productos_app:tiendanube-list')
    permission_required = 'add_tiendanubeapi'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # Convierte el texto en fecha y extrae solo el año/mes/día
    def convert_date(self, date_joined):
        if date_joined is not None:
            index = date_joined.find('T')
            return datetime.strptime(date_joined[0:index], '%Y-%m-%d')
        return datetime.now().date()

    # HAY QUE CARGAR LOS DATOS DE LOS CLIENTES QUE ESTAN VACIOS
    def post(self, request, *args, **kwargs):
        response = {'resp': True}
        try:
            params = {
                'Content-Type': 'application/json',
                'Authentication': f'{API_KEY}',
                'User-Agent': f'{USER_AGENT}'
            }
            data = requests.get(f'{SITIO_WEB}', headers=params)

            if data.status_code == 200:
                response_json = json.loads(data.text)

                # Este for es para> Departmen / Province / Client / Product
                for response_i in response_json:
                    # Verifica si la compra no fue anulada
                    cancel_reason = response_i['cancel_reason']
                    if cancel_reason == None:
                        # for j in response_i['products']:
                        # if (response_i['customer']['name'] == 'Sin nombre' ):
                        #     customer_name = response_i['customer']['name']
                        if (response_i['customer']['name'] != None):
                            customer_name = response_i['customer']['name']
                        else:
                            customer_name = ' '
                        if customer_name == 'Sin nombre' or customer_name == ' ':
                            customer_name = response_i['shipping_address']['name']
                        if customer_name == None:
                            customer_name = ' '
                        if customer_name == ' ':
                            customer_name = response_i['customer']['billing_name']
                        #
                        if customer_name == None:
                            customer_name = ' '
                        if customer_name == ' ':
                            customer_name = response_i['billing_name']
                        if customer_name == None:
                            customer_name = ' '
                        #
                        if response_i['customer']['email'] != None:
                            email = response_i['customer']['email']
                        else:
                            email = ''

                        # # Control Clinte Nombre y Email
                        #                   if (not (Client.objects.filter(email__icontains=response_i['customer']['email'])).exists() or not (Client.objects.filter(name__icontains=response_i['customer']['name'])).exists()):
                        if (not (Client.objects.filter(email__icontains=email)).exists() or not (
                        Client.objects.filter(name__icontains=customer_name)).exists()):
                            #
                            if response_i['customer']['identification'] != None:
                                identification = response_i['customer']['identification']
                            else:
                                identification = ''
                            #
                            if response_i['billing_phone'] != None:
                                phone = response_i['billing_phone']
                            else:
                                phone = ''
                            if (phone == '') | (phone == None):
                                phone = response_i['customer']['phone']
                            if (phone == '') | (phone == None):
                                phone = response_i['shipping_address']['phone']
                            if phone == None:
                                phone = ''
                            #
                            if response_i['billing_address'] != None:
                                address = response_i['billing_address']
                            else:
                                address = ''
                            if (address == '') | (address == None):
                                address = response_i['shipping_address']['address']
                            if address == None:
                                address = ''
                            #
                            if response_i['billing_number'] != None:
                                address_number = response_i['billing_number']
                            else:
                                address_number = ''
                            if (address_number == '') | (address_number == None):
                                address_number = response_i['shipping_address']['number']
                            if address_number == None:
                                address_number = ''
                            #
                            if response_i['billing_floor'] != None:
                                floor = response_i['billing_floor']
                            else:
                                floor = ''
                            if (floor == '') | (floor == None):
                                floor = response_i['shipping_address']['floor']
                            if floor == None:
                                floor = ''
                            #
                            if response_i['billing_locality'] != None:
                                locality = response_i['billing_locality']
                            else:
                                locality = ''
                            if (locality == '') | (locality == None):
                                locality = response_i['shipping_address']['locality']
                            if locality == None:
                                locality = ''
                            #
                            if response_i['billing_city'] != None:
                                city = response_i['billing_city']
                            else:
                                city = ''
                            if (city == '') | (city == None):
                                city = response_i['shipping_address']['city']
                            if city == None:
                                city = ' '
                            #
                            if response_i['billing_zipcode'] != None:
                                zipcode = response_i['billing_zipcode']
                            else:
                                zipcode = ''
                            if (zipcode == '') | (zipcode == None):
                                zipcode = response_i['shipping_address']['zipcode']
                            if zipcode == None:
                                zipcode = ''
                            #
                            if response_i['billing_province'] != None:
                                province = response_i['billing_province']
                            else:
                                province = ''
                            if (province == '') | (province == None):
                                province = response_i['shipping_address']['province']
                            if province == None:
                                province = ' '
                            #
                            # print('*********** MODELO Departamento ***********')
                            if city != None:
                                if (not (Department.objects.filter(name__icontains=city)).exists()):
                                    dep = Department()
                                    dep.name = city
                                    dep.save()
                            else:
                                found_department = Department.objects.filter(
                                    name__icontains='') or Department.objects.filter(name__icontains=' ')
                                if not (found_department.exists()):
                                    dep = Department()
                                    dep.name = ' '
                                    dep.save()
                            # print('Grabado exitosamente Departamento')

                            # print('*********** MODELO Provincia ***********')
                            if province != None:
                                if (not (Province.objects.filter(name__icontains=province)).exists()):
                                    prov = Province()
                                    prov.name = province
                                    prov.save()
                            else:
                                found_province = Province.objects.filter(name__icontains='')
                                if not (found_province.exists()):
                                    prov = Province()
                                    prov.name = ''
                                    prov.save()
                            # print('Grabado exitosamente Provincia')

                            # print('*********** MODELO Clientes ***********')
                            # # Guardado en Tabla de Clientes

                            cli = Client()
                            cli.name = customer_name
                            cli.email = email
                            cli.identification = identification
                            cli.phone = phone
                            cli.address = address
                            cli.number = address_number
                            cli.floor = floor
                            cli.locality = locality

                            # print("*************** Este es el ID de los Departamentos ***************")
                            if city != None:
                                found_department = Department.objects.filter(name__icontains=city)
                                if found_department.exists():
                                    cli.department_id = found_department[0].id
                            else:
                                # found_department = Department.objects.filter(name__icontains='')
                                found_department = Department.objects.filter(name__icontains=' ')
                                cli.department_id = found_department[0].id

                            # print("*************** Este es el ID de los Provincia ***************")
                            if province != None:
                                found_province = Province.objects.filter(name__icontains=province)
                                if found_province.exists():
                                    cli.province_id = found_province[0].id
                            else:
                                # found_province = Province.objects.filter(name__icontains='')
                                found_province = Province.objects.filter(name__icontains=' ')
                                cli.province_id = found_province[0].id
                            #
                            cli.postal_code = zipcode
                            cli.save()
                            # print('Grabado exitosamente Cliente')
                            # print('****************')

                # Este for es para Tiendanube /
                for response_i in response_json:
                    # Verifica si la compra no fue anulada
                    cancel_reason = response_i['cancel_reason']
                    # Ordenes canceladas u ordenes anuladas porque el pago fue rechazado
                    if cancel_reason == None and response_i['payment_status'] != 'voided':
                        # Ingresa si la orden NO WXISTE
                        if (not (TiendanubeApi.objects.filter(order_number=response_i['number']))):
                            flag = True
                            flag2 = False
                            sale_id_order_number = 0
                            #
                            for j in response_i['products']:
                                #
                                if (response_i['customer']['name'] != None):
                                    customer_name = response_i['customer']['name']
                                else:
                                    customer_name = ' '
                                if customer_name == 'Sin nombre' or customer_name == ' ':
                                    customer_name = response_i['shipping_address']['name']
                                if customer_name == None:
                                    customer_name = ' '
                                if customer_name == ' ':
                                    customer_name = response_i['customer']['billing_name']
                                #
                                if customer_name == None:
                                    customer_name = ' '
                                if customer_name == ' ':
                                    customer_name = response_i['billing_name']
                                if customer_name == None:
                                    customer_name = ' '
                                #
                                if response_i['customer']['email'] != None:
                                    email = response_i['customer']['email']
                                else:
                                    email = ''
                                #
                                if response_i['customer']['identification'] != None:
                                    identification = response_i['customer']['identification']
                                else:
                                    identification = ''
                                #
                                if response_i['billing_phone'] != None:
                                    phone = response_i['billing_phone']
                                else:
                                    phone = ''
                                if phone == None or phone == '':
                                    phone = response_i['customer']['phone']
                                if phone == None or phone == '':
                                    phone = response_i['shipping_address']['phone']
                                if phone == None:
                                    phone = ''
                                #
                                if response_i['billing_address'] != None:
                                    address = response_i['billing_address']
                                else:
                                    address = ''
                                if address == None or address == '':
                                    address = response_i['shipping_address']['address']
                                if address == None:
                                    address = ''
                                #
                                if response_i['billing_number'] != None:
                                    address_number = response_i['billing_number']
                                else:
                                    address_number = ''
                                if address_number == None or address_number == '':
                                    address_number = response_i['shipping_address']['number']
                                if address_number == None:
                                    address_number = ''
                                #
                                if response_i['billing_floor'] != None:
                                    floor = response_i['billing_floor']
                                else:
                                    floor = ''
                                if floor == None or floor == '':
                                    floor = response_i['shipping_address']['floor']
                                if floor == None:
                                    floor = ''
                                #
                                if response_i['billing_locality'] != None:
                                    locality = response_i['billing_locality']
                                else:
                                    locality = ''
                                if locality == None or locality == '':
                                    locality = response_i['shipping_address']['locality']
                                if locality == None:
                                    locality = ''
                                #
                                if response_i['billing_city'] != None:
                                    city = response_i['billing_city']
                                else:
                                    city = ''
                                if city == None or city == '':
                                    city = response_i['shipping_address']['city']
                                if city == None:
                                    city = ' '
                                #
                                if response_i['billing_zipcode'] != None:
                                    zipcode = response_i['billing_zipcode']
                                else:
                                    zipcode = ''
                                if zipcode == None or zipcode == '':
                                    zipcode = response_i['shipping_address']['zipcode']
                                if zipcode == None:
                                    zipcode = ''
                                #
                                if response_i['billing_province'] != None:
                                    province = response_i['billing_province']
                                else:
                                    province = ''
                                if province == None or province == '':
                                    province = response_i['shipping_address']['province']
                                if province == None:
                                    province = ' '
                                #
                                if response_i['billing_country'] != None:
                                    country = response_i['billing_country']
                                else:
                                    country = ''
                                if country == None or country == '':
                                    country = response_i['shipping_address']['country']
                                if country == None:
                                    country = ''
                                #
                                product_price = j['price']
                                product_quantity = j['quantity']
                                gateway = response_i['gateway']
                                oder_status = response_i['status']
                                payment_status = response_i['payment_status']
                                currency = response_i['currency']
                                subtotal = response_i['subtotal']
                                discount_coupon = response_i['discount_coupon']
                                discount = response_i['discount']
                                shipping_cost_customer = response_i['shipping_cost_customer']
                                total = response_i['total']
                                shipping_option = response_i['shipping_option']
                                customer_note = response_i['note']
                                owner_note = response_i['owner_note']
                                gateway_id = response_i['gateway_id']
                                id_order_number = response_i['id']

                                # print('*********** MODELO Tiendanube ***********')
                                tiendanubeapi = TiendanubeApi()
                                tiendanubeapi.order_number = response_i['number']
                                tiendanubeapi.created_at = self.convert_date(response_i['created_at'])
                                tiendanubeapi.customer_name = customer_name
                                tiendanubeapi.customer_email = email
                                tiendanubeapi.customer_identification = identification
                                tiendanubeapi.customer_phone = phone
                                tiendanubeapi.customer_address = address
                                tiendanubeapi.customer_number = address_number
                                tiendanubeapi.customer_floor = floor
                                tiendanubeapi.customer_locality = locality
                                tiendanubeapi.customer_city = city
                                tiendanubeapi.customer_zipcode = zipcode
                                tiendanubeapi.customer_province = province
                                tiendanubeapi.customer_country = country

                                productImagesId = int(j['product_id'])
                                if not (Product.objects.filter(name__icontains=j['name'])).exists():
                                    product = Product()
                                    product.name = j['name']
                                    if product_price == None or product_price == '':
                                        product.pvp = 0.0
                                    else:
                                        product.pvp = float(j['price'])
                                    product.productimageid = productImagesId
                                    product.save()
                                else:
                                    prods = Product.objects.filter(name__icontains=j['name'])
                                    for i in prods:
                                        if product_price == None or product_price == '':
                                            i.pvp = 0.0
                                        else:
                                            i.pvp = float(j['price'])
                                        i.productimageid = productImagesId
                                        i.save()

                                tiendanubeapi.product_name = j['name']
                                #
                                if product_price == None or product_price == '':
                                    tiendanubeapi.product_price = 0.0
                                else:
                                    tiendanubeapi.product_price = float(j['price'])
                                #
                                if product_quantity == None or product_quantity == '':
                                    tiendanubeapi.product_quantity = 0
                                else:
                                    tiendanubeapi.product_quantity = int(j['quantity'])
                                #
                                # tiendanubeapi.product_image = j['image']['src']1
                                #
                                tiendanubeapi.paid_at = self.convert_date(response_i['paid_at'])
                                #
                                if gateway == None:
                                    tiendanubeapi.gateway = ''
                                else:
                                    tiendanubeapi.gateway = response_i['gateway']
                                #
                                if oder_status == None:
                                    tiendanubeapi.oder_status = ''
                                else:
                                    tiendanubeapi.oder_status = response_i['status']
                                #
                                if payment_status == None:
                                    tiendanubeapi.payment_status = ''
                                else:
                                    tiendanubeapi.payment_status = response_i['payment_status']
                                #
                                if currency == None:
                                    tiendanubeapi.currency = ''
                                else:
                                    tiendanubeapi.currency = response_i['currency']
                                #
                                if subtotal == None or subtotal == '':
                                    tiendanubeapi.subtotal = 0.0
                                else:
                                    tiendanubeapi.subtotal = float(response_i['subtotal'])
                                #
                                if discount_coupon == None:
                                    tiendanubeapi.discount_coupon = ''
                                else:
                                    tiendanubeapi.discount_coupon = response_i['discount_coupon']
                                #
                                if discount == None or discount == '':
                                    tiendanubeapi.discount = 0.0
                                else:
                                    tiendanubeapi.discount = float(response_i['discount'])
                                #
                                if shipping_cost_customer == None or shipping_cost_customer == '':
                                    tiendanubeapi.shipping_cost_customer = 0.0
                                else:
                                    tiendanubeapi.shipping_cost_customer = float(response_i['shipping_cost_customer'])
                                #
                                if total == None or total == '':
                                    tiendanubeapi.total = 0.0
                                else:
                                    tiendanubeapi.total = float(response_i['total'])
                                #
                                if shipping_option == None:
                                    tiendanubeapi.shipping_option = ''
                                else:
                                    tiendanubeapi.shipping_option = response_i['shipping_option']
                                #
                                if customer_note == None:
                                    tiendanubeapi.customer_note = ''
                                else:
                                    tiendanubeapi.customer_note = response_i['note']
                                #
                                if owner_note == None:
                                    tiendanubeapi.owner_note = ''
                                else:
                                    tiendanubeapi.owner_note = response_i['owner_note']
                                #
                                if gateway_id == None:
                                    tiendanubeapi.gateway_id = ''
                                else:
                                    tiendanubeapi.gateway_id = response_i['gateway_id']
                                #
                                if id_order_number == None:
                                    tiendanubeapi.order_id = ''
                                else:
                                    tiendanubeapi.order_id = response_i['id']
                                #
                                tiendanubeapi.save()
                                #
                                #
                                if product_price == None or product_price == '':
                                    product_price = 0.0
                                else:
                                    product_price = float(j['price'])
                                #
                                if product_quantity == None or product_quantity == '':
                                    product_quantity = 0
                                else:
                                    product_quantity = int(j['quantity'])
                                #
                                customer_note = response_i['note']
                                owner_note = response_i['owner_note']
                                #
                                ############################################################
                                # Esto es para cuando todos los productos tengas ID
                                ############################################################
                                found_products = Product.objects.filter(name__icontains=j['name'])
                                # INICIALIZO EL CONFECIONISTA EN 1
                                dress_making = Dressmaking.objects.filter(id=1)
                                process = Processes()
                                process.process_prod_id = found_products[0].id
                                #
                                if customer_note == None:
                                    customer_note = ''
                                else:
                                    customer_note = response_i['note']
                                #
                                if owner_note == None:
                                    owner_note = ''
                                else:
                                    owner_note = response_i['owner_note']
                                #
                                if customer_note != '' or owner_note != '':
                                    process.observation = '[ Cliente Nota = ' + customer_note + ' ] - [ Nuestra Nota = ' + owner_note + ' ]'
                                else:
                                    process.observation = ''
                                #
                                if product_quantity == None or product_quantity == '':
                                    process.quantity = 0
                                else:
                                    process.quantity = int(j['quantity'])
                                #
                                if response_i['payment_status'] == 'pending':
                                    process.state_id = Estados.objects.get(state_type='Pendiente').id
                                else:
                                    process.state_id = Estados.objects.get(state_type='Diseño').id
                                process.destiny = 'Tiendanube 1'
                                process.process_date_joined = self.convert_date(response_i['created_at'])
                                process.process_date_finish = datetime.now() + timedelta(7)
                                process.order_id = response_i['id']
                                process.product_id = found_products[0].id
                                process.order_number = "TN01-" + str(response_i['number'])
                                #
                                for p in Product.objects.filter(name__icontains=j['name']):
                                    process.image = p.image
                                    break
                                process.state_dressmaking_id = dress_making[0].id
                                process.save()
                                # print('Grabado exitosamente Procesos')

                                # Si la orden está pendiente de pago entra en Pendientes (MODELO PENDING)

                                if response_i['payment_status'] == 'pending':
                                    ppending = ProcessesPending()
                                    ppending.process_prod_id = found_products[0].id
                                    #
                                    if customer_note != '' or owner_note != '':
                                        ppending.observation = '[ Cliente Nota = ' + customer_note + ' ] - [ Nuestra Nota = ' + owner_note + ' ]'
                                    else:
                                        ppending.observation = ''
                                    #
                                    if product_quantity == None or product_quantity == '':
                                        ppending.quantity = 0
                                    else:
                                        ppending.quantity = int(j['quantity'])
                                    #
                                    ppending.state_id = Estados.objects.get(state_type='Pendiente').id
                                    ppending.destiny = 'Tiendanube 1'
                                    ppending.process_date_joined = self.convert_date(response_i['created_at'])
                                    ppending.process_date_finish = datetime.now() + timedelta(7)
                                    ppending.order_id = response_i['id']
                                    ppending.product_id = found_products[0].id
                                    ppending.order_number = "TN01-" + str(response_i['number'])
                                    #
                                    for p in Product.objects.filter(name__icontains=j['name']):
                                        ppending.image = p.image
                                        break
                                    ppending.state_dressmaking_id = dress_making[0].id
                                    ppending.save()

                                # Si la orden NO está pendiente de pago ingresa a Disenio (MODELO DESING)
                                else:
                                    pdesing = ProcessesDesign()
                                    pdesing.process_prod_id = found_products[0].id
                                    #
                                    if customer_note != '' or owner_note != '':
                                        pdesing.observation = '[ Cliente Nota = ' + customer_note + ' ] - [ Nuestra Nota = ' + owner_note + ' ]'
                                    else:
                                        pdesing.observation = ''
                                    #
                                    if product_quantity == None or product_quantity == '':
                                        pdesing.quantity = 0
                                    else:
                                        pdesing.quantity = int(j['quantity'])
                                    #
                                    pdesing.state_id = Estados.objects.get(state_type='Diseño').id
                                    pdesing.destiny = 'Tiendanube 1'
                                    pdesing.process_date_joined = self.convert_date(response_i['created_at'])
                                    pdesing.process_date_finish = datetime.now() + timedelta(7)
                                    pdesing.order_id = response_i['id']
                                    pdesing.product_id = found_products[0].id
                                    pdesing.order_number = "TN01-" + str(response_i['number'])
                                    #
                                    for p in Product.objects.filter(name__icontains=j['name']):
                                        pdesing.image = p.image
                                        break
                                    pdesing.state_dressmaking_id = dress_making[0].id
                                    pdesing.save()
                                    # print('Grabado exitosamente Disenio')

                                # print('*********** MODELO Sale AND DateSale ***********')
                                if flag == True:
                                    client = (Client.objects.filter(name__icontains=customer_name) and Client.objects.filter(email__icontains=email))
                                    flag2 = True
                                    sale = Sale()
                                    sale.cli_id = client[0].id
                                    sale.date_joined = self.convert_date(response_i['created_at'])
                                    #
                                    if subtotal == None:
                                        sale.subtotal = 0.0
                                    else:
                                        sale.subtotal = float(response_i['subtotal'])
                                    #
                                    if discount == None:
                                        sale.iva = 0.0
                                    else:
                                        sale.iva = float(response_i['discount'])
                                    #
                                    if total == None:
                                        sale.total = 0.0
                                    else:
                                        sale.total = float(response_i['total'])
                                    #
                                    sale.save()
                                    sale.order_number = "TN01-" + str(response_i['number'])
                                    sale_id_order_number = sale.id
                                    sale.save()
                                    flag = False
                                    #
                                # Detalle de la venta
                                if flag2 == True:
                                    products_name = Product.objects.filter(name__icontains=j['name'])
                                    det = DetSale()
                                    det.sale_id = sale_id_order_number
                                    det.prod_id = products_name[0].id
                                    #
                                    if product_quantity == None:
                                        det.cant = 0
                                        cantities = 0
                                    else:
                                        # det.cant = int(j['quantity'])
                                        # cantities = int(j['quantity'])
                                        det.cant = int(product_quantity)
                                        cantities = int(product_quantity)
                                    #
                                    if product_price == None:
                                        det.price = 0.0
                                        preices = 0.0
                                    else:
                                        # det.price = float(j['price'])
                                        # preices = float(j['price'])
                                        det.price = float(product_price)
                                        preices = float(product_price)
                                    #
                                    det.subtotal = preices * cantities
                                    det.save()

            else:
                response = {'error': 'No ha ingresado a ninguna opción'}
        except Exception as e:
            response['error'] = str(e)
        finally:
            print(response)
        return JsonResponse(response, safe=False)


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Tiendanube API'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['action'] = 'add'
        context['list_url'] = self.success_url
        return context


# AGREGO PRODUCTOS A UNA ORDEN DE Tiendanube
class SaleTiendanubeApiCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = TiendanubeApi
    model1 = Product
    model2 = Processes
    model3 = Estados
    model4 = Sale
    model5 = DetSale
    form_class = ProcessesForm
    form_class_second = TiendanubeForm
    template_name = 'tiendanube/createapiadd.html'
    success_url = reverse_lazy('productos_app:tiendanube-list')
    permission_required = 'add_tiendanubeapi'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            # # SEARCH PRODUCTS AUTOCOMPLETE
            if action == 'search_products':
                data = []
                ids_exclude = json.loads(request.POST['ids'])
                term = request.POST['term']
                products = Product.objects.filter()
                if len(term):
                    products = products.filter(name__icontains=term)
                for i in products.exclude(id__in=ids_exclude):   #[0:10]:
                    item = i.toJSON()
                    item['value'] = i.name
                    data.append(item)
            # SEARCH PRODUCTS SELECT-2
            elif action == 'search_autocomplete':
                data = []
                ids_exclude = json.loads(request.POST['ids'])
                term = request.POST['term'].strip()
                data.append({'id': term, 'text': term})
                products = Product.objects.filter(name__icontains=term)
                for i in products.exclude(id__in=ids_exclude):   #[0:10]:
                    item = i.toJSON()
                    item['text'] = i.name
                    data.append(item)
            # # SEARCH ORDER NUMBER
            elif action == 'search_order_numbers':
                data = []
                tnon = TiendanubeApi.objects.filter(order_number__icontains=request.POST['term'])   #[0:10]
                for i in tnon:
                    item = i.toJSON()
                    # item['value'] = i.order_number
                    item['text'] = i.order_number
                    data.append(item)
            elif action == 'add':
                with transaction.atomic():
                    vents = json.loads(request.POST['vents'])
                    var_client = ''
                    for i in vents['products']:
                        tnApi = TiendanubeApi()
                        tnApi.order_number = i['order_number']
                        tnApi.created_at = vents['date_joined']
                        tnApi.customer_name = i['customer_name']
                        tnApi.customer_email = i['customer_email']
                        tnApi.customer_identification = i['customer_identification']
                        tnApi.customer_phone = i['customer_phone']
                        tnApi.customer_address = i['customer_address']
                        tnApi.customer_number = i['customer_number']
                        tnApi.customer_floor = i['customer_floor']
                        tnApi.customer_locality = i['customer_locality']
                        tnApi.customer_city = i['customer_city']
                        tnApi.customer_zipcode = i['customer_zipcode']
                        tnApi.customer_province = i['customer_province']
                        tnApi.customer_country = i['customer_country']
                        tnApi.product_id = i['id']
                        tnApi.product_name = i['name']
                        tnApi.product_price = float(i['pvp'])
                        tnApi.product_quantity = int(i['cant'])
                        for p in Product.objects.filter(name__icontains=i['name']):
                            tnApi.product_image = p.image
                        tnApi.paid_at = vents['date_joined']
                        tnApi.subtotal = float(vents['subtotal'])
                        tnApi.discount = float(vents['iva'])
                        tnApi.total = float(vents['total'])
                        tnApi.order_id = i['order_id']
                        tnApi.save()

                        search_products = Product.objects.filter(name__icontains=i['name'])
                        # INICIALIZO EL CONFECIONISTA EN 1
                        dress_making = Dressmaking.objects.filter(id=1)
                        proces = Processes()
                        proces.process_prod_id = search_products[0].id
                        proces.quantity = int(i['cant'])
                        proces.state_id = Estados.objects.get(state_type='Diseño').id
                        proces.destiny = 'Tiendanube 1'
                        proces.process_date_joined = vents['date_joined']
                        proces.process_date_finish = datetime.now() + timedelta(7)
                        proces.order_id = i['order_id']
                        proces.product_id = i['id']
                        proces.order_number = "TN01-" + i['order_number']
                        for p in Product.objects.filter(name__icontains=i['name']):
                            proces.image = p.image
                        proces.state_dressmaking_id = dress_making[0].id
                        proces.save()

                        design = ProcessesDesign()
                        design.process_prod_id = search_products[0].id
                        design.quantity = int(i['cant'])
                        design.state_id = Estados.objects.get(state_type='Diseño').id
                        design.destiny = 'Tiendanube 1'
                        design.process_date_joined = vents['date_joined']
                        design.process_date_finish = datetime.now() + timedelta(7)
                        design.order_id = i['order_id']
                        design.product_id = i['id']
                        design.order_number = "TN01-" + i['order_number']
                        for p in Product.objects.filter(name__icontains=i['name']):
                            design.image = p.image
                        design.state_dressmaking_id = dress_making[0].id
                        design.save()

                        var_client = Client.objects.filter(name__icontains=i['customer_name']) and Client.objects.filter(identification=i['customer_identification'])

                    # print('*********** MODELO Sale AND DateSale ***********')
                    var_order_number = "TN01-" + str(i['order_number'])
                    for i in Sale.objects.all().order_by('-id'):
                        if (i.order_number == var_order_number):
                            client = var_client
                            i.cli_id = client[0].id
                            i.date_joined = vents['date_joined']
                            i.subtotal = float(vents['subtotal']) + float(i.subtotal)
                            i.iva = float(vents['iva']) + float(i.iva)
                            i.total = float(vents['total']) + float(i.total)
                            i.order_number = var_order_number
                            i.save()
                            for j in vents['products']:
                                det = DetSale()
                                det.sale_id = i.id
                                det.prod_id = j['id']
                                det.price = float(j['pvp'])
                                det.cant = int(j['cant'])
                                det.subtotal = float(j['subtotal'])
                                det.save()
                            break

            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.form_class_second(self.request.GET)
        context['title'] = 'Tiendanube API - Agregar Productos a una Orden'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['action'] = 'add'
        context['list_url'] = self.success_url
        return context


# Modifica un Número de Orden
class TiendanubeUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = TiendanubeApi
    form_class = TiendanubeForm
    template_name = 'tiendanube/create.html'
    success_url = reverse_lazy('productos_app:tiendanube-list')
    permission_required = 'change_tiendanubeapi'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Editar Registro de Tiendanube'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['action'] = 'edit'
        context['list_url'] = self.success_url
        return context

# Elimina un Número de Orden
class TiendanubeDeleteView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    model = TiendanubeApi
    form_class = TiendanubeForm
    success_url = reverse_lazy('productos_app:tiendanube-list')
    permission_required = 'delete_tiendanubeapi'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminar Registro de Tiendanube'
        context['nav_productions'] = 'Producción'
        context['nav_tiendanube_api'] = 'Tiendanube API'
        context['nav_sale'] = 'Ventas'
        context['list_url'] = self.success_url
        return context


