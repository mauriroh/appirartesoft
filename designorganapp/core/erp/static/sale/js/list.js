let tblSale;
// Da el formato a la tabla que se despliga debajo de la fila al precioanr le boón (+) frl DataTable
function format(d) {
    // console.log(d);
    let html = '<table class="table">';

    html += '<thead style="background-color:var(--structure_blue3);" class="text-white">';
        html += '<tr><th scope="col">Nombre Producto</th>';
        html += '<th scope="col">Precio de Venta</th>';
        html += '<th scope="col">Cantidad</th>';
        html += '<th scope="col">Subtotal</th></tr>';
    html += '</thead>';
    html += '<tbody>';
        $.each(d.det, function (key, value) {
            html+='<tr>'
                html+='<td>'+value.prod.name+'</td>'
                html+='<td>'+value.price+'</td>'
                html+='<td>'+value.cant+'</td>'
                html+='<td>'+value.subtotal+'</td>'
            html+='</tr>';
        });
    html += '</tbody>';

    return html;
}

// Table de la vista "Listado de Ventas"
$(function () {
    tblSale = $('#data').DataTable({
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla =(",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        },
        scrollX: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 25,
        order: [[ 1, 'desc' ]],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: "",
            headers: {
                'X-CSRFToken': csrftoken
            }
        },
        columns: [
            //Esto muestra el detalle de la fila usando el DataTable botón (+)
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {"data": "id"},
            {"data": "order_number"},
            {"data": "cli.name"},
            {"data": "date_joined"},
            {"data": "subtotal"},
            {"data": "iva"},
            {"data": "total"},
        ],
        columnDefs: [
            {
                targets: [-7],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a rel="details" style="background-color:var(--structure_blue3); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-magnifying-glass"></i></a>';
                    buttons += '<a href="/erp/sale/invoice/pdf/' + row.id + '/" target="_blank" style="background-color:var(--structure_red); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-file-pdf"></i></a>';
                    buttons += '<a href="/erp/sale/delete/' + row.id + '/" style="background-color:var(--structure_red); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-trash-can"></i></a>';
                    return buttons;
                }
            },
            {
                targets: [-5, -6],
                class: 'text-left',
            },
            {
                targets: [-1, -2, -3],
                class: 'text-left',
                orderable: false,
                render: function (data, type, row) {
                    return '$ ' + parseFloat(data).toFixed(2);
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });

    // Modal que mustra el detalle de la venta
    $('#data tbody')
        .on('click', 'a[rel="details"]', function () {
            let tr = tblSale.cell($(this).closest('td, li')).index();
            let data = tblSale.row(tr.row).data();
            // console.log(data);
            // Envia los datos a la vista ListView
            $('#tblDet').DataTable({
                language: {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla =(",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "copy": "Copiar",
                        "colvis": "Visibilidad"
                    }
                },
                responsive: true,
                // scrollX: true,
                autoWidth: false,
                destroy: true,
                deferRender: true,
                //pageLength: 100,
                //data: data.det, // Este metodo NO ES en tiempo reeal como lo es con Ajax (paso siguiente)
                ajax: {
                    url: window.location.pathname,
                    type: 'POST',
                    data: {
                        'action': 'search_details_prod',
                        'id': data.id
                    },
                    dataSrc: "",
                    headers: {
                        'X-CSRFToken': csrftoken
                    }
                },
                columns: [
                    {"data": "prod.name"},
                    {"data": "price"},
                    {"data": "cant"},
                    {"data": "subtotal"},
                ],
                columnDefs: [
                    {
                        targets: [-1, -3],
                        class: 'text-left',
                        orderable: false,
                        render: function (data, type, row) {
                            return '$ ' + parseFloat(data).toFixed(2);
                        }
                    },
                    {
                        targets: [-2],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return data;
                        }
                    },
                    {
                        targets: [-4],
                        class: 'text-left',
                    },
                ],
                initComplete: function (settings, json) {

                }
            });
            // Una vez que se tiene los datos activamos el modal
            $('#myModelDet').modal('show');
        })
        //Evento que se activa al precioanr el botón (+) del DataTable
        .on('click', 'td.details-control', function () {
            let tr = $(this).closest('tr');
            let row = tblSale.row( tr );
            // Open the component
            if ( row.child.isShown() ) {
                // If this row is already open, then close the component
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row. Whit method format show the data
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        });

});