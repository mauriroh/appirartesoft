$(function () {
    $('#data').DataTable({
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla =(",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        },
        scrollX: true,
        paging: true,
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 25,
        order: [[ 1, 'asc' ]],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: "",
            headers: {
                'X-CSRFToken': csrftoken
            }
        },
        columns: [
            {"data": "id"},
            {"data": "services_state.name"},
            {"data": "name"},
            {"data": "client_number"},
            {"data": "date_joined"},
            {"data": "date_finish"},
            {"data": "total"},
        ],
        columnDefs: [
            {
                targets: [-7],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/erp/services/update/' + row.id + '/" style="background-color:var(--structure_blue4); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-pen-clip"></i></a>';
                    buttons += '<a href="/erp/services/delete/' + row.id + '/" style="background-color:var(--structure_red); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-trash-can"></i></a>';
                    return buttons;
                }
            },
            {
                targets: [-6],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    if(row.services_state.id === 1 ){
                        return '<span style="background-color:var(--structure_red); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.services_state.id === 2 ){
                        return '<span style="background-color:var(--structure_blue4); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                }
            },
            {
                targets: [-5, -4],
                class: 'text-left',
            },
            {
                targets: [-2],
                class: 'text-danger',
            },
            {
                targets: [-1],
                class: 'text-left',
                orderable: false,
                render: function (data, type, row) {
                    return '$ ' + parseFloat(data).toFixed(2);
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});
