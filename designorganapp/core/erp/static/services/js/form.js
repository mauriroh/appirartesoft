let serv = {
    items: {
        name:'',
        client_number:'',
        date_joined: '',
        date_finish: '',
        total:0.0,
        services_state:','
    },
};
/***************************************************
                    FUNCTIONS
 ***************************************************/
$(function () {

    $('input[name="date_joined"]').prop("disabled","disabled");

    $('#date_joined').datetimepicker({
        format: 'YYYY-MM-DD',
        date: moment().format("YYYY-MM-DD"),
        locale: 'es',
        minDate: moment().format("YYYY-MM-DD")
    });

    $('#date_finish').datetimepicker({
        format: 'YYYY-MM-DD',
        date: moment().format("YYYY-MM-DD"),
        locale: 'es',
        minDate: moment().format("YYYY-MM-DD")
    });

    // SUBMIT DATA ViewDjango
    $('form').on('submit', function (e) {
        e.preventDefault();
        serv.items.name = $('input[name="name"]').val();
        serv.items.client_number = $('input[name="client_number"]').val();
        serv.items.date_joined = $('input[name="date_joined"]').val();
        serv.items.date_finish = $('input[name="date_finish"]').val();
        serv.items.total = $('input[name="total"]').val();
        serv.items.services_state = $('select[name="services_state"]').val();

        let parameters = new FormData();
        parameters.append('action', $('input[name="action"]').val());
        parameters.append('serv', JSON.stringify(serv.items));

        submit_with_ajax(window.location.pathname, 'Notificación', '¿Estas seguro de realizar la siguiente acción?', parameters, function (response) {
            location.href = '/erp/services/list/';
        });
    });
});
