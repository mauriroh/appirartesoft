$(function () {
    $('#data').DataTable({
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla =(",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        },
        scrollX: true,
        paging: true,
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        //pageLength: 100,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: "",
            headers: {
                'X-CSRFToken': csrftoken
            }
        },
        columns: [
            {"data": "id"},
            {"data": "id"},
            {"data": "state_type"},
        ],
        columnDefs: [
            {
                targets: [-2],
                class: 'text-left',
                orderable: false,
                render: function (data, type, row) {
                    return buttons = '<a href="/erp/states/update/' + row.id + '/" style="background-color:var(--structure_blue4); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-pen-clip"></i></a>';
                }

            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    if(row.id === 1){
                        return '<span style="background-color:var(--color_iconos_1_8); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.id === 2){
                        return '<span style="background-color:var(--color_iconos_2_7); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.id === 3){
                        return '<span style="background-color:var(--color_iconos_3); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.id === 4){
                        return '<span style="background-color:var(--color_iconos_4); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.id === 5){
                        return '<span style="background-color:var(--color_estructura_1); FONT-SIZE: 11pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.id === 6){
                        return '<span style="background-color:var(--color_estructura_2); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.id === 7){
                        return '<span style="background-color:var(--color_iconos_2_7); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.id === 8){
                        return '<span style="background-color:var(--color_iconos_1_8); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    return '<span style="background-color:var(--structure_red); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$'+parseFloat(data).toFixed(2);
                }
            },

        ],
        initComplete: function (settings, json) {

        }
    });

});