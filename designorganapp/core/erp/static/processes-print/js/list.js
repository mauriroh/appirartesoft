$(function () {
    $('#data').DataTable({
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla =(",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        },
        scrollX: true,
        paging: true,
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 100,
        order: [[ 2, 'asc' ]],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: "",
            headers: {
                'X-CSRFToken': csrftoken
            }
        },
        columns: [
            {"data": "id"},
            {"data": "state.state_type"},
            {"data": "order_number"},
            {"data": "process_date_joined"},
            {"data": "process_date_finish"},
            {"data": "process_prod.name"},
            {"data": "image"},
            {"data": "quantity"},
            {"data": "observation"},
        ],
        columnDefs: [
            {
                targets: [-9],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    buttons = '<a href="/erp/processes-print/update/' + row.id + '/" style="background-color:var(--structure_blue4); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-pen-clip"></i></a>';
                    buttons += '<a href="/erp/processes-print/detail/' + row.id + '/" style="background-color:var(--structure_blue3); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-magnifying-glass"></i></a>';
                    return buttons;
                }
            },
            {
                targets: [-3],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '<img src="'+data+'" class="img-fluid d-block mx-auto" style="width: 50px; height: 50px;">';
                }
            },
            {
                targets: [-1, -2, -4, -7],
                class: 'text-left',
            },
            {
                targets: [-8],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '<span style="background-color:var(--color_iconos_3); FONT-SIZE: 10pt" class="badge text-white">' + data + '</span>'
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });

});

