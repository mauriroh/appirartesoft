/******************************************************************************
                        FUNCIONES - VALIDACION DE PRODUCTOS
******************************************************************************/
/* Nombre del Producto */
$('input[name="name"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9-áéíóúÁÉÍÓÚÑñüÜ(), ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Stock */
$('input[name="stock"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Precio de Venta */
$('input[name="pvp"]').bind('keypress', function(event) {
// "^[0-9]{1,10}[\.][0-9]{1,2}$");
    let regex = new RegExp("[0-9.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});