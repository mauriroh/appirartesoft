let tblProducts;
let tblSearchProducts;

let vents = {
    items: {
        cli: '',
        date_joined: '',
        subtotal: 0.00,
        iva: 0.00,
        total: 0.00,
        products: []
    },

    get_ids: function () {
        let ids = [];
        $.each(this.items.products, function (key, value) {
            ids.push(value.id);
        });
        return ids;
    },
    /******************************************************************************
                            Cálculos
    ******************************************************************************/
    calculate_invoice: function () { // vídeo 60 => 8:00
        let var_subtotal = 0.00;
        // var var_discount = $('input[name="discount"]').val();
        // var var_additional_cost2 = $('input[name="additional_cost"]').val();
        let var_iva = $('input[name="iva"]').val();
        $.each(this.items.products, function (pos, dict) {
            // dict.pos = pos;
            dict.subtotal = dict.cant * parseFloat(dict.pvp);
            var_subtotal += dict.subtotal;
        });
        this.items.subtotal = var_subtotal;
        this.items.iva = this.items.subtotal * var_iva;
        // this.items.discount = var_discount * (-1);
        // this.items.additional_cost2 = var_additional_cost2 * 1;
        // this.items.total = this.items.subtotal + this.items.iva + this.items.discount + this.items.additional_cost2;
        this.items.total = this.items.subtotal - this.items.iva;

        $('input[name="subtotal"]').val(this.items.subtotal.toFixed(2));
        $('input[name="ivacalc"]').val(this.items.iva.toFixed(2));
        $('input[name="total"]').val(this.items.total.toFixed(2));

    },
    /******************************************************************************
     Lista los productos que son seleccionados en el BOX "Buscador de Productos" en la tabla
    ******************************************************************************/
    add: function (item) {
        this.items.products.push(item);
        this.list();
    },
    list: function () {
        this.calculate_invoice();
        tblProducts = $('#tblProducts').DataTable({
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla =(",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            },
            responsive: true,
            autoWidth: false,
            destroy: true,
            data: this.items.products,
            columns: [
                {"data": "id"},
                {"data": "name"},
                {"data": "pvp"},
                {"data": "cant"},
                {"data": "subtotal"},
            ],
            columnDefs: [
                {
                    targets: [0],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<a rel="remove" style="background-color:var(--structure_red); FONT-SIZE: 12pt" class="btn badge text-white"><i class="fa-solid fa-trash-can"></i></a>';
                    }
                },
                {
                    targets: [-1],
                    class: 'text-left',
                    orderable: false,
                    render: function (data, type, row) {
                        return '$ ' + parseFloat(data).toFixed(2);
                    }
                },
                {
                    targets: [-2],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<input type="text" name="cant" class="form-control form-control-sm input-sm text-center" autocomplete="off" value="' + row.cant + '">';
                    }
                },
                {
                    targets: [-4],
                    class: 'text-left',
                },
            ],
            /*Este registra los cambios que se editan dentro de la tabla.
              sobre la columna de cantidades */
            rowCallback(row, data, displayNum, displayIndex, dataIndex) {
                $(row).find('input[name="cant"]').TouchSpin({
                    min: 1,
                    max: 1000000000,
                    // max:data.stock,
                    step: 1,
                    buttondown_class: 'input-group-text',   // Alinea el botón baja al input
                    buttonup_class: 'input-group-text',
                });
                //     console.log(data);
            },
            initComplete: function (settings, json) {

            }
        });
        // console.log(this.get_ids());
    },
};

/******************************************************************************
                    FUNCTION SHOW IMAGE IN Select2
******************************************************************************/
function formatRepo(repo) {
    if (repo.loading) { // Este pedaso de código hace, cuando no hay nada en el select2, lo deja vacío, sino largaría un error
        return repo.text;
    }

    if (!Number.isInteger(repo.id)) {
        return repo.text;
    }

    let option = $(
        '<div class="wrapper container">' +
            '<div class="row">' +
                '<div class="col-lg-1">' +
                    '<img src="' + repo.image + '" class="img-fluid d-block mx-auto rounded" style="width: 60px; height: 40px;">' +
                '</div>' +
                '<div class="col-lg-11 text-left shadow-sm">' +
                    //'<br>'
                    '<p style="margin-bottom: 0;">' +
                        '<b>Producto:</b> ' + repo.name + '<br>' +
                        '<b>Stock:</b> <span style="background-color:var(--search_product_stock); FONT-SIZE: 10pt" class="badge text-white">' + repo.stock + '</span><br>' +
                        '<b>Precio:</b> <span style="background-color:var(--search_product_price); FONT-SIZE: 10pt" class="badge text-white"> $ ' + repo.pvp + '</span>' +
                    '</p>' +
                '</div>' +
            '</div>' +
        '</div>');
    return option;
}
/******************************************************************************
                                    FUNCIONES
******************************************************************************/
$(function () {
    $('.select2').select2({
        theme: "bootstrap4",
        language: 'es'
    });

    $('#date_joined').datetimepicker({
        format: 'YYYY-MM-DD',
        date: moment().format("YYYY-MM-DD"),
        locale: 'es',
        //minDate: moment().format("YYYY-MM-DD")
    });
/******************************************************************************
                        Touchspin: Descuento en %
******************************************************************************/
    $("input[name='iva']").TouchSpin({
        min: 0,
        max: 100,
        step: 0.01, // Incrementador de 0.01 en 0.01
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%',
        buttondown_class: 'input-group-text',   // Alinea el botón baja al input
        buttonup_class: 'input-group-text',     // Alinea el botón subir input
    }).on('change', function () {
        vents.calculate_invoice();
    })
        .val(0.00);

    /******************************************************************************
                        Touchspin: Descuento por número
    ******************************************************************************/
    $("input[name='discount']").TouchSpin({
        min: 0,
        max: 100000000,
        step: 1, // Incrementador de 0.01 en 0.01
        // decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        prefix: '$',
        buttondown_class: 'input-group-text',   // Alinea el botón baja al input
        buttonup_class: 'input-group-text',     // Alinea el botón subir input
    }).on('change keyup', function () {
        vents.calculate_invoice();
    })
        .val(0);

     /******************************************************************************
                        Touchspin - Costo Adicional
    ******************************************************************************/
    $("input[name='additional_cost']").TouchSpin({
        min: 0,
        max: 100000000,
        step: 1, // Incrementador de 0.01 en 0.01
        // decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        prefix: '$',
        buttondown_class: 'input-group-text',   // Alinea el botón baja al input
        buttonup_class: 'input-group-text',     // Alinea el botón subir input
    }).on('change keyup', function () {
        vents.calculate_invoice();
    })
        .val(0);

    /******************************************************************************
                        Search Clients
    ******************************************************************************/
    $('select[name="cli"]').select2({
        theme: "bootstrap4",
        language: 'es',
        allowClear: true,
        ajax: {
            delay: 250,
            type: 'POST',
            url: window.location.pathname,
            data: function (params) {
                let queryParameters = {
                    term: params.term,
                    action: 'search_clients'
                }
                return queryParameters;
            },
            headers: {
                'X-CSRFToken': csrftoken
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
        },
        placeholder: 'Ingrese Nombre | DNI',
        minimumInputLength: 1,
    });

    /******************************************************************************
                                    BOTONES
    ******************************************************************************/
    // Buttons Add Clients
    $('.btnAddClient').on('click', function () {
        // Modal Clients
        $('#ModalFormClients').modal('show');
    });

    // Limpia el Modal cuando se cierra
    $('#ModalFormClients').on('hidden.bs.modal', function (e) {
        $('#frmClient').trigger('reset');
    })

    /******************************************************************************
                        Event Submit Add Client
    ******************************************************************************/
    $('#frmClient').on('submit', function (e) {
        e.preventDefault();
        let parameters = new FormData(this);
        parameters.append('action', 'create_client');

        submit_with_ajax(window.location.pathname, 'Notificación',
            '¿Estas seguro de crear un nuevo Cliente?', parameters, function (response) {
                // console.log(response);
                let newOption = new Option(response.full_name, response.id, false, true);
                $('select[name="cli"]').append(newOption).trigger('change');
                $('#ModalFormClients').modal('hide');
            });
    });

    /******************************************************************************
                        EButton Limpiar Tabla de Productos
    ******************************************************************************/
    $('.btnRemoveAll').on('click', function () {
        if (vents.items.products.length === 0) return false;
        alert_action('Notificación', '¿Estás seguro de eliminar todos los items del listado?', function () {
            vents.items.products = [];
            vents.list();
        }, function () {

        });
    });

    /******************************************************************************
                        Event Cant de la tabla - comenzar en el vídeo 63
    ******************************************************************************/
    $('#tblProducts tbody')
        .on('click', 'a[rel="remove"]', function () {
            let tr = tblProducts.cell($(this).closest('td, li')).index();   // Obtengo la fila / posición
            alert_action('Notificación', '¿Estás seguro de eliminar el ítem del listado?',
                function () {
                    vents.items.products.splice(tr.row, 1);
                    vents.list();
                }, function () {

                });
        })
        .on('change', 'input[name="cant"]', function () {
            console.clear();
            let cant = parseInt($(this).val());
            let tr = tblProducts.cell($(this).closest('td, li')).index();   // Obtengo la fila / posición
            vents.items.products[tr.row].cant = cant;
            vents.calculate_invoice();
            $('td:eq(4)', tblProducts.row(tr.row).node()).html('$' + vents.items.products[tr.row].subtotal.toFixed(2));
        });

    /******************************************************************************
                        CLEAR Select THIS PRODUCTS
    ******************************************************************************/
    $('.btnClearSearch').on('click', function () {
        // $('input[name="search"]').val('').focus();
        $('select[name="search"]').val('').focus();
    });

    /******************************************************************************
                        Modal Search Product con Select2
    ******************************************************************************/
    $('.btnSearchProducts').on('click', function () {
        tblSearchProducts = $('#tblSearchProducts').DataTable({
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla =(",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        },
        paging: true,
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname, // vídeo 104 - 743
            type: 'POST',
            data: {
                'action': 'search_products',
                'ids': JSON.stringify(vents.get_ids()),
                // 'term': $('input[name="search"]').val()
                'term': $('select[name="search"]').val()
            },
            dataSrc: "",
            headers: {
                'X-CSRFToken': csrftoken
            }
        },
        columns: [
            {"data": "name"},
            {"data": "image"},
            {"data": "stock"},
            {"data": "pvp"},
            {"data": "id"},
        ],
        columnDefs: [
            {
                targets: [-5],
                class: 'text-left',
                orderable: false,
            },
            {
                targets: [-4],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '<img src="'+data+'" class="img-fluid d-block mx-auto" style="width: 30px; height: 30px;">';
                }
            },
            {
                targets: [-3],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    if(row.stock > 0 && row.stock <= 5){
                        return '<span style="background-color:var(--structure_yellow); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.stock > 5){
                        return '<span style="background-color:var(--structure_blue4); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                    }
                    return '<span style="background-color:var(--structure_red); FONT-SIZE: 10pt" class="badge text-white">'+data+'</span>'
                }
            },
            {
                targets: [-2],
                class: 'text-left',
                orderable: false,
                render: function (data, type, row) {
                    return '$ '+parseFloat(data).toFixed(2);
                }
            },
            {
                // targets: [-7],
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return buttons = '<a rel="add" style="background-color:var(--structure_blue4); FONT-SIZE: 11pt" class="badge text-white"><i class="fa-solid fa-plus"></i></a>';
                }
            },

        ],
        initComplete: function (settings, json) {

        }
    });
        // Search Products
        $('#ModalSearchProducts').modal('show');
    });

    /******************************************************************************
      Con el evento change, el usuario al incrementar la cantidad se calcula el total de la fila del producto
    ******************************************************************************/
    $('#tblSearchProducts tbody')
        .on('click', 'a[rel="add"]', function () {
            let tr = tblSearchProducts.cell($(this).closest('td, li')).index();
            let product = tblSearchProducts.row(tr.row).data();
            // console.log(product);
            product.cant = 1;
            product.subtotal = 0.00;
            product.additional_cost = 0;
            product.discount = 0;
            vents.add(product);
            // Modal Search Products: Se elimina la fila cuando se presiona el botón de agregar un producto a la lista de ventas
            tblSearchProducts.rows($(this).parents('tr')).remove().draw();
        });

    /******************************************************************************
                        Event Submit - Sending date to BDs
    ******************************************************************************/
    $('#frmSale').on('submit', function (e) {
        e.preventDefault();
        if (vents.items.products.length === 0) {
            message_error('La factura debe tener al menos un ítem para realizar la venta.');
            return false;
        }
        //
        vents.items.date_joined = $('input[name="date_joined"]').val();
        vents.items.cli = $('select[name="cli"]').val();
        let parameters = new FormData();
        parameters.append('action', $('input[name="action"]').val());
        parameters.append('vents', JSON.stringify(vents.items));    // Transforma el diccionario del formulario en string
        submit_with_ajax(window.location.pathname, 'Notificación',
            '¿Estas seguro de realizar la siguiente acción?', parameters, function (response) {
                alert_action('Notificación', '¿Desea imprimir la factura de la venta?', function () {
                    window.open('/erp/sale/invoice/pdf/' + response.id + '/', '_blank');
                    location.href = '/erp/sale/list/';
                }, function () {
                    location.href = '/erp/sale/list/';
                });
            });
    });

    /******************************************************************************
                        SEARCH PRODUCTS - LIST WHITH Select2
    ******************************************************************************/
    $('select[name="search"]').select2({
        theme: "bootstrap4",
        language: 'es',
        allowClear: true,
        ajax: {
            delay: 250,
            type: 'POST',
            url: window.location.pathname,
            data: function (params) {
                let queryParameters = {
                    term: params.term,
                    action: 'search_autocomplete',
                    ids: JSON.stringify(vents.get_ids())
                }
                return queryParameters;
            },
            headers: {
                'X-CSRFToken': csrftoken
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
        },
        placeholder: 'Enter a description',
        minimumInputLength: 1,
        templateResult: formatRepo,
    }).on('select2:select', function (e) {
        let data = e.params.data;
        if(!Number.isInteger(data.id)){
            return false;
        }

        data.cant = 1;
        data.subtotal = 0.00;
        data.additional_cost = 0;
        data.discount = 0;
        vents.add(data);
        $(this).val('').trigger('change.select2'); // este limpia el input luego de haber seleccionado el objeto
    });

    // Esto se puso aqui para que funcione bien el editar y calcule bien los valores del iva. // sino tomaría el valor del iva de la base debe
    // coger el que pusimos al inicializarlo.
    vents.list();

});






