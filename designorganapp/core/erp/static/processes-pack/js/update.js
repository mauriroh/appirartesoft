let checkbox_flag = Boolean(false);
let checkbox_order = Boolean(false);
let checkbox_finish_date = Boolean(false);
let start_date;
let finish_date;

let proces = {
    items: {
        process_prod: '',
        observation:'',
        quantity:0,
        state:'',
        destiny:'',
        process_date_joined:'',
        process_date_finish: '',
        order_id:'',
        product_id:'',
        order_number:'',
        state_dressmaking:'',
    },

};
/******************************************************
                        FUNCIONES
*******************************************************/
$(function () {
    // BLOQUE DE VARIABLES
    $('select[name="process_prod"]').prop("disabled","disabled");
    $('input[name="start_date"]').prop("disabled","disabled");
    $('input[name="finish_date"]').prop("disabled","disabled");
    document.getElementById('order_numbers').value = $('input[name="order_number"]').val();
    document.getElementById('productos').value = $('select[name="process_prod"]').val();
    // INICIALIZACION DE VARIABLES
    $('#checkbox_finish_date').prop('checked', false);
    // OCULTAR VARIABLES
    document.getElementById('process_date_finish').style.display = 'none';
    document.getElementById('icon_date_finish').style.display = 'none';
    start_date = $('input[name="process_date_joined"]').val();
    finish_date = $('input[name="process_date_finish"]').val();

    // START DATE
    $('#process_date_finish').datetimepicker({
        format: 'YYYY-MM-DD',
        date: moment().format("YYYY-MM-DD"),
        locale: 'es',
        minDate: moment().format("YYYY-MM-DD")
    });

    // OCULTA O MUESTRA A LOS CONFECCIONISTAS
    $('#id_state').on('click', function (){
        if ($('select[name="state"]').val() === '5') {
            document.getElementById('lbl_state_dressmaking').style.display = 'block';
            document.getElementById('id_state_dressmaking').style.display = 'block';
        }else{
            document.getElementById('lbl_state_dressmaking').style.display = 'none';
            document.getElementById('id_state_dressmaking').style.display = 'none';
        }
    });

    // ACTIVA EL CHECK-BOX PARA FECHA DE CAMBIO
    $('.checkbox_finish_date').on('click', function () {
        checkbox_finish_date = document.getElementById('checkbox_finish_date').checked;

            if(checkbox_finish_date === true) {
                document.getElementById('process_date_finish').style.display = 'block';
                document.getElementById('icon_date_finish').style.display = 'block';
                checkbox_flag = true;
            }else{
                document.getElementById('process_date_finish').style.display = 'none';
                document.getElementById('icon_date_finish').style.display = 'none';
                checkbox_flag = false;
            }
    });

    // ENVIA LOS DATOS AL MODELO
    $('form').on('submit', function (e) {
        e.preventDefault();

        if (checkbox_flag === true){
            proces.items.process_prod = $('input[name="productos"]').val();
            proces.items.observation = $('textarea[name="observation"]').val();
            proces.items.quantity = $('input[name="quantity"]').val();
            proces.items.state = $('select[name="state"]').val();
            proces.items.destiny = $('input[name="destiny"]').val();
            proces.items.process_date_joined = start_date
            proces.items.process_date_finish = $('input[name="process_date_finish"]').val();
            proces.items.order_id = $('input[name="order_id"]').val();
            proces.items.product_id = $('input[name="product_id"]').val();
            proces.items.order_number = $('input[name="order_numbers"]').val();
            proces.items.state_dressmaking = $('select[name="state_dressmaking"]').val();
            proces.items.checkbox_order = document.getElementById('checkbox_order').checked;

        }else{
            proces.items.process_prod = $('input[name="productos"]').val();
            proces.items.observation = $('textarea[name="observation"]').val();
            proces.items.quantity = $('input[name="quantity"]').val();
            proces.items.state = $('select[name="state"]').val();
            proces.items.destiny = $('input[name="destiny"]').val();
            proces.items.process_date_joined = start_date
            proces.items.process_date_finish = finish_date
            proces.items.order_id = $('input[name="order_id"]').val();
            proces.items.product_id = $('input[name="product_id"]').val();
            proces.items.order_number = $('input[name="order_numbers"]').val();
            proces.items.state_dressmaking = $('select[name="state_dressmaking"]').val();
            proces.items.checkbox_order = document.getElementById('checkbox_order').checked;
        }

        var parameters = new FormData();
        parameters.append('action', $('input[name="action"]').val());
        parameters.append('proces', JSON.stringify(proces.items));

        submit_with_ajax(window.location.pathname, 'Notificación', '¿Estas seguro de realizar la siguiente acción?', parameters, function () {
            location.href = '/erp/processes-pack/list/';
        });

    });

});