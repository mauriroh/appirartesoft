$(function () {
    $('#data').DataTable({
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla =(",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        },
        scrollX: true,
        paging: true,
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 25,
        order: [[ 1, 'desc' ]],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: "",
            headers: {
                'X-CSRFToken': csrftoken
            }
        },
        columns: [
            {"data": "id"},
            {"data": "order_number"},
            {"data": "created_at"},
            {"data": "customer_name"},
            {"data": "customer_email"},
            {"data": "customer_identification"},
            {"data": "customer_phone"},
            {"data": "customer_address"},
            {"data": "customer_number"},
            {"data": "customer_floor"},
            {"data": "customer_locality"},
            {"data": "customer_city"},
            {"data": "customer_zipcode"},
            {"data": "customer_province"},
            {"data": "customer_country"},
            {"data": "product_name"},
            {"data": "product_price"},
            {"data": "product_quantity"},
            {"data": "paid_at"},
            {"data": "gateway"},
            {"data": "oder_status"},
            {"data": "payment_status"},
            {"data": "currency"},
            {"data": "subtotal"},
            {"data": "discount_coupon"},
            {"data": "discount"},
            {"data": "shipping_cost_customer"},
            {"data": "total"},
            {"data": "shipping_option"},
            {"data": "customer_note"},
            {"data": "owner_note"},
            {"data": "gateway_id"},
            {"data": "order_id"},
        ],
        columnDefs: [
            {
                targets: [-1, -2, -3, -4, -5, -9, -11, -12, -13, -14, -15, -16, -18, -19, -20, -21, -22, -23, -24, -25, -26, -27, -28, -29, -30, -31, -32],
                class: 'text-left',
            },
            {
                targets: [-6, -7, -8, -10, -17],
                class: 'text-left',
                orderable: false,
                render: function (data, type, row) {
                    return '$ ' + parseFloat(data).toFixed(2);
                }
            },

            {
                targets: [0],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return  buttons = '<a href="/erp/tiendanube/update/' + row.id + '/" style="background-color:var(--structure_blue4); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-pen-clip"></i></a>';
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});
