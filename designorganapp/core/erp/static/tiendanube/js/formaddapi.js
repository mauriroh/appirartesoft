let tblProducts;
let tblSearchProducts;

let vents = {
    items: {
        process_date_joined: '',
        discount: 0,
        additional_cost2: 0,
        subtotal: 0.0,
        iva: 0.0,
        total: 0.0,
        products: [],
    },

    get_ids: function () {
        let ids = [];
        $.each(this.items.products, function (key, value) {
            ids.push(value.id);
        });
        return ids;
    },

    /***************************************************************
                            Cálculos
    ****************************************************************/
    calculate_invoice: function (){  // vídeo 60 => 8:00
        let var_subtotal = 0.00;
        let var_iva = $('input[name="iva"]').val();
        $.each(this.items.products, function (pos, dict){
            dict.subtotal = dict.cant * parseFloat(dict.pvp);
            var_subtotal += dict.subtotal;
        });
        this.items.subtotal = var_subtotal;
        this.items.iva = this.items.subtotal * var_iva;
        this.items.total = this.items.subtotal - this.items.iva;

        $('input[name="subtotal"]').val(this.items.subtotal.toFixed(2));
        $('input[name="ivacalc"]').val(this.items.iva.toFixed(2));
        $('input[name="total"]').val(this.items.total.toFixed(2));
    },

    /******************************************************************************
     Lista los productos que son seleccionados en el BOX "Buscador de Productos" en la tabla
    ******************************************************************************/
    add: function (item) {
        this.items.products.push(item);
        this.list();
    },
    list: function () {
        this.calculate_invoice();
        tblProducts = $('#tblProducts').DataTable({
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla =(",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            },
            responsive: true,
            autoWidth: false,
            destroy: true,
            data: this.items.products,
            columns: [
                {"data": "id"},
                {"data": "order_number"},
                {"data": "name"},
                {"data": "pvp"},
                {"data": "cant"},
                {"data": "subtotal"},
            ],
            columnDefs: [
                {
                    targets: [0],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<a rel="remove" style="background-color:var(--structure_red); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-trash-can"></i></a>';
                    }
                },
                {
                    targets: [-1,-3],
                    class: 'text-left',
                    orderable: false,
                    render: function (data, type, row) {
                        return '$' + parseFloat(data).toFixed(2);
                    }
                },
                {
                    targets: [-2],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<input type="text" name="cant" class="form-control form-control-sm input-sm text-center" autocomplete="off" value="' + row.cant + '">';
                    }
                },
                {
                    targets: [-4, -5],
                    class: 'text-left',
                },
            ],
            /*Este registra los cambios que se editan dentro de la tabla.
              sobre la columna de cantidades */
            rowCallback(row, data, displayNum, displayIndex, dataIndex){
                $(row).find('input[name="cant"]').TouchSpin({
                    min: 1,
                    max: 1000000000,
                    step: 1,
                    buttondown_class: 'input-group-text',   // Alinea el botón baja al input
                    buttonup_class: 'input-group-text',
                });
            },
            initComplete: function (settings, json) {

            }
        });
    },
};

/******************************************************************************
                    FUNCTION SHOW IMAGE IN Select2
******************************************************************************/
function formatRepo(repo) {
    if (repo.loading) { // Este pedaso de código hace, cuando no hay nada en el select2, lo deja vacío, sino largaría un error
        return repo.text;
    }

    if (!Number.isInteger(repo.id)) {
        return repo.text;
    }

    let option = $(
        '<div class="wrapper container">' +
            '<div class="row">' +
                '<div class="col-lg-1">' +
                    '<img src="' + repo.image + '" class="img-fluid d-block mx-auto rounded" style="width: 60px; height: 40px;">' +
                '</div>' +
                '<div class="col-lg-11 text-left shadow-sm">' +
                //'<br>'
                    '<p style="margin-bottom: 0;">' +
                        '<b>Producto:</b> ' + repo.name + '<br>' +
                        '<b>Stock:</b> <span style="background-color:var(--search_product_stock); FONT-SIZE: 10pt" class="badge text-white">' + repo.stock + '</span><br>' +
                        '<b>Precio:</b> <span style="background-color:var(--search_product_price); FONT-SIZE: 10pt" class="badge text-white"> $ ' + repo.pvp + '</span>' +
                    '</p>' +
                '</div>' +
            '</div>' +
        '</div>');
    return option;
}

/******************************************************************************
                                    FUNCIONES
******************************************************************************/
$(function (){
    document.getElementById('order_numbers').value = '';
    document.getElementById('customer_names').value = '';
    document.getElementById('email').value = '';
    document.getElementById('dni_cuit').value = '';
    document.getElementById('phone').value ='';
    document.getElementById('address').value = '';
    document.getElementById('number').value = '';
    document.getElementById('flor').value = '';
    document.getElementById('locality').value = '';
    document.getElementById('city').value = '';
    document.getElementById('postal_code').value = '';
    document.getElementById('province').value = '';
    document.getElementById('country').value = '';
    document.getElementById('orders_id').value = '';
    document.getElementById('input_order_numbers').value = '';
    document.getElementById('hiddenSearchProducts').style.display='none';

    // Inicializa el Select en la página
    $('.select2').select2({
        theme: "bootstrap4",
        language: 'es'
    });

    // Calendar
    $('#process_date_joined').datetimepicker({
        format: 'YYYY-MM-DD',
        date: moment().format("YYYY-MM-DD"),
        locale: 'es',
        minDate: moment().format("YYYY-MM-DD")
    });

    // Touchspin: Descuento en %
    $("input[name='iva']").TouchSpin({
        min: 0,
        max: 100,
        step: 0.01, // Incrementador de 0.01 en 0.01
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%',
        buttondown_class: 'input-group-text',   // Alinea el botón baja al input
        buttonup_class: 'input-group-text',     // Alinea el botón subir input
    }).on('change', function () {
        vents.calculate_invoice();
    })
        .val(0.00);


    // Touchspin: Descuento por número
    $("input[name='discount']").TouchSpin({
        min: 0,
        max: 100000000,
        step: 1, // Incrementador de 0.01 en 0.01
        // decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        prefix: '$',
        buttondown_class: 'input-group-text',   // Alinea el botón baja al input
        buttonup_class: 'input-group-text',     // Alinea el botón subir input
    }).on('change keyup', function () {
        vents.calculate_invoice();
    })
        .val(0);

    // Touchspin - Costo Adicional
    $("input[name='additional_cost']").TouchSpin({
        min: 0,
        max: 100000000,
        step: 1, // Incrementador de 0.01 en 0.01
        // decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        prefix: '$',
        buttondown_class: 'input-group-text',   // Alinea el botón baja al input
        buttonup_class: 'input-group-text',     // Alinea el botón subir input
    }).on('change keyup', function () {
        vents.calculate_invoice();
    })
        .val(0);


    /////////////////******************************//////////////////////////////
    // Button Limpiar Tabla de Productos
    $('.btnRemoveAlltn').on('click', function () {
        if (vents.items.products.length === 0) return false;
        alert_action('Notificación', '¿Estás seguro de eliminar todos los items del listado?', function () {
            vents.items.products = [];
            vents.list();
        }, function () {

        });
    });

    // Event Cant de la tabla - comenzar en el vídeo 63
    $('#tblProducts tbody')
        .on('click', 'a[rel="remove"]', function () {
            let tr = tblProducts.cell($(this).closest('td, li')).index();   // Obtengo la fila / posición
            alert_action('Notificación', '¿Estás seguro de eliminar el ítem del listado?',
                function () {
                    vents.items.products.splice(tr.row, 1);
                    vents.list();
                }, function () {

                });
        })
        .on('change', 'input[name="cant"]', function () {
            console.clear();
            let cant = parseInt($(this).val());
            let tr = tblProducts.cell($(this).closest('td, li')).index();   // Obtengo la fila / posición
            vents.items.products[tr.row].cant = cant;        // Obtengo el objeto completo de la fila
            vents.calculate_invoice();
            $('td:eq(5)', tblProducts.row(tr.row).node()).html('$' + vents.items.products[tr.row].subtotal.toFixed(2));
        });


    // Limpia el buscador de Número de Órdenes
    $('.btnClearSearchNumberOrden').on('click', function () {
        $('input[name="search_order_number"]').val('').focus();
    });

    // CLEAR Select THIS PRODUCTS
    $('.btnClearSearch').on('click', function () {
        // $('input[name="search"]').val('').focus();
        $('select[name="search"]').val('').focus();
    });

    // Modal Search Product con Select2
    $('.btnSearchProducts').on('click', function () {
        tblSearchProducts = $('#tblSearchProducts').DataTable({
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla =(",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        },
        // scrollY: 400,
        // scrollX: true,
        paging: true,
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'search_products',
                'ids': JSON.stringify(vents.get_ids()),
                'term': $('select[name="search"]').val()
            },
            dataSrc: "",
            headers: {
                'X-CSRFToken': csrftoken
            }
        },
        columns: [
            {"data": "name"},
            {"data": "image"},
            {"data": "stock"},
            {"data": "pvp"},
            {"data": "id"},
        ],
        columnDefs: [
            {
                targets: [-5],
                class: 'text-left',
                orderable: false,
            },
            {
                targets: [-4],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '<img src="'+data+'" class="img-fluid d-block mx-auto" style="width: 30px; height: 30px;">';
                }
            },
            {
                targets: [-3],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    if(row.stock > 0 && row.stock <= 5){
                        return '<span style="background-color:var(--structure_yellow); FONT-SIZE: 12pt" class="badge text-white">'+data+'</span>'
                    }
                    else if(row.stock > 5){
                        return '<span style="background-color:var(--structure_blue4); FONT-SIZE: 12pt" class="badge text-white">'+data+'</span>'
                    }
                    return '<span style="background-color:var(--structure_red); FONT-SIZE: 12pt" class="badge text-white">'+data+'</span>'
                }
            },
            {
                targets: [-2],
                class: 'text-left',
                orderable: false,
                render: function (data, type, row) {
                    return '$'+parseFloat(data).toFixed(2);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return buttons = '<a rel="add" style="background-color:var(--structure_blue4); FONT-SIZE: 12pt" class="badge text-white"><i class="fa-solid fa-plus"></i></a>';
                    // return buttons;
                }
            },

        ],
        initComplete: function (settings, json) {

        }
    });
        // Search Products
        $('#ModalSearchProductsTiendanube1').modal('show');
    });

    // Con el evento change, el usuario al incrementar la cantidad se calcula el total de la fila del producto
    $('#tblSearchProducts tbody')
        .on('click', 'a[rel="add"]', function () {
            let tr = tblSearchProducts.cell($(this).closest('td, li')).index();
            let product = tblSearchProducts.row(tr.row).data();
            product.order_number = document.getElementById('order_numbers').value;
            product.customer_name = document.getElementById('customer_names').value;
            product.customer_email = document.getElementById('email').value;
            product.customer_identification = document.getElementById('dni_cuit').value;
            product.customer_phone = document.getElementById('phone').value;
            product.customer_address = document.getElementById('address').value;
            product.customer_number = document.getElementById('number').value;
            product.customer_floor = document.getElementById('flor').value;
            product.customer_locality = document.getElementById('locality').value;
            product.customer_city = document.getElementById('city').value;
            product.customer_zipcode = document.getElementById('postal_code').value;
            product.customer_province = document.getElementById('province').value;
            product.customer_country = document.getElementById('country').value;
            product.order_id = document.getElementById('orders_id').value;
            product.cant = 1;
            product.subtotal = 0.0;
            product.additional_cost = 0;
            product.discount = 0;
            console.log(vents.items);
            vents.add(product);
            // Modal Search Products: Se elimina la fila cuando se presiona el botón de agregar un producto a la lista de ventas
            tblSearchProducts.rows($(this).parents('tr')).remove().draw();
        });


    // Event Submit - Sending date to BDs
    $('form').on('submit', function (e) {
        e.preventDefault();
        if (vents.items.products.length === 0) {
            message_error('La factura debe tener al menos un ítem para realizar la venta.');
            return false;
        }
        //
        vents.items.date_joined = $('input[name="process_date_joined"]').val();
        let parameters = new FormData(this);
        parameters.append('action', $('input[name="action"]').val());
        parameters.append('vents', JSON.stringify(vents.items));
        submit_with_ajax(window.location.pathname, 'Notificación', '¿Estas seguro de realizar la siguiente acción?', parameters, function () {
            Swal.fire({
                title: 'Alerta',
                text: 'Acción Exitosa',
                icon: 'success',
                timer: 2000,
                onClose: () => {
                    location.href = '/erp/tiendanube/list/';
                }
            }).then((result) => {

            });
        });
    });


    //////////////////////////////////////////////
    // Search ORDER NUMBER - List whith Select2
    //////////////////////////////////////////////
    $('select[name="search_order_number"]').select2({
        theme: "bootstrap4",
        language: 'es',
        allowClear: true,
        ajax: {
            delay: 250,
            type: 'POST',
            url: window.location.pathname,
            data: function (params) {
                let queryParameters = {
                    term: params.term,
                    action: 'search_order_numbers'
                }
                return queryParameters;
            },
            headers: {
                'X-CSRFToken': csrftoken
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
        },

        placeholder: 'Enter Order Number', // vídeo 67
        minimumInputLength: 1,
        // templateResult: formatRepo,
   }).on('select2:select', function (e) {
        let data = e.params.data;
        document.getElementById('hiddenSearchProducts').style.display='';
        document.getElementById('order_numbers').value = data.order_number;
        document.getElementById('input_order_numbers').value = data.order_number;
        document.getElementById('input_order_numbers').value = data.order_number;
        // Asigno el número de orden al Label
        document.querySelector('#lblOrderNumber').textContent = "Número de Orden: " + data.order_number;
        // order_number = ui.item.order_number;
        document.getElementById('customer_names').value = data.customer_name;
        // customer_name = ui.item.customer_name;
        document.getElementById('email').value = data.customer_email;
        // customer_email = ui.item.customer_email;
        document.getElementById('dni_cuit').value = data.customer_identification;
        // customer_identification = ui.item.customer_identification;
        document.getElementById('phone').value = data.customer_phone;
        // customer_phone = ui.item.customer_phone;
        document.getElementById('address').value = data.customer_address;
        // customer_address = ui.item.customer_address;
        document.getElementById('number').value = data.customer_number;
        // customer_number = ui.item.customer_number;
        document.getElementById('flor').value = data.customer_floor;
        // customer_floor = ui.item.customer_floor;
        document.getElementById('locality').value = data.customer_locality;
        // customer_locality = ui.item.customer_locality;
        document.getElementById('city').value = data.customer_city;
        // customer_city = ui.item.customer_city;
        document.getElementById('postal_code').value = data.customer_zipcode;
        // customer_zipcode = ui.item.customer_zipcode;
        document.getElementById('province').value = data.customer_province;
        // customer_province = ui.item.customer_province;
        document.getElementById('country').value = data.customer_country;
        // customer_country = ui.item.customer_country;
        document.getElementById('orders_id').value = data.order_id;

        $(this).val('').trigger('change.select2'); // este limpia el input luego de haber seleccionado el objeto
    });

   // SEARCH PRODUCTS - LIST WHITH Select2
    $('select[name="search"]').select2({
        theme: "bootstrap4",
        language: 'es',
        allowClear: true,
        ajax: {
            delay: 250,
            type: 'POST',
            url: window.location.pathname,
            data: function (params) {
                let queryParameters = {
                    term: params.term,
                    action: 'search_autocomplete',
                    ids: JSON.stringify(vents.get_ids())
                }
                return queryParameters;
            },
            headers: {
                'X-CSRFToken': csrftoken
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
        },
        placeholder: 'Enter a description',
        minimumInputLength: 1,
        templateResult: formatRepo,
    }).on('select2:select', function (e) {
        let data = e.params.data;
        if(!Number.isInteger(data.id)){
            return false;
        }

        console.log(data);
        data.order_number = document.getElementById('order_numbers').value;
        data.customer_name = document.getElementById('customer_names').value;
        data.customer_email = document.getElementById('email').value;
        data.customer_identification = document.getElementById('dni_cuit').value;
        data.customer_phone = document.getElementById('phone').value;
        data.customer_address = document.getElementById('address').value;
        data.customer_number = document.getElementById('number').value;
        data.customer_floor = document.getElementById('flor').value;
        data.customer_locality = document.getElementById('locality').value;
        data.customer_city = document.getElementById('city').value;
        data.customer_zipcode = document.getElementById('postal_code').value;
        data.customer_province = document.getElementById('province').value;
        data.customer_country = document.getElementById('country').value;
        data.order_id = document.getElementById('orders_id').value;
        data.cant = 1;
        data.subtotal = 0.0;
        data.additional_cost = 0;
        data.discount = 0;
        console.log(vents.items);
        vents.add(data);
        $(this).val('').trigger('change.select2'); // este limpia el input luego de haber seleccionado el objeto
    });

    // Esto se puso aqui para que funcione bien el editar y calcule bien los valores del iva. // sino tomaría el valor del iva de la base debe
    // coger el que pusimos al inicializarlo.
    vents.list();


     // // Search Order Numbers Autocomplet
    // $('input[name="search_order_number"]').autocomplete({
    //     source: function (request, response) {
    //         $.ajax({
    //             url: window.location.pathname,
    //             type: 'POST',
    //             data: {
    //                 'action': 'search_order_numbers',
    //                 'term': request.term
    //             },
    //             dataType: 'json',
    //         }).done(function (data) {
    //             response(data);
    //         }).fail(function (jqXHR, textStatus, errorThrown) {
    //             //alert(textStatus + ': ' + errorThrown);
    //         }).always(function (data) {
    //
    //         });
    //     },
    //     delay: 500,
    //     minLength: 1,
    //     //video 59 => 8:00
    //     select: function (event, ui) {
    //         event.preventDefault();
    //         console.clear();
    //         // vents.items.order_number_array.push(ui.item);
    //         // console.log(ui.item.order_number);
    //         // console.log(ui.item.customer_name);
    //         document.getElementById('order_numbers').value = ui.item.order_number;
    //         document.getElementById('input_order_numbers').value = ui.item.order_number;
    //         // order_number = ui.item.order_number;
    //         document.getElementById('customer_names').value = ui.item.customer_name;
    //         // customer_name = ui.item.customer_name;
    //         document.getElementById('email').value = ui.item.customer_email;
    //         // customer_email = ui.item.customer_email;
    //         document.getElementById('dni_cuit').value = ui.item.customer_identification;
    //         // customer_identification = ui.item.customer_identification;
    //         document.getElementById('phone').value = ui.item.customer_phone;
    //         // customer_phone = ui.item.customer_phone;
    //         document.getElementById('address').value = ui.item.customer_address;
    //         // customer_address = ui.item.customer_address;
    //         document.getElementById('number').value = ui.item.customer_number;
    //         // customer_number = ui.item.customer_number;
    //         document.getElementById('flor').value = ui.item.customer_floor;
    //         // customer_floor = ui.item.customer_floor;
    //         document.getElementById('locality').value = ui.item.customer_locality;
    //         // customer_locality = ui.item.customer_locality;
    //         document.getElementById('city').value = ui.item.customer_city;
    //         // customer_city = ui.item.customer_city;
    //         document.getElementById('postal_code').value = ui.item.customer_zipcode;
    //         // customer_zipcode = ui.item.customer_zipcode;
    //         document.getElementById('province').value = ui.item.customer_province;
    //         // customer_province = ui.item.customer_province;
    //         document.getElementById('country').value = ui.item.customer_country;
    //         // customer_country = ui.item.customer_country;
    //         document.getElementById('orders_id').value = ui.item.order_id;
    //         // order_id = ui.item.order_id;
    //         // vents.add(ui.item);
    //         $(this).val('');
    //         // or_numb = vents.items.order_number_array[order_number];
    //         // console.log(or_numb);
    //         // console.log('Estoy en el AJAX N° ORDEN *2')
    //         // order_number:'',
    //         // customer_name: '',
    //         // customer_email: '',
    //         // customer_identification: '',
    //         // customer_phone: '',
    //         // customer_address: '',
    //         // customer_number: '',
    //         // customer_floor: '',
    //         // customer_locality: '',
    //         // customer_city: '',
    //         // customer_zipcode: '',
    //         // customer_province: '',
    //         // customer_country: '',
    //         // order_id: '',
    //         // order_number_array: [],
    //
    //         // ui.item.order_number = 101010;
    //         // ui.item.cant = 1;
    //         // ui.item.subtotal = 0.0;
    //         // // ui.items.additional_cost = 0.00;
    //         // // ui.item.discount = 0.00;
    //         // console.log(vents.items);
    //         // vents.add(ui.item);
    //         // // vents.items.products.push(ui.item);
    //         // // vents.list();
    //         // $(this).val('');
    //
    //         // vents.items.order_number_array.push(ui.item);
    //         // console.log(vents.items);
    //         //
    //         // console.log(vents.items.order_number_array.order_number);
    //         // $('input[name="order_number"]').val(vents.items.order_number_array.order_number);
    //
    //     }
    // });



        // // Search Products
    // $('input[name="searchtnaddapi"]').autocomplete({
    //     source: function (request, response) {
    //         $.ajax({
    //             url: window.location.pathname,
    //             type: 'POST',
    //             data: {
    //                 'action': 'search_products',
    //                 'term': request.term
    //             },
    //             dataType: 'json',
    //         }).done(function (data) {
    //             response(data);
    //         }).fail(function (jqXHR, textStatus, errorThrown) {
    //             //alert(textStatus + ': ' + errorThrown);
    //         }).always(function (data) {
    //
    //         });
    //     },
    //     delay: 500,
    //     minLength: 1,
    //     select: function (event, ui) {
    //         event.preventDefault();
    //         console.clear();
    //         ui.item.order_number = document.getElementById('order_numbers').value;
    //         ui.item.customer_name = document.getElementById('customer_names').value;
    //         ui.item.customer_email = document.getElementById('email').value;
    //         ui.item.customer_identification = document.getElementById('dni_cuit').value;
    //         ui.item.customer_phone = document.getElementById('phone').value;
    //         ui.item.customer_address = document.getElementById('address').value;
    //         ui.item.customer_number = document.getElementById('number').value;
    //         ui.item.customer_floor = document.getElementById('flor').value;
    //         ui.item.customer_locality = document.getElementById('locality').value;
    //         ui.item.customer_city = document.getElementById('city').value;
    //         ui.item.customer_zipcode = document.getElementById('postal_code').value;
    //         ui.item.customer_province = document.getElementById('province').value;
    //         ui.item.customer_country = document.getElementById('country').value;
    //         ui.item.order_id = document.getElementById('orders_id').value;
    //         // order_number = ui.item.order_number;
    //         // customer_name = ui.item.customer_name;
    //         // customer_email = ui.item.customer_email;
    //         // customer_identification = ui.item.customer_identification;
    //         // customer_phone = ui.item.customer_phone;
    //         // customer_address = ui.item.customer_address;
    //         // customer_number = ui.item.customer_number;
    //         // customer_floor = ui.item.customer_floor;
    //         // customer_locality = ui.item.customer_locality;
    //         // customer_city = ui.item.customer_city;
    //         // customer_zipcode = ui.item.customer_zipcode;
    //         // customer_province = ui.item.customer_province;
    //         // customer_country = ui.item.customer_country;
    //         // order_id = ui.item.order_id;
    //         ui.item.cant = 1;
    //         ui.item.subtotal = 0.0;
    //         ui.item.additional_cost = 0;
    //         ui.item.discount = 0;
    //         console.log(vents.items);
    //         vents.add(ui.item);
    //         // vents.items.products.push(ui.item);
    //         // vents.list();
    //         $(this).val('');
    //     }
    // });
});