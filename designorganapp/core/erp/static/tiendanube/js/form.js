// Carga los datos del archivo EXCEL XLSX en el HTML
let ExcelToJSON = function () {
  this.parseExcel = function (file) {
    let reader = new FileReader();

    reader.onload = function (e) {
      let data = e.target.result;
      let workbook = XLSX.read(data, {
        type: "binary",
      });
      workbook.SheetNames.forEach(function (sheetName) {
        // Here is your object
        let XL_row_object = XLSX.utils.sheet_to_row_object_array(
            workbook.Sheets[sheetName]
        );
        let json_object = JSON.stringify(XL_row_object);

        jQuery("#xlx_json").val(json_object);
        data = XL_row_object;

          $.ajax({
            url: window.location.pathname,
            type: "POST",
            headers: {
              'Authorization': "Token " + localStorage.access_token
            },
            data: data,

            }).done(function (data) {
                console.log(data);
                if (!data.hasOwnProperty('error')) {
                    location.href = '{{ list_url }}';
                    return false;
                }
                message_error(data.error);
            }).fail(function (jqXHR, textStatus, errorThrown) {

            }).always(function (data) {

            });
          /////////////////////////  Envio de Datos del Form a la BD  ////////////////////////
          let sendData = document.getElementById("enviar");
          sendData.click();
          /////////////////////////  END Envio de Datos del Form a la BD  ////////////////////////
        /////////////////////////  END del For  ////////////////////////
      });
    };

    reader.onerror = function (ex) {
      console.log(ex);
    };

    reader.readAsBinaryString(file);
  };
};

function handleFileSelect(evt) {
  let files = evt.target.files; // FileList object
  let xl2json = new ExcelToJSON();
  xl2json.parseExcel(files[0]);
}

function upLoadFile() {
  document
    .getElementById("upload")
    .addEventListener("change", handleFileSelect, false);
    console.log('function upLoadFile')

}
