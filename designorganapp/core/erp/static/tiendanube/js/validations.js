/******************************************************************************
                        FUNCIONES - VALIDACION DE CAMPOS CLIENTE
******************************************************************************/
/* Nombre y Apellido */
$('input[name="name"]').bind('keypress', function(event) {
    alert('Entro a la validación de name');
    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

 /* Email */
$('input[name="email"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9üÜ_@.]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* DNI */
$('input[name="identification"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Teléfono */
$('input[name="phone"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9+]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Dirección */
$('input[name="address"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-Z0-9áéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Número del domicilio */
$('input[name="number"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Piso de Departamento */
$('input[name="floor"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñ0-9-° ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Localidad */
$('input[name="locality"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ0-9 ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Código Postal */
$('input[name="postal_code"]').bind('keypress', function(event) {

    let regex = new RegExp("[A-Z0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Nombre y Apellido */
$('input[name="state_type"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});