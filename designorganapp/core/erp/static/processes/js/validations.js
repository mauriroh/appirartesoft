/******************************************************************************
                        FUNCIONES - VALIDACION DE CAMPOS PROCESOS
******************************************************************************/
/* Cantidad */
$('input[name="quantity"]').bind('keypress', function(event) {

    let regex = new RegExp("[0-9]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

/* Tipo de Tienda */
$('input[name="destiny"]').bind('keypress', function(event) {

    let regex = new RegExp("[a-zA-ZáéíóúÁÉÍÓÚÑñüÜ0-9 ]");
    let key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});
