let proces = {
    items: {
        process_prod: '',
        product_id:'',
        observation:'',
        quantity:0,
        state:'',
        destiny:'',
        process_date_joined: '',
        process_date_finish: '',
        state_dressmaking:1,
    },
};

// FUNCTION SHOW IMAGE IN Select2
function formatRepo(repo) {
    if (repo.loading) { // Este pedaso de código hace, cuando no hay nada en el select2, lo deja vacío, sino largaría un error
        return repo.text;
    }

    if (!Number.isInteger(repo.id)) {
        return repo.text;
    }

    let option = $(
        '<div class="wrapper container">' +
            '<div class="row">' +
                '<div class="col-lg-1">' +
                    '<img src="' + repo.image + '" class="img-fluid d-block mx-auto rounded" style="width: 60px; height: 40px;">' +
                '</div>' +
                '<div class="col-lg-11 text-left shadow-sm">' +
                    '<p style="margin-bottom: 0;">' +
                        '<b>Producto:</b> ' + repo.name + '<br>' +
                        '<b>Stock:</b> <span style="background-color:var(--search_product_stock); FONT-SIZE: 10pt" class="badge text-white">' + repo.stock + '</span><br>' +
                        '<b>Precio:</b> <span style="background-color:var(--search_product_price); FONT-SIZE: 10pt" class="badge text-white"> $' + repo.pvp + '</span>' +
                    '</p>' +
                '</div>' +
            '</div>' +
        '</div>');
    return option;
}

/////////////////////// FUNCTIONS ////////////////////////////
$(function () {
    document.getElementById('process_prod').value = '';
    document.getElementById('id_state_dressmaking').value = 1;
    // document.getElementById('product_id').value = '';

    // Inicializa el Select en la página
    $('.select2').select2({
        theme: "bootstrap4",
        language: 'es'
    });

    $('#process_date_joined').datetimepicker({
        format: 'YYYY-MM-DD',
        date: moment().format("YYYY-MM-DD"),
        locale: 'es',
        maxDate: moment().format("YYYY-MM-DD"),
        minDate: moment().format("YYYY-MM-DD")
    });

    $('#process_date_finish').datetimepicker({
        format: 'YYYY-MM-DD',
        date: moment().format("YYYY-MM-DD"),
        locale: 'es',
        minDate: moment().format("YYYY-MM-DD")
    });

    // OCULTA O MUESTRA A LOS CONFECCIONISTAS
    $('#select2-id_state-container').on('click', function (){
        if ($('select[name="state"]').val() === '5') {
            console.log('Vale 5')
            document.getElementById('lbl_state_dressmaking').style.display = 'block';
            document.getElementById('id_state_dressmaking').style.display = 'block';
        }else{
            console.log('Vale otros')
            document.getElementById('lbl_state_dressmaking').style.display = 'none';
            document.getElementById('id_state_dressmaking').style.display = 'none';
        }
    });

     // SEARCH PRODUCTS
    $('select[name="products"]').select2({
        theme: "bootstrap4",
        language: 'es',
        allowClear: true,
        ajax: {
            delay: 250,
            type: 'POST',
            url: window.location.pathname,
            data: function (params) {
                let queryParameters = {
                    term: params.term,
                    action: 'search_products'
                }
                return queryParameters;
            },
            headers: {
                'X-CSRFToken': csrftoken
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
        },
        placeholder: 'Enter Description', // vídeo 67
        minimumInputLength: 1,
        templateResult: formatRepo,
   }).on('select2:select', function (e) {
        let data = e.params.data;

        document.getElementById('process_prod').value = data.name;
        document.getElementById('product_id').value = data.id;

        $(this).val('').trigger('change.select2'); // este limpia el input luego de haber seleccionado el objeto
    });
    // SUBMIT DATA ViewDjango
    $('form').on('submit', function (e) {
        e.preventDefault();

        proces.items.process_prod = $('input[name="process_prod"]').val();
        proces.items.product_id = $('input[name="product_id"]').val();
        proces.items.observation = $('textarea[name="observation"]').val();
        proces.items.quantity = $('input[name="quantity"]').val();
        proces.items.state = $('select[name="state"]').val();
        proces.items.destiny = $('input[name="destiny"]').val();
        proces.items.process_date_joined = $('input[name="process_date_joined"]').val();
        proces.items.process_date_finish = $('input[name="process_date_finish"]').val();
        proces.items.state_dressmaking = $('select[name="state_dressmaking"]').val();

        let parameters = new FormData();
        parameters.append('action', $('input[name="action"]').val());
        parameters.append('proces', JSON.stringify(proces.items));

        submit_with_ajax(window.location.pathname, 'Notificación', '¿Estas seguro de realizar la siguiente acción?', parameters, function (response) {
            location.href = '/erp/processes/list/';
        });
    });
});