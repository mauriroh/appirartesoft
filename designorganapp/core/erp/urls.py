from django.urls import path

from core.erp.views.client.views import *

from core.erp.views.dashboard.views import *
from core.erp.views.department.views import *
from core.erp.views.processes.views import *
from core.erp.views.product.views import *
from core.erp.views.province.views import *
from core.erp.views.sale.views import *
from core.erp.views.states.views import *
from core.erp.views.tiendanube.views import *
from core.erp.views.processesdesign.views import *
from core.erp.views.processeslayout.views import *
from core.erp.views.processesprint.views import *
from core.erp.views.processesironing.views import *
from core.erp.views.processesmaking.views import *
from core.erp.views.processespack.views import *
from core.erp.views.processespending.views import *
from core.erp.views.services.views import *
from core.erp.views.processesdispatch.views import *
from core.erp.views.graphic.views import *
from core.erp.views.contact.views import *

app_name = 'productos_app'

urlpatterns = (

    # STATES
    path('states/list/', StatesListView.as_view(), name='states-list'),
    path('states/create/', StatesCreateView.as_view(), name='states-create'),
    path('states/update/<int:pk>/', StatesUpdateView.as_view(), name='states-update'),

    # DEPARTMENTS
    path('department/list/', DepartmentListView.as_view(), name='department-list'),
    path('department/create/', DepartmentCreateView.as_view(), name='department-create'),
    path('department/update/<int:pk>/', DepartmentUpdateView.as_view(), name='department-update'),

    # PROVINCES
    path('province/list/', ProvinceListView.as_view(), name='province-list'),
    path('province/create/', ProvinceCreateView.as_view(), name='province-create'),
    path('province/update/<int:pk>/', ProvinceUpdateView.as_view(), name='province-update'),

    # CLIENTS
    path('client/list/', ClientsListView.as_view(), name='client-list'),
    path('client/create/', ClientsCreateView.as_view(), name='client-create'),
    path('client/update/<int:pk>/', ClientsUpdateView.as_view(), name='client-update'),
    path('client/delete/<int:pk>/', ClientsDeleteView.as_view(), name='client-delete'),

    # PRODUCTS
    path('product/list/', ProductListView.as_view(), name='product-list'),
    path('product/create/', ProductCreateView.as_view(), name='product-create'),
    path('product/update/<int:pk>/', ProductUpdateView.as_view(), name='product-update'),
    path('product/delete/<int:pk>/', ProductDeleteView.as_view(), name='product-delete'),

    # SALE
    path('sale/list/', SaleListView.as_view(), name='sale-list'),
    path('sale/create/', SaleCreateView.as_view(), name='sale-create'),
    path('sale/update/<int:pk>/', SaleUpdateView.as_view(), name='sale-update'),
    path('sale/delete/<int:pk>/', SaleDeleteView.as_view(), name='sale-delete'),
    path('sale/invoice/pdf/<int:pk>/', SaleInvoicePdfView.as_view(), name='sale-invoice-pdf'),

    # PROCESSES
    path('processes/list/', ProcessesListView.as_view(), name='processes-list'),
    path('processes/create/', ProcessesCreateView.as_view(), name='processes-create'),
    path('processes/update/<int:pk>/', ProcessesUpdateView.as_view(), name='processes-update'),
    path('processes/detail/<int:pk>/', ProcessesDetailView.as_view(), name='processes-detail'),
    # path('processes/delete/<int:pk>/', ProcessesDeleteView.as_view(), name='processes-delete'),

    # 01 - DESIGN
    path('processes-design/list/', DesignListView.as_view(), name='processes-design-list'),
    path('processes-design/update/<int:pk>/', DesignUpdateView.as_view(), name='processes-design-update'),
    path('processes-design/detail/<int:pk>/', DesignDetailView.as_view(), name='processes-design-detail'),

    # 02 - LAYOUT
    path('processes-layout/list/', LayoutListView.as_view(), name='processes-layout-list'),
    path('processes-layout/update/<int:pk>/', LayoutUpdateView.as_view(), name='processes-layout-update'),
    path('processes-layout/detail/<int:pk>/', LayoutDetailView.as_view(), name='processes-layout-detail'),

    # 03 - PRINT
    path('processes-print/list/', PrintListView.as_view(), name='processes-print-list'),
    path('processes-print/update/<int:pk>/', PrintUpdateView.as_view(), name='processes-print-update'),
    path('processes-print/detail/<int:pk>/', PrintDetailView.as_view(), name='processes-print-detail'),

    # 04 - IRONING
    path('processes-ironing/list/', IroningListView.as_view(), name='processes-ironing-list'),
    path('processes-ironing/update/<int:pk>/', IroningUpdateView.as_view(), name='processes-ironing-update'),
    path('processes-ironing/detail/<int:pk>/', IroningDetailView.as_view(), name='processes-ironing-detail'),

    # 05 - MAKING
    path('processes-making/list/', MakingListView.as_view(), name='processes-making-list'),
    path('processes-making/update/<int:pk>/', MakingUpdateView.as_view(), name='processes-making-update'),
    path('processes-making/detail/<int:pk>/', MakingDetailView.as_view(), name='processes-making-detail'),

    # 06 - PACK
    path('processes-pack/list/', PackListView.as_view(), name='processes-pack-list'),
    path('processes-pack/update/<int:pk>/', PackUpdateView.as_view(), name='processes-pack-update'),
    path('processes-pack/detail/<int:pk>/', PackDetailView.as_view(), name='processes-pack-detail'),

    # 07 - DISPATCH
    path('processes-dispatch/list/', DispatchListView.as_view(), name='processes-dispatch-list'),
    path('processes-dispatch/update/<int:pk>/', DispatchUpdateView.as_view(), name='processes-dispatch-update'),
    path('processes-dispatch/detail/<int:pk>/', DispatchDetailView.as_view(), name='processes-dispatch-detail'),

    # 10 - PENDING
    path('processes-pending/list/', PendingListView.as_view(), name='processes-pending-list'),
    path('processes-pending/update/<int:pk>/', PendingUpdateView.as_view(), name='processes-pending-update'),
    path('processes-pending/detail/<int:pk>/', PendingDetailView.as_view(), name='processes-pending-detail'),
    # path('processes-pending/delete/<int:pk>/', PendingDeleteView.as_view(), name='processes-pending-delete'),

    # TIENDANUBE
    path('tiendanube/list/', TiendanubeListView.as_view(), name='tiendanube-list'),
    path('tiendanube/createapi/', TiendanubeApiCreateView.as_view(), name='tiendanube-createapi'),
    path('tiendanube/createapiadd/', SaleTiendanubeApiCreateView.as_view(), name='tiendanube-createapiadd'),
    path('tiendanube/update/<int:pk>/', TiendanubeUpdateView.as_view(), name='tiendanube-update'),
    path('tiendanube/delete/<int:pk>/', TiendanubeDeleteView.as_view(), name='tiendanube-delete'),

    # SERVICES
    path('services/list/', ServicesListView.as_view(), name='services-list'),
    path('services/create/', ServicesCreateView.as_view(), name='services-create'),
    path('services/update/<int:pk>/', ServicesUpdateView.as_view(), name='services-update'),
    path('services/delete/<int:pk>/', ServicesDeleteView.as_view(), name='services-delete'),

    # CONTACT
    path('contact/create/', ContactCreateView.as_view(), name='contact-create'),

    # GRAPHIC
    path('graphic/detailsalesall/', GraphicDetailSalesAllView.as_view(), name='graphic-detail-sales'),
    path('graphic/tiendanubeonesales/', GraphicTiendanubeOneSalesVie.as_view(), name='graphic-tiendanubeone-sales'),


    # PANEL DASHBOARD
    path('dashboard/', DashboardView.as_view(), name='dashboard'),
)
