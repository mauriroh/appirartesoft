from datetime import datetime

from django.db import models
from django.forms import model_to_dict

from config.settings import MEDIA_URL, STATIC_URL
# from core.erp.choices import gender_choices


class Estados(models.Model):
    state_type = models.CharField(max_length=20, null=False, blank=False, unique=True, verbose_name='Estado')

    def __str__(self):
        return self.state_type

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'
        db_table = 'states'
        ordering = ['id']


# DEPARTAMENTO | CIUDAD
class Department(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False, unique=True, verbose_name='Ciudad / Departamento')

    # objects = DepartmentManager()

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'
        db_table = 'department'
        ordering = ['name']


# PROVINCIA
class Province(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False, unique=True, verbose_name='Provincia / Estado')

    # objects = ProvinceManager()

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Provincia'
        verbose_name_plural = 'Provincias'
        db_table = 'province'
        ordering = ['name']


# NOMBRE DE CONFECCIONISTAS
class Dressmaking(models.Model):
    name = models.CharField(max_length=20, null=False, blank=False, unique=True, verbose_name='Nombre Confeccionista')
    def __str__(self):
        return self.name
    def toJSON(self):
        item = model_to_dict(self)
        return item
    class Meta:
        verbose_name = 'Confeccionista'
        verbose_name_plural = 'Confeccionistas'
        db_table = 'dressmaking'
        ordering = ['id']


# CLIENTES
class Client(models.Model):
    name = models.CharField(max_length=250, blank=True, null=False, verbose_name='Nombre comprador')
    email = models.EmailField(blank=True, default='', verbose_name='Email')
    identification = models.CharField(max_length=250, blank=True, default='', verbose_name='DNI / CUIT')
    phone = models.CharField(max_length=250, blank=True, default='', verbose_name='Teléfono')
    address = models.CharField(max_length=250, blank=True, default='', verbose_name='Dirección')
    number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número')
    floor = models.CharField(max_length=250, blank=True, default='', verbose_name='Piso')
    locality = models.CharField(max_length=250, blank=True, default='', verbose_name='Localidad')
    department = models.ForeignKey(Department, on_delete=models.CASCADE, verbose_name='Ciudad / Departamento')
    province = models.ForeignKey(Province, on_delete=models.CASCADE, verbose_name='Provincia / Estado')
    postal_code = models.CharField(max_length=250, blank=True, default='', verbose_name='Código Postal')
    others = models.TextField(blank=True, default='', verbose_name='Otros')


    def __str__(self):
        # return self.name
        return self.get_full_name()

    def get_full_name(self):
        return '{} / {}'.format(self.name, self.identification)

    def toJSON(self):
        item = model_to_dict(self)
        item['department'] = self.department.toJSON()
        item['province'] = self.province.toJSON()
        item['full_name'] = self.get_full_name()
        return item

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        db_table = 'client'
        ordering = ['name']
        unique_together = ('name', 'email')


# PRODUCTOS
class Product(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False, verbose_name='Nombre del Producto')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    stock = models.IntegerField(default=0, verbose_name='Stock')
    pvp = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Precio de venta')
    productimageid = models.CharField(max_length=250, blank=True, default='', verbose_name='Id Producto Imagen')

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        item['image'] = self.get_image()
        item['pvp'] = format(self.pvp, '.2f')
        return item

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        db_table = 'product'
        ordering = ['name']


# VENTAS
class Sale(models.Model):
    cli = models.ForeignKey(Client, on_delete=models.CASCADE)
    date_joined = models.DateField(default=datetime.now)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    iva = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')

    def __str__(self):
        return self.cli.name

    def toJSON(self): # cual mas?
        item = model_to_dict(self)
        item['cli'] = self.cli.toJSON()
        item['subtotal'] = format(self.subtotal, '.2f')
        item['iva'] = format(self.iva, '.2f')
        item['total'] = format(self.total, '.2f')
        item['date_joined'] = self.date_joined.strftime('%Y-%m-%d')
        item['det'] = [i.toJSON() for i in self.detsale_set.all()]  # vídeo 64 - 12:20
        return item

    def delete(self, using=None, keep_parents=False):
        for det in self.detsale_set.all():
            det.prod.stock += det.cant
            det.prod.save()
        super(Sale, self).delete()

    class Meta:
        verbose_name = 'Venta'
        verbose_name_plural = 'Ventas'
        db_table = 'sale'
        ordering = ['-id']


# DETALLE VENTA
class DetSale(models.Model):
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
    prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    cant = models.IntegerField(default=0)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.prod.name

    def toJSON(self):
        item = model_to_dict(self, exclude=['sale'])
        item['prod'] = self.prod.toJSON()
        item['price'] = format(self.price, '.2f')
        item['subtotal'] = format(self.subtotal, '.2f')
        return item

    class Meta:
        verbose_name = 'Detalle de Venta'
        verbose_name_plural = 'Detalle de Ventas'
        db_table = 'sale_detail'
        ordering = ['-id']


# PROCESOS
class Processes(models.Model):
    process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    destiny = models.CharField(max_length=30, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    state_dressmaking = models.ForeignKey(Dressmaking, on_delete=models.CASCADE, verbose_name='Confeccionista')

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    def toJSON(self):
        item = model_to_dict(self)
        item['process_prod'] = self.process_prod.toJSON()
        item['state'] = self.state.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        item['image'] = self.get_image()
        return item

    def __str__(self):
        return self.process_prod.name

    class Meta:
        verbose_name = 'Proceso'
        verbose_name_plural = 'Procesos'
        db_table = 'processes'
        ordering = ['-id']

# 01 PROCESO DISEÑOS
class ProcessesDesign(models.Model):
    process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    destiny = models.CharField(max_length=30, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    state_dressmaking = models.ForeignKey(Dressmaking, on_delete=models.CASCADE, verbose_name='Confeccionista')

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    def toJSON(self):
        item = model_to_dict(self)
        item['process_prod'] = self.process_prod.toJSON()
        item['state'] = self.state.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        item['image'] = self.get_image()
        return item

    def __str__(self):
        return self.process_prod.name

    class Meta:
        verbose_name = 'Proceso Disenio'
        verbose_name_plural = 'Procesos Disenios'
        db_table = 'processes_design'
        ordering = ['-id']

# 02 PROCESO MAQUETERIA
class ProcessesLayout(models.Model):
    process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    destiny = models.CharField(max_length=30, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    state_dressmaking = models.ForeignKey(Dressmaking, on_delete=models.CASCADE, verbose_name='Confeccionista')

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    def toJSON(self):
        item = model_to_dict(self)
        item['process_prod'] = self.process_prod.toJSON()
        item['state'] = self.state.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        item['image'] = self.get_image()
        return item

    def __str__(self):
        return self.process_prod.name

    class Meta:
        verbose_name = 'Proceso Maquetería'
        verbose_name_plural = 'Procesos Maqueterías'
        db_table = 'processes_layout'
        ordering = ['-id']

# 03 PROCESO IMPRESION
class ProcessesPrint(models.Model):
    process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    destiny = models.CharField(max_length=30, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    state_dressmaking = models.ForeignKey(Dressmaking, on_delete=models.CASCADE, verbose_name='Confeccionista')

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    def toJSON(self):
        item = model_to_dict(self)
        item['process_prod'] = self.process_prod.toJSON()
        item['state'] = self.state.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        item['image'] = self.get_image()
        return item

    def __str__(self):
        return self.process_prod.name

    class Meta:
        verbose_name = 'Proceso Impresión'
        verbose_name_plural = 'Procesos Impresiones'
        db_table = 'processes_print'
        ordering = ['-id']

# 04 PROCESO PLANCHADO
# 04 - IRONING
class ProcessesIroning(models.Model):
    process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    destiny = models.CharField(max_length=30, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    state_dressmaking = models.ForeignKey(Dressmaking, on_delete=models.CASCADE, verbose_name='Confeccionista')

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    def toJSON(self):
        item = model_to_dict(self)
        item['process_prod'] = self.process_prod.toJSON()
        item['state'] = self.state.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        item['image'] = self.get_image()
        return item

    def __str__(self):
        return self.process_prod.name

    class Meta:
        verbose_name = 'Proceso Planchado'
        verbose_name_plural = 'Procesos Planchados'
        db_table = 'processes_ironing'
        ordering = ['-id']

# 05 PROCESO CONFECCION
class ProcessesMaking(models.Model):
    process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    destiny = models.CharField(max_length=30, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    state_dressmaking = models.ForeignKey(Dressmaking, on_delete=models.CASCADE, verbose_name='Confeccionista')

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    def toJSON(self):
        item = model_to_dict(self)
        item['process_prod'] = self.process_prod.toJSON()
        item['state'] = self.state.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        item['state_dressmaking'] = self.state_dressmaking.toJSON()
        item['image'] = self.get_image()
        return item

    def __str__(self):
        return self.process_prod.name

    class Meta:
        verbose_name = 'Proceso Confección'
        verbose_name_plural = 'Procesos Confecciones'
        db_table = 'processes_making'
        ordering = ['-id']

# 06 PROCESO PAQUETERIA
class ProcessesPack(models.Model):
    process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    destiny = models.CharField(max_length=30, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    state_dressmaking = models.ForeignKey(Dressmaking, on_delete=models.CASCADE, verbose_name='Confeccionista')

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    def toJSON(self):
        item = model_to_dict(self)
        item['process_prod'] = self.process_prod.toJSON()
        item['state'] = self.state.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        item['image'] = self.get_image()
        return item

    def __str__(self):
        return self.process_prod.name

    class Meta:
        verbose_name = 'Proceso Paquetería'
        verbose_name_plural = 'Procesos Paqueterías'
        db_table = 'processes_pack'
        ordering = ['-id']

# 07 PROCESO DESPACHO
class ProcessesDispatch(models.Model):
    process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    destiny = models.CharField(max_length=30, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    state_dressmaking = models.ForeignKey(Dressmaking, on_delete=models.CASCADE, verbose_name='Confeccionista')

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    def toJSON(self):
        item = model_to_dict(self)
        item['process_prod'] = self.process_prod.toJSON()
        item['state'] = self.state.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        item['image'] = self.get_image()
        return item

    def __str__(self):
        return self.process_prod.name

    class Meta:
        verbose_name = 'Proceso Despacho'
        verbose_name_plural = 'Procesos Despachos'
        db_table = 'processes_dispatch'
        ordering = ['-id']

# 10 PROCESO PENDIENTES
class ProcessesPending(models.Model):
    process_prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    observation = models.TextField(blank=True, default='', verbose_name='Observaciones')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Cantidad')
    state = models.ForeignKey(Estados, on_delete=models.CASCADE, verbose_name='Estados')
    destiny = models.CharField(max_length=30, null=False, blank=False, verbose_name='Tiendas')
    process_date_joined = models.DateField(default=datetime.now)
    process_date_finish = models.DateField(default=datetime.now)
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    state_dressmaking = models.ForeignKey(Dressmaking, on_delete=models.CASCADE, verbose_name='Confeccionista')

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    def toJSON(self):
        item = model_to_dict(self)
        item['process_prod'] = self.process_prod.toJSON()
        item['state'] = self.state.toJSON()
        item['process_date_joined'] = self.process_date_joined.strftime('%Y-%m-%d')
        item['process_date_finish'] = self.process_date_finish.strftime('%Y-%m-%d')
        item['image'] = self.get_image()
        return item

    def __str__(self):
        return self.process_prod.name

    class Meta:
        verbose_name = 'Proceso Pendiente'
        verbose_name_plural = 'Procesos Pendientes'
        db_table = 'processes_pending'
        ordering = ['-id']

# 08 - FINALIZADO | 09 CANCELADO

# TIENDANUBE
class TiendanubeApi(models.Model):
    order_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número orden')
    created_at = models.DateField(null=True, blank=True, verbose_name='Fecha venta')
    customer_name = models.CharField(max_length=250, blank=True, default='', verbose_name='Nombre comprador')
    customer_email = models.EmailField(blank=True, default='', verbose_name='Email')
    customer_identification = models.CharField(max_length=250, blank=True, default='', verbose_name='DNI / CUIT')
    customer_phone = models.CharField(max_length=250, blank=True, default='', verbose_name='Teléfono')
    customer_address = models.CharField(max_length=250, blank=True, default='', verbose_name='Dirección')
    customer_number = models.CharField(max_length=250, blank=True, default='', verbose_name='Número')
    customer_floor = models.CharField(max_length=250, blank=True, default='', verbose_name='Piso')
    customer_locality = models.CharField(max_length=250, blank=True, default='', verbose_name='Localidad')
    customer_city = models.CharField(max_length=250, blank=True, default='', verbose_name='Ciudad / Departamento')
    customer_zipcode = models.CharField(max_length=250, blank=True, default='', verbose_name='Código Postal')
    customer_province = models.CharField(max_length=250, blank=True, default='', verbose_name='Provincia / Estado')
    customer_country = models.CharField(max_length=250, blank=True, default='', verbose_name='País')
    product_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id producto')
    product_name = models.CharField(max_length=250, blank=True, default='', verbose_name='Nombre producto')
    product_price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Precio producto')
    product_quantity = models.IntegerField(default=0, verbose_name='Cantidad producto')
    product_image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen producto')
    paid_at = models.DateField(null=True, blank=True, verbose_name='Fecha pago')
    gateway = models.CharField(max_length=250, blank=True, default='', verbose_name='Medio pago')
    oder_status = models.CharField(max_length=250, blank=True, default='', verbose_name='Estado orden')
    payment_status = models.CharField(max_length=250, blank=True, default='', verbose_name='Estado pago')
    currency = models.CharField(max_length=250, blank=True, default='', verbose_name='Moneda')
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Subtotal Venta')
    discount_coupon = models.CharField(max_length=250, blank=True, default='', verbose_name='Cupón descuento')
    discount = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Descuento')
    shipping_cost_customer = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Costo envío')
    total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Total venta')
    shipping_option = models.CharField(max_length=250, blank=True, default='', verbose_name='Medio envío')
    customer_note = models.TextField(blank=True, default='', verbose_name='Nota comprador', null=True)
    owner_note = models.TextField(blank=True, default='', verbose_name='Nota vendedor', null=True)
    gateway_id = models.CharField(max_length=250, blank=True, default='', null=True, verbose_name='Identificador transacción medio pago')
    order_id = models.CharField(max_length=250, blank=True, default='', verbose_name='Id orden')

    def toJSON(self):
        item = model_to_dict(self)
        item['created_at'] = self.created_at.strftime('%Y-%m-%d')
        item['subtotal'] = format(self.subtotal, '.2f')
        item['discount'] = format(self.discount, '.2f')
        item['shipping_cost_customer'] = format(self.shipping_cost_customer, '.2f')
        item['total'] = format(self.total, '.2f')
        item['paid_at'] = self.paid_at.strftime('%Y-%m-%d')
        item['product_price'] = format(self.product_price, '.2f')
        item['product_image'] = self.get_product_image()
        return item

    def __str__(self):
        return self.order_number

    def get_product_image(self):
        if self.get_product_image:
            # return '{}{}'.format(MEDIA_URL, self.get_image)
            return '{}{}'.format(MEDIA_URL, self.get_product_image)
        return '{}{}'.format(STATIC_URL, 'image/empty.png')

    class Meta:
        verbose_name = 'Tiendanube'
        verbose_name_plural = 'Tiendanube'
        db_table = 'tiendanubeapi'
        ordering = ['-order_number']


# ESTADOL DE SERVICIO (PAGO - IMPAGO)
class ServicesState(models.Model):
    name = models.CharField(max_length=20, null=False, blank=False, unique=True, verbose_name='Estado Servicio')

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Estado de Servicio'
        verbose_name_plural = 'Estados de Servicios'
        db_table = 'services_state'
        ordering = ['id']


# TIPO DE SERVICIOS / PAGO DE LOS SERVICIOS
class Services(models.Model):
    name = models.CharField(max_length=250, null = False, blank = False, unique = True, verbose_name='Nombre servicio')
    client_number = models.CharField(max_length=250, blank=True, default='', verbose_name='N° Cliente / Cuenta')
    date_joined = models.DateField(default=datetime.now)
    date_finish = models.DateField(default=datetime.now)
    total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Total a pagar')
    services_state = models.ForeignKey(ServicesState, on_delete=models.CASCADE, verbose_name='Estado Servicio')

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        item['date_joined'] = self.date_joined.strftime('%Y-%m-%d')
        item['date_finish'] = self.date_finish.strftime('%Y-%m-%d')
        item['total'] = format(self.total, '.2f')
        item['services_state'] = self.services_state.toJSON()
        return item

    class Meta:
        verbose_name = 'Servicio'
        verbose_name_plural = 'Servicios'
        db_table = 'services'
        ordering = ['-id']



